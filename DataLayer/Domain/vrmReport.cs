/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886
using System;
using System.Collections;

namespace myVRM.DataLayer
{
	/// <summary>
	/// Summary description(s) for Reports.
	/// </summary>
	class SQLQueryModify
	{
		public const int SQLQueryStringPreQuery    = 1;
		public const int SQLQueryStringPostQuery   = 2;
		public const int SQLQueryStringPreFilter   = 3;
		public const int SQLQueryStringSelectQuery = 4;
	}
	/// <summary>
	/// Summary description for Report ReportSchedule.
	/// </summary>
	public class ReportSchedule
	{
		#region Private Internal Members
	
		private int m_ID;
		private int m_userId;
		private int m_tID;
		private DateTime m_submitDate;
		private string m_Title;
		private string m_Description;
		private DateTime m_completeDate;
		private int m_timeZone;
		private string m_inXml;
		private int m_status;
		private string m_notifyEmail;
		private string m_report;
		private int m_deleted;

		#endregion
		
		#region Public Properties

		public int ID
		{
			get {return m_ID;}
			set {m_ID = value;}
		}
		public int userId
		{
			get {return m_userId;}
			set {m_userId = value;}
		}
		public int tID
		{
			get {return m_tID;}
			set {m_tID = value;}
		}
		public DateTime SubmitDate
		{
			get {return m_submitDate;}
			set {m_submitDate = value;}
		}
		public string reportTitle
		{
			get {return m_Title;}
			set {m_Title = value;}
		}
		public string reportDescription
		{
			get {return m_Description;}
			set {m_Description = value;}
		}		
		public DateTime CompleteDate
						
		{
			get {return m_completeDate;}
			set {m_completeDate = value;}
		}

		public int timeZone
		{
			get {return m_timeZone;}
			set {m_timeZone = value;}
		}
		public string inXml
		{
			get {return m_inXml;}
			set {m_inXml = value;}
		}		
		public int status
		{
			get {return m_status;}
			set {m_status = value;}
		}
		public string notifyEmail
		{
			get {return m_notifyEmail;}
			set {m_notifyEmail = value;}
		}		
		public string report
		{
			get 
			{
				if(m_report == null) return string.Empty;
				else return m_report;
			}
			set	
			{
				if(value == null) m_report  = string.Empty;
				else m_report  = value;
			}
		}
		public int deleted
		{
			get {return m_deleted;}
			set {m_deleted = value;}
		}
		
		#endregion		
		
		public ReportSchedule()
		{
		}
	}
	/// <summary>
	/// Summary description for Report Template.
	/// </summary>
	public class ReportTemplate
	{
		#region Private Internal Members
	
		private int m_ID;
		private int m_userId;
		private int m_InputID;
		private DateTime m_LastModified;
		private string m_Title;
		private string m_Description;
		private string m_query;
		private string m_tables;
		private string m_orderby;
		private string m_filter;
		private string m_outputformat;
		private string m_totalquery;
		private string m_outputTotal;
		private string m_columnHeadings;
		private int m_Public;
		private int m_deleted;

		#endregion
		
		#region Public Properties

		public int ID
		{
			get {return m_ID;}
			set {m_ID = value;}
		}
		public int userId
		{
			get {return m_userId;}
			set {m_userId = value;}
		}
		public int InputID
		{
			get {return m_InputID;}
			set {m_InputID = value;}
		}
		public DateTime LastModified
		{
			get {return m_LastModified;}
			set {m_LastModified = value;}
		}
		public string templateTitle
		{
			get {return m_Title;}
			set {m_Title = value;}
		}
		public string templateDescription
		{
			get {return m_Description;}
			set {m_Description = value;}
		}		
		public string query
		{
			get {return m_query;}
			set {m_query = value;}
		}		
		public string tables
		{
			get {return m_tables;}
			set {m_tables = value;}
		}
		public string orderby
		{
			get {return m_orderby;}
			set {m_orderby = value;}
		}		
		public string filter
		{
			get {return m_filter;}
			set {m_filter = value;}
		}
		public string outputformat
		{
			get {return m_outputformat;}
			set {m_outputformat = value;}
		}		
		public string totalquery
		{
			get {return m_totalquery;}
			set {m_totalquery = value;}
		}		
		public string outputTotal
		{
			get {return m_outputTotal;}
			set {m_outputTotal = value;}
		}
		public string columnHeadings
		{
			get {return m_columnHeadings;}
			set {m_columnHeadings = value;}
		}
		public int isPublic
		{
			get {return m_Public;}
			set {m_Public = value;}
		}
		public int deleted
		{
			get {return m_deleted;}
			set {m_deleted = value;}
		}
		
		#endregion		
		
		public ReportTemplate()
		{
		}
	}
	
	public class TokenObject
	{
		public string tokenName;
		public string tokenValue;
	}

	// just what we need for now
	public class VRMuser
	{
		public int    userId;
		public string firstName,lastName,email;
	}

	/// <summary>
	/// Summary description for Report Input.
	/// </summary>
	public class ReportInputItem
	{
		#region Private Internal Members
	
		private int m_inputID;
		private string m_inputClause;

		#endregion
		
		#region Public Properties

		public int inputID
		{
			get {return m_inputID;}
			set {m_inputID = value;}
		}
		public string inputClause
		{
			get {return m_inputClause;}
			set {m_inputClause = value;}
		}
		#endregion		
		
		public ReportInputItem()
		{
		}
	}

	public class InputItem
	{
		public int inputID;
		public int itemID;
		public string inputItem;
	}

    //FB 2343 Start
    public class ReportMonthlyDays
    {
        #region Private Internal Members

        private int m_UID;
        private int m_ID;
        private int m_OrgId;
        private string m_MonthName;
        private int m_MonthWorkingDays;
        private DateTime m_MonthStartDate;
        private DateTime m_MonthEndDate;
        private int m_CurrentYear;

        #endregion

        #region Public Properties

        public int UID
        {
            get { return m_UID; }
            set { m_UID = value; }
        }
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        public int OrgId
        {
            get { return m_OrgId; }
            set { m_OrgId = value; }
        }
        public string MonthName
        {
            get { return m_MonthName; }
            set { m_MonthName = value; }
        }
        public int MonthWorkingDays
        {
            get { return m_MonthWorkingDays; }
            set { m_MonthWorkingDays = value; }
        }
        public DateTime MonthStartDate
        {
            get { return m_MonthStartDate; }
            set { m_MonthStartDate = value; }
        }
        public DateTime MonthEndDate
        {
            get { return m_MonthEndDate; }
            set { m_MonthEndDate = value; }
        }
        public int CurrentYear
        {
            get { return m_CurrentYear; }
            set { m_CurrentYear = value; }
        }
        #endregion

        public ReportMonthlyDays() //FBDoubt
        {
        }
    }

    public class ReportWeeklyDays
    {
        #region Private Internal Members

        private int m_UID;
        private int m_ID;
        private int m_OrgId;
        private string m_WeekNumber;
        private int m_WeekWorkingDays;
        private DateTime m_WeekStartDate;
        private DateTime m_WeekEndDate;
        private int m_CurrentYear;

        #endregion

        #region Public Properties

        public int UID
        {
            get { return m_UID; }
            set { m_UID = value; }
        }
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        public int OrgId
        {
            get { return m_OrgId; }
            set { m_OrgId = value; }
        }
        public string WeekNumber
        {
            get { return m_WeekNumber; }
            set { m_WeekNumber = value; }
        }
        public int WeekWorkingDays
        {
            get { return m_WeekWorkingDays; }
            set { m_WeekWorkingDays = value; }
        }
        public DateTime WeekStartDate
        {
            get { return m_WeekStartDate; }
            set { m_WeekStartDate = value; }
        }
        public DateTime WeekEndDate
        {
            get { return m_WeekEndDate; }
            set { m_WeekEndDate = value; }
        }
        public int CurrentYear
        {
            get { return m_CurrentYear; }
            set { m_CurrentYear = value; }
        }
        #endregion

        public ReportWeeklyDays()
        {
        }
    }
    //FB 2343 End
    //FB 2410 Start
    public class BatchReportSettings
    {
        #region Private Internal Members

        private int m_ID, m_OrgId, m_CreatedUser, m_FrequencyType, m_WorkingHours, m_HostID, m_RequestorID, m_VNOCID, m_Timezone, m_MCUID, m_EndpointID;//ZD 101708
        private string m_ReportType, m_InputType, m_InputValue, m_EmailAddress, m_JobName, m_ReportName, m_Operator, m_ConfTitle, m_CallURI, m_RoomIDs;//ZD 101708
        private DateTime m_DateFrom, m_DateTo, m_RptStartDate, m_RptEndDate;
        
        #endregion

        #region Public Properties
        

        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        public int OrgId
        {
            get { return m_OrgId; }
            set { m_OrgId = value; }
        }
        public int CreatedUser
        {
            get { return m_CreatedUser; }
            set { m_CreatedUser = value; }
        }
        public string JobName
        {
            get { return m_JobName; }
            set { m_JobName = value; }
        } 
        public string ReportType
        {
            get { return m_ReportType; }
            set { m_ReportType = value; }
        }       
        public DateTime DateFrom
        {
            get { return m_DateFrom; }
            set { m_DateFrom = value; }
        }
        public DateTime DateTo
        {
            get { return m_DateTo; }
            set { m_DateTo = value; }
        }
        public DateTime RptStartDate
        {
            get { return m_RptStartDate; }
            set { m_RptStartDate = value; }
        }
        public DateTime RptEndDate
        {
            get { return m_RptEndDate; }
            set { m_RptEndDate = value; }
        }
        public string InputType
        {
            get { return m_InputType; }
            set { m_InputType = value; }
        }
        public string InputValue
        {
            get { return m_InputValue; }
            set { m_InputValue = value; }
        }
        public string EmailAddress
        {
            get { return m_EmailAddress; }
            set { m_EmailAddress = value; }
        }
        public int FrequencyType
        {
            get { return m_FrequencyType; }
            set { m_FrequencyType = value; }
        }
        public int WorkingHours
        {
            get { return m_WorkingHours; }
            set { m_WorkingHours = value; }
        }
        public string ReportName
        {
            get { return m_ReportName; }
            set { m_ReportName = value; }
        }
        //ZD 101708
        public int HostID
        {
            get { return m_HostID; }
            set { m_HostID = value; }
        }
        public int RequestorID
        {
            get { return m_RequestorID; }
            set { m_RequestorID = value; }
        }
        public int VNOCID
        {
            get { return m_VNOCID; }
            set { m_VNOCID = value; }
        }
        public int Timezone
        {
            get { return m_Timezone; }
            set { m_Timezone = value; }
        }
        public int MCUID
        {
            get { return m_MCUID; }
            set { m_MCUID = value; }
        }
        public int EndpointID
        {
            get { return m_EndpointID; }
            set { m_EndpointID = value; }
        }
        public string Operator
        {
            get { return m_Operator; }
            set { m_Operator = value; }
        }
        public string ConfTitle
        {
            get { return m_ConfTitle; }
            set { m_ConfTitle = value; }
        }
        public string CallURI
        {
            get { return m_CallURI; }
            set { m_CallURI = value; }
        }
        public string RoomIDs
        {
            get { return m_RoomIDs; }
            set { m_RoomIDs = value; }
        }
        #endregion

        public BatchReportSettings()
        {
        }
    }

    public class BatchReportDates
    {
        #region Private Internal Members

        private int m_ID, m_ReportID, m_FrequencyType, m_Sent;
        private DateTime m_ReportDate, m_SentTime = DateTime.MinValue;

        #endregion

        #region Public Properties

        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        public int ReportID
        {
            get { return m_ReportID; }
            set { m_ReportID = value; }
        }
        public DateTime ReportDate
        {
            get { return m_ReportDate; }
            set { m_ReportDate = value; }
        }
        public int FrequencyType
        {
            get { return m_FrequencyType; }
            set { m_FrequencyType = value; }
        }
        public int Sent
        {
            get { return m_Sent; }
            set { m_Sent = value; }
        }
        public DateTime SentTime
        {
            get { return m_SentTime; }
            set { m_SentTime = value; }
        }
       
        
        #endregion

        public BatchReportDates()
        {
        }
    }

    //FB 2410 End


}
