﻿<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100866 End--%>
<%@ Page Language="C#" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" Inherits="ns_MonitorMCU" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<meta http-equiv="Cache-control" content="no-cache"/>
    <title>Call Monitor</title>    
    <script type="text/javascript">
        //ZD 100604 start
        var img = new Image();
        img.src = "../en/image/wait1.gif";
        //ZD 100604 End
       function showNestedGridView(obj) {
           var nestedGridView = document.getElementById(obj);
           var imageID = document.getElementById('img' + obj);
           
           var gridState = document.getElementById("hdnGridState"); 

           if (nestedGridView.style.display == "none") {
               gridState.value += nestedGridView.id + ",,"; // ZD 100988
               nestedGridView.style.display = "inline";
               imageID.src = "image/loc/nolines_minus.gif";
           } else {
               gridState.value = gridState.value.replace(nestedGridView.id + ",,",""); // ZD 100988
               nestedGridView.style.display = "none";
               imageID.src = "image/loc/nolines_plus.gif";
           }
           
       }
       
       
       function showNestedGridView2(obj) {
           var nestedGridView = document.getElementById(obj);
           var imageID = document.getElementById('img' + obj);
           
           var gridState = document.getElementById("hdnGridState"); 
           

           if (nestedGridView.style.display == "none") {
               gridState.value += nestedGridView.id + ",,"; // ZD 100988
               nestedGridView.style.display = "inline";
               imageID.src = "image/loc/nolines_minus.gif";
           } else {
               gridState.value = gridState.value.replace(nestedGridView.id + ",,",""); // ZD 100988
               nestedGridView.style.display = "none";
               imageID.src = "image/loc/nolines_plus.gif";
           }
       }
       
       function goToCal()
       {
       if(document.getElementById("lstCalendar") != null)
       {
		           if (document.getElementById("lstCalendar").value == "4"){
                           window.location.href = "MonitorMCU.aspx";
                   } 
		           if (document.getElementById("lstCalendar").value == "5"){
                   window.location.href = "point2point.aspx";
                   }
               }
           }
           function DataLoading(val) {
               //alert(val);
               if (val == "1")
                   document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
               else
                   document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
           }    

    </script>    
    <style type="text/css">
       .hidden
    {
        display: none;
    }    
     a img { outline:none;
    text-decoration:none;
    border:0;
    }
    </style>
    
    <link href="css/MonitorMCU.css" type="text/css" rel="stylesheet" />    
    
</head>
<script type="text/javascript" src="script/CallMonitorJquery/jquery.1.4.2.js" ></script>
<script type="text/javascript" src="script/CallMonitorJquery/jquery.bpopup-0.7.0.min.js"></script>
<script type="text/javascript" src="../<%=Session["language"] %>/script/CallMonitorJquery/MonitorMCU.js"></script>
<script type="text/javascript" src="script/CallMonitorJquery/json2.js" ></script>
<body id="frmMonitor"> <%--ZD 100980--%>
    <div id="refDiv" style="width:1600px; height:1400px; z-index: 10000; position:Fixed; left:0px; top:0px; display:none; background-color:Gray; filter: alpha(opacity=50) !important; opacity:0.5; overflow:hidden;">
        <div align="center" style="display:block; position:fixed; top:25%; left:50%">
            <img border="0" src="image/wait1.gif" alt="<asp:Literal Text='<%$ Resources:WebResources, Loading%>' runat='server'></asp:Literal>" />
        </div>
    </div>
    <div id="successbox" style="text-align:center; font-family:Verdana;" class="lblMessage"></div><br />
    <div id="errormsgbox" style="text-align: center; font-family:Verdana; font-weight:bold; color: Red;"></div><br />
    <form id="form1" runat="server">
     <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
    <asp:UpdatePanel ID="updtNonIframe" runat="server" UpdateMode="Always"><%--ZD 101417 Starts--%>
    <ContentTemplate>
    <input type="hidden" id="hdnGridState" runat="server" />
    <input type="hidden" id="communStatus" value="0" />
    <input type="hidden" id="TotalMcuCount" value="0" runat="server"  />
    <input type="hidden" id="msgData" value="" />
    <input type="hidden" id="MuteAllIdArray" value="" /> <%--// FB 2652--%>
    <input type="hidden" id="MuteAllterminalTypeArray" value="" /> <%--// FB 2652--%>
    <input type="hidden" id="CurrentConfSupportingRowID" value="" />  <%--// FB 2652--%>
    <div>
        <%--FB 2646 Starts--%> 
        <%--FB 2984 Starts--%>
        <table align="center">
        <tr>
            <td>
                <table align="right" border="0" width="140"> <%--FB 2646 Starts--%>
                <tr>        
                    <td align="right" nowrap="nowrap">
                        <h3><asp:Literal Text="<%$ Resources:WebResources, MonitorMCU_CallMonitor%>" runat="server"></asp:Literal></h3>
                    </td>
                </tr>
                </table>
            </td>
            <td>
                <table width="230">
                <tr>
                    <td align="left" style="width:200">
                        <select id="lstCalendar" name="lstCalendar" class="altText"  size="1" onchange="goToCal();javascript:DataLoading('1');" runat="server"> <%--FB 2058--%>
          		        </select> 
                    </td>
                </tr>
                </table>
             </td>
             <td>
                <table>
                    <tr>
                        <td style="width:100" nowrap="nowrap"> 
                            <asp:Label id="lblchksilo" text="<%$ Resources:WebResources, MonitorMCU_lblchksilo%>" runat="server" cssclass="blackblodtext"></asp:Label>
                        </td>
                        <td style="width:50">
                            <asp:CheckBox ID="chkAllSilo" runat="server" AutoPostBack="true"/> 
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        </table> 
        <div id="dataLoadingDIV" name="dataLoadingDIV" style="display:none" align="center" >
              <img border='0' src='image/wait1.gif' runat="server" alt='<%$ Resources:WebResources, Loading%>' />
         </div> <%--ZD 100678 End--%>
            <%--FB 2984 Ends--%>
            <%--FB 2646 Ends--%>
            
        <br />
        <div style="padding-left:400px"><asp:Label ID="lblError" runat="server" CssClass="lblError"></asp:Label></div><br /> <%--FB 2653--%>
        <asp:GridView ID="grdGrand" runat="server" AutoGenerateColumns="false" OnRowDataBound="bindConference"
            HorizontalAlign="Center" HeaderStyle-BackColor="#000033" HeaderStyle-ForeColor="White"
            GridLines="None" CellPadding="1" ShowHeader="false" BackColor="#F2F4F8">
            <EmptyDataTemplate><font style="color:Black;" ><span class="lblError"><asp:Literal Text="<%$ Resources:WebResources, MonitorMCU_NoRecordsFoun%>" runat="server"></asp:Literal></span></font></EmptyDataTemplate> 
            <Columns>
                <asp:TemplateField ItemStyle-BackColor="#3075AE" ItemStyle-ForeColor="#FFFFFF" ItemStyle-Width="8%">
                    <ItemTemplate>
                        <a href="javascript:showNestedGridView('divid-<%# Eval("nameid") %>');"><%--FB 2956--%>
                            <img id="imgdivid-<%# Eval("nameid") %>" alt="<asp:Literal Text='<%$ Resources:WebResources, ShowHide%>' runat='server' />" border="0" src="image/loc/nolines_plus.gif" /><%--FB 2956--%>
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/image/MonitorMCU/mcu.gif" Width="25px" AlternateText="<%$ Resources:WebResources, MonitorMCU_MonitorMCU%>" /> <%--ZD 100419--%>
                            <asp:Label ID="lblConfMultiInfo" Visible="false" Text='<%# Eval("confMultiInfo") %>' runat="server"></asp:Label>
                            <asp:Label ID="lblPartyMultiInfo" Visible="false" Text='<%# Eval("partyMultiInfo") %>' runat="server"></asp:Label>
                            <asp:Label ID="lblMcuIP" Visible="false" Text='<%# Eval("ip") %>' runat="server"></asp:Label>                            
                            <input type="hidden" id="confTotalCount<%# Container.DataItemIndex +1 %>" value="<%# Eval("confCount") %>" />
                            
                        </a>                                                
                    </ItemTemplate>
                </asp:TemplateField>
                <%--FB 2501 Dec10 Start--%>
                <asp:TemplateField ItemStyle-BackColor="#3075AE" ItemStyle-ForeColor="#FFFFFF" ItemStyle-Width="3%">
                    <ItemTemplate>
                        <span><asp:Literal Text="<%$ Resources:WebResources, MonitorMCU_Name%>" runat="server"></asp:Literal></span>
                    </ItemTemplate>
                </asp:TemplateField>                 
                <asp:BoundField DataField="name" HeaderText="Name" ItemStyle-BackColor="#3075AE" ItemStyle-ForeColor="#FFFFFF" ItemStyle-Width="15%" ></asp:BoundField> <%--FB 2501 Dec7--%>
                <asp:TemplateField ItemStyle-BackColor="#3075AE" ItemStyle-ForeColor="#FFFFFF" ItemStyle-Width="3%"> <%--FB 2646 Starts--%>
                    <ItemTemplate>
                        <span><asp:Literal Text="<%$ Resources:WebResources, MonitorMCU_Silo%>" runat="server"></asp:Literal></span>
                    </ItemTemplate>
                </asp:TemplateField>                 
                <asp:BoundField DataField="siloName" HeaderText="Silo" ItemStyle-BackColor="#3075AE" ItemStyle-ForeColor="#FFFFFF" ItemStyle-Width="15%" ></asp:BoundField> <%--FB 2646 Ends--%>
                 <asp:TemplateField ItemStyle-BackColor="#3075AE" ItemStyle-ForeColor="#FFFFFF" ItemStyle-Width="5%">
                    <ItemTemplate>
                        <span><asp:Literal Text="<%$ Resources:WebResources, MonitorMCU_Address%>" runat="server"></asp:Literal></span>
                    </ItemTemplate>
                </asp:TemplateField> 
                <asp:BoundField DataField="ip" HeaderText="IP"  ItemStyle-BackColor="#3075AE" ItemStyle-ForeColor="#FFFFFF" ItemStyle-Width="12%"></asp:BoundField>                
                 <asp:TemplateField ItemStyle-BackColor="#3075AE" ItemStyle-ForeColor="#FFFFFF" ItemStyle-Width="3%">
                    <ItemTemplate>
                        <span><asp:Literal Text="<%$ Resources:WebResources, MonitorMCU_Type%>" runat="server"></asp:Literal></span>
                    </ItemTemplate>
                </asp:TemplateField> 
                <asp:BoundField DataField="type" HeaderText="Type" ItemStyle-BackColor="#3075AE" ItemStyle-ForeColor="#FFFFFF" ItemStyle-Width="17%"></asp:BoundField>                
                <asp:TemplateField ItemStyle-BackColor="#3075AE" ItemStyle-ForeColor="#FFFFFF" ItemStyle-Width="3%"> <%--FB 2501 Dec7--%>
                    <ItemTemplate>
                        <span><asp:Literal Text="<%$ Resources:WebResources, MonitorMCU_Ports%>" runat="server"></asp:Literal></span>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ItemStyle-BackColor="#3075AE" ItemStyle-ForeColor="#FFFFFF" ItemStyle-Width="5%"> <%--FB 2501 Dec7--%>
                    <ItemTemplate>
                        <asp:Label ID="MCUTotalPort"  Text='<%# Eval("MCUTotalPort") %>' ToolTip="<%$ Resources:WebResources, AvailableTotal%>" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <%--FB 2501 Dec6 End--%>
                <%--FB 2501 Dec10 Start--%>
                <asp:BoundField DataField="MCUStatus" HeaderText="MCUStatus" ItemStyle-BackColor="#3075AE" ItemStyle-ForeColor="#FFFFFF" ItemStyle-Width="17%"></asp:BoundField>                
                
                
                <asp:TemplateField ItemStyle-BackColor="#3075AE" ItemStyle-ForeColor="#FFFFFF" ItemStyle-Width="3%">
                    <ItemTemplate>
                        <a href='' onclick='this.childNodes[0].click();return false;'><img src="../image/MonitorMCU/favourite_<%# Eval("favorite") %>.gif" title="<asp:Literal Text='<%$ Resources:WebResources, Favorite%>' runat='server'></asp:Literal>" class="setfavorite" id="<%# Eval("mcuID") %>" width="25px"  alt="<asp:Literal Text='<%$ Resources:WebResources, Favorite%>' runat='server' />" /></a>   <%--ZD 100419--%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ItemStyle-BackColor="#3075AE" ItemStyle-ForeColor="#FFFFFF">
                    <ItemTemplate>
                        <tr>
                            <td colspan="100%">
                                <div id="divid-<%# Eval("nameid") %>" style="display: none; position: relative;">
                                    <asp:GridView ID="grdParent" CellPadding="1" runat="server" AutoGenerateColumns="false" OnRowDataBound="bindParticipant" Width="97%" HeaderStyle-BackColor="#330000" HeaderStyle-ForeColor="white" HorizontalAlign="Right"
                                        GridLines="None" ShowHeader="false">                                        
                                            <EmptyDataTemplate><font style="color:Black;" ><span class="lblError"><asp:Literal Text="<%$ Resources:WebResources, MonitorMCU_NoConferences%>" runat="server"></asp:Literal></span></font></EmptyDataTemplate> 
                                        <Columns>
                                            <asp:TemplateField ItemStyle-BackColor="#D4D7DA" ItemStyle-ForeColor="#000000" ItemStyle-Width="6%">
                                                <ItemTemplate> <%--ZD 102784--%>
                                                    <a href="javascript:showNestedGridView2('divid-<%# Eval("confId") %><%# ((string)Eval("xconfName")).Replace("'","").Replace("\"","").Replace("\\","") %><%# ((string)Eval("confName")).Replace("'","").Replace("\"","").Replace("\\","") %>');"> <%--ZD 100940--%>
                                                        <img id="imgdivid-<%# Eval("confId") %><%# ((string)Eval("xconfName")).Replace("'","").Replace("\"","").Replace("\\","") %><%# ((string)Eval("confName")).Replace("'","").Replace("\"","").Replace("\\","") %>" alt="<asp:Literal Text='<%$ Resources:WebResources, ShowHide%>' runat='server' />" border="0" src="image/loc/nolines_plus.gif" /><%--ZD 100940--%>
                                                    </a>
                                                    <asp:Image ID="Image2" runat="server" ImageUrl="~/image/MonitorMCU/confIcon.gif" AlternateText="Conf-Icon" Width="25px" /> <%--ZD 100419--%>
                                                    <input type="hidden" id="hdnlevel<%# Eval("xparentId") %>" />
                                                    <span id="hdnconfid<%# Eval("xparentId") %>"  value="<%# Eval("confId") %>" />
                                                    <input type="hidden" id="McuBridgeID<%# Eval("xparentId") %>" value="<%# Eval("confMcuBridgeID") %>" />                                                                                                       
                                                    <asp:Label ID="mcubridgeID" runat="server" Visible="false" Text='<%# Bind("confMcuBridgeID") %>'></asp:Label>  
                                                    <asp:Label ID="lblxparentid" runat="server" Visible="false" Text='<%# Bind("xparentId") %>'></asp:Label>  
                                                    <asp:Label ID="lblxmergestr" runat="server" Visible="false" Text='<%# Bind("xmergeString") %>'></asp:Label>                                                                                                                                                                                                             
                                                    <asp:Label ID="lblPartyMultiInfo2" Visible="false" Text='<%# Eval("partyMultiInfo2") %>' runat="server"></asp:Label>
                                                    <asp:Label ID="CamVisible" Visible="false" Text='<%# Eval("confcameraStatus") %>' runat="server"></asp:Label> 
                                                </ItemTemplate>
                                            </asp:TemplateField>                                            
                                            <asp:BoundField DataField="xparentId" Visible="false"  ItemStyle-BackColor="#D4D7DA" ItemStyle-ForeColor="#000000">
                                            </asp:BoundField>                                            
                                            <asp:BoundField DataField="xmergeString" Visible="false" ItemStyle-BackColor="#D4D7DA" ItemStyle-ForeColor="#000000">
                                            </asp:BoundField>
                                            <asp:BoundField DataField="confName" HeaderText="ConfName"  ItemStyle-BackColor="#D4D7DA" ItemStyle-ForeColor="#000000" ItemStyle-Width="15%"></asp:BoundField>
                                            <%--<asp:BoundField DataField="confId" HeaderText="confId" ItemStyle-BackColor="#D4D7DA" ItemStyle-ForeColor="#000000"></asp:BoundField> --%>
                                            <asp:BoundField DataField="confDate" HeaderText="confDate" ItemStyle-BackColor="#D4D7DA" ItemStyle-ForeColor="#000000" ItemStyle-Width="15%"></asp:BoundField>                                            
                                            <%--<asp:BoundField DataField="duration" HeaderText="duration" ItemStyle-BackColor="#D4D7DA" ItemStyle-ForeColor="#000000" ItemStyle-Width="7%"></asp:BoundField>--%> 
                                            <asp:TemplateField ItemStyle-BackColor="#D4D7DA" ItemStyle-ForeColor="#000000" ItemStyle-Width="5%"><%-- ZD 100420--%>
                                                <ItemTemplate> <%--FB 2582 --%>
                                            <label style="color:Blue" id="coundowntime<%# Eval("xconfMcuIndex") %><%# Container.DataItemIndex +1 %>" onload="javascript:MessageBox()" visible="true" value="0" ></label> <%--Tamil1--%>
                                            </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField ItemStyle-BackColor="#D4D7DA" ItemStyle-ForeColor="#000000" ItemStyle-Width="59%" ItemStyle-HorizontalAlign="Right" ><%-- ZD 100420--%>
                                                <ItemTemplate>
                                                  <input type="hidden" id="userID" value="<%=Session["UserID"]%>" />
                                                    <input type="hidden" id="conUserId<%# Eval("xparentId") %>" value="<%=Session["UserID"]%>" />
                                                    <input type="hidden" id="conID<%# Eval("xparentId") %>" value="<%# Eval("confId") %>" />
                                                    <input type="hidden" id="conActualStatus<%# Eval("xparentId") %>" value="<%# Eval("confActualStatus") %>" />
                                                    <input type="hidden" id="conUniqID<%# Eval("xparentId") %>" value="<%# Eval("mcuAlias") %>" />
                                                    <input type="hidden" id="conName<%# Eval("xparentId") %>" value="<%# Eval("confName") %>" />
                                                    <input type="hidden" id="conType<%# Eval("xparentId") %>" value="<%# Eval("confType") %>" />                                                    
                                                    <input type="hidden" id="conStatus<%# Eval("xparentId") %>" value="<%# Eval("confStatus") %>" />
                                                    <input type="hidden" id="conStartMode<%# Eval("xparentId") %>" value="<%# Eval("connectingStatus") %>" />
                                                    <input type="hidden" id="conLastRun<%# Eval("xparentId") %>" value="<%# Eval("confLastRun") %>" />
                                                    <input type="hidden" id="hdnProfileID" value="<%=Session["ProfileID"]%>" />
                                                    <input type="hidden" id="hdnMcuIP<%# Eval("xparentId") %>" value="<%# Eval("McuIP")%>" /> 
                                                    <input type="hidden" id="hdntime<%# Eval("xconfMcuIndex") %><%# Container.DataItemIndex +1 %>" value="<%# Eval("ConfFinishingTime")%>" /> 
                                                    <input type="hidden" id="hdnSetlayoutVal<%# Eval("xparentId") %>" value="<%# Eval("CurSetlayoutValue") %>" />     
                                                    <input type="hidden" id="conforgID<%# Eval("xparentId") %>" value='<%# Eval("conforgID") %>' /><%--FB 2646--%>
													<input type="hidden" id="PartyListCount<%# Eval("xparentId") %>" value="<%# Eval("partCount") %>" />    <%--FB 2652--%> 
                                                    <%--ZD 100602 Starts--%>
                                                    <input type="hidden" id="confStartDate<%# Eval("xparentId") %>" value="<%# Eval("confBufferStartTime") %>" />
                                                    <input type="hidden" id="confEndTime<%# Eval("xparentId") %>" value="<%# Eval("confBufferEndTime") %>" />
                                                    <input type="hidden" id="confTimezone<%# Eval("xparentId") %>" value="<%# Eval("confTimezone") %>" />
                                                    <input type="hidden" id="confServiceType<%# Eval("xparentId") %>" value="<%# Eval("confServiceType") %>" />
                                                    <%--ZD 100602 End--%>
                                                    <input type="hidden" id="hdnCodianMCUType<%# Eval("xparentId") %>" value="<%# Eval("confCodianMcuType") %>" /><%--ZD 102057--%>
                                                    
                                                    <img src="../image/MonitorMCU/type_<%# Eval("ConftypeStatus") %>.gif" title="<asp:Literal Text='<%$ Resources:WebResources, AudioandVideo%>' runat='server' />" id="Av<%# Eval("xparentId") %>" alt="Audio Video" width="25px" /> <%--FB 2441 II--%> <%--ZD 100419--%>
                                                    <%-- FB 2652 Starts--%>
                                                    <img src="../image/MonitorMCU/MuteAll_0.png" class="MuteAll" style="cursor:pointer;visibility:<%# (Eval("confMuteAllExcept").ToString() == "1")?"visible":"hidden" %>" " title="<asp:Literal Text='<%$ Resources:WebResources, Muteallexcept%>' runat='server' />" id="<%# Eval("xparentId") %>"   alt="<asp:Literal Text='<%$ Resources:WebResources, MuteAll%>' runat='server' />" width="25px" height="25px" /> <%--ZD 100419--%>
                                                    <img src="../image/MonitorMCU/MuteAll_1.png" class="UnMuteAll" title="<asp:Literal Text='<%$ Resources:WebResources, Unmuteallexcept%>' runat='server' />" id="<%# Eval("xparentId") %>"  style="cursor:pointer;visibility:<%# (Eval("confUnmuteAll").ToString() == "1")?"visible":"hidden" %>"  alt="<asp:Literal Text='<%$ Resources:WebResources, UnMuteAll%>' runat='server' />" width="25px" height="25px" />  <%--ZD 100419--%>
                                                    <%--FB 2652 Ends--%>
                                                    <a href='' onclick='this.childNodes[0].click();return false;'><img src="../image/MonitorMCU/confcall_1.gif" class="GridEvents" style="cursor:pointer;" title="<asp:Literal Text='<%$ Resources:WebResources, ConnectAll%>' runat='server' />" id="call<%# Eval("xparentId") %>" alt="<asp:Literal Text='<%$ Resources:WebResources, ConnectAll%>' runat='server' />" width="25px" /></a>  <%--ZD 100419--%>
                                                    <a href='' onclick='this.childNodes[0].click();return false;'><img src="../image/MonitorMCU/confcall_0.gif" class="GridEvents" style="cursor:pointer;" title="<asp:Literal Text='<%$ Resources:WebResources, DisconnectAll%>' runat='server' />" id="call<%# Eval("xparentId") %>" alt="<asp:Literal Text='<%$ Resources:WebResources, DisconnectAll%>' runat='server' />" width="25px" /></a>  <%--ZD 100419--%> <%--ZD 101043--%>
                                                    <img src="../image/MonitorMCU/addUser.gif" class="addUser" style="cursor:pointer; visibility:<%# (Eval("ConflockStatus").ToString() != "1")?"visible":"hidden" %>" title="<asp:Literal Text='<%$ Resources:WebResources, AddParty%>' runat='server' />" id="addUser<%# Eval("xparentId") %>" alt="<asp:Literal Text='<%$ Resources:WebResources, AddParty%>' runat='server' />" width="25px" /> <%--ZD 100419--%>                                                   
                                                    <img src="../image/MonitorMCU/conflock_<%# Eval("ConflockStatus") %>.gif" style="cursor:pointer; visibility:<%# (Eval("ConfLockVisibleStatus2").ToString() == "1")?"visible":"hidden" %>" title="<asp:Literal Text='<%$ Resources:WebResources, Lockstatus%>' runat='server' />" class="GridEvents" id="lock<%# Eval("xparentId") %>" alt="<asp:Literal Text='<%$ Resources:WebResources, Lock%>' runat='server' />" width="25px" /> <%--ZD 100419--%> <%--ZD 100685  --%>                                                                                                     
                                                    <img src="../image/MonitorMCU/confmessage_1.gif" title="<asp:Literal Text='<%$ Resources:WebResources, MonitorMCU_SendMessage%>' runat='server' />" alt="<asp:Literal Text='<%$ Resources:WebResources, MonitorMCU_SendMessage%>' runat='server' />" style="cursor:pointer; visibility:<%# (Eval("ConfMessageVisibleStatus2").ToString() == "1")?"visible":"hidden" %>" class="GridEvents" id="message<%# Eval("xparentId") %>" width="25px"/>
                                                    <img src="../image/MonitorMCU/confaudioTx_<%# Eval("ConfAudioTxStatus") %>.gif" style="cursor:pointer; visibility:<%# (Eval("ConfAudioTXVisibleStatus2").ToString() == "1")?"visible":"hidden" %>" class="GridEvents" title="<asp:Literal Text='<%$ Resources:WebResources, MutereceiveaudioTx%>' runat='server' />" id="audioTx<%# Eval("xparentId") %>" alt="audioTx" width="25px" /><%--ZD 100419--%>  
                                                    <img src="../image/MonitorMCU/confaudioRx_<%# Eval("ConfAudioRxStatus") %>.gif" class="GridEvents" style="cursor:pointer; visibility:<%# (Eval("ConfAudioRXVisibleStatus2").ToString() == "1")?"visible":"hidden" %>" title="<asp:Literal Text='<%$ Resources:WebResources, MutereceiveaudioRx%>' runat='server' />" id="audioRx<%# Eval("xparentId") %>" alt="audioRx" width="25px" /><%--ZD 100419--%>  
                                                    <img src="../image/MonitorMCU/confvideoTx_<%# Eval("ConfVideoTxStatus") %>.gif" title="<asp:Literal Text='<%$ Resources:WebResources, MuteVideoTx%>' runat='server' />" class="GridEvents" style="cursor:pointer; visibility:<%# (Eval("ConfVideoTXVisibleStatus2").ToString() == "1")?"visible":"hidden" %>" id="videoTx<%# Eval("xparentId") %>" alt="videoTx" width="25px" /><%--ZD 100419--%>  
                                                    <img src="../image/MonitorMCU/confvideoRx_<%# Eval("ConfVideoRxStatus") %>.gif" title="<asp:Literal Text='<%$ Resources:WebResources, MuteVideoRx%>' runat='server' />" class="GridEvents" style="cursor:pointer; visibility:<%# (Eval("ConfVideoRXVisibleStatus2").ToString() == "1")?"visible":"hidden" %>" id="videoRx<%# Eval("xparentId") %>" alt="videoRx" width="25px" /><%--ZD 100419--%>  
                                                    <a href='' onclick='this.childNodes[0].click();return false;' style="visibility:<%# (Eval("confExtendTime").ToString() == "1")?"visible":"hidden" %>" ><img src="../image/MonitorMCU/time_1.gif" title="<asp:Literal Text='<%$ Resources:WebResources, point2point_ExtendTime%>' runat='server' />" class="GridEvents" style="cursor:pointer;  visibility:<%# (Eval("confExtendTime").ToString() == "1")?"visible":"hidden" %>" id="time<%# Eval("xparentId") %>" alt="<asp:Literal Text='<%$ Resources:WebResources, EventLog_Time%>' runat='server' />" width="25px" /></a>
                                                    <a href='' onclick='this.childNodes[0].click();return false;'><img src="../image/MonitorMCU/confdelete_1.gif" title="<asp:Literal Text='<%$ Resources:WebResources, DashBoard_LinkButton4%>' runat='server' />" class="GridEvents" style="cursor:pointer;" id="delete<%# Eval("xparentId") %>" alt="<asp:Literal Text='<%$ Resources:WebResources, ExpressConference_btnGuestRoomDelete%>' runat='server' />" width="25px" /></a>
                                                    <img src="../image/MonitorMCU/conflayout_0.gif" style="visibility:<%# (Eval("ConfLayoutVisibleStatus2").ToString() == "1")?"visible":"hidden" %>" title="<asp:Literal Text='<%$ Resources:WebResources, Layout%>' runat='server' />" class="layout" id="<%# Eval("xparentId") %>" alt="<asp:Literal Text='<%$ Resources:WebResources, Layout%>' runat='server' />" width="25px" /><%--ZD 100419--%> 
                                                    <%--<img src="../image/MonitorMCU/camera_0.gif" style="visibility:<%# (Eval("ConfCameraVisibleStatus2").ToString() == "1")?"visible":"hidden" %>" title="Camera" class="GridEvents" id="camera<%# Eval("xparentId") %>" alt="" width="25px" />--%> 
                                                    <img src="../image/MonitorMCU/packet_<%# Eval("confpacketlossStatus") %>.gif" style="visibility:<%# (Eval("ConfPacketlossVisibleStatus2").ToString() == "1")?"visible":"hidden" %>" class="GridEvents" id="packet<%# Eval("xparentId") %>" alt="<asp:Literal Text='<%$ Resources:WebResources, Layout%>' runat='server' />" width="25px" /><%--ZD 100419--%>  
                                                    <img src="../image/MonitorMCU/confrecord_<%# Eval("confrecordStatus") %>.gif" style="visibility:<%# (Eval("ConfRecordVisibleStatus2").ToString() == "1")?"visible":"hidden" %>" class="GridEvents" id="record<%# Eval("xparentId") %>" alt="<asp:Literal Text='<%$ Resources:WebResources, Recording%>' runat='server' />" width="25px" title="Recording" /><%--ZD 100419--%>   <%--ZD 100685--%>
                                                    <a href='' onclick='this.childNodes[0].click();return false;'><img src="../image/MonitorMCU/eventLog_1.gif" title="<asp:Literal Text='<%$ Resources:WebResources, EventLog_EventLogs%>' runat='server' />" class="EventLog" style="cursor: pointer;" id="eventLog<%# Eval("xparentId") %>" alt="<asp:Literal Text='<%$ Resources:WebResources, EventLog_EventLogs%>' runat='server' />" width="25px" /></a>  <%--FB 2501 Dec7--%><%--ZD 100419--%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField ItemStyle-BackColor="#D4D7DA" ItemStyle-ForeColor="#000000">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td colspan="100%" id="Grand_<%# Container.DataItemIndex +1 %>_child_<%# Container.DataItemIndex +1 %>">
                                                            <div id="divid-<%# Eval("confId") %><%# ((string)Eval("xconfName")).Replace("'","").Replace("\"","").Replace("\\","") %><%# ((string)Eval("confName")).Replace("'","").Replace("\"","").Replace("\\","") %>" style="display: none; position: relative;"><%--ZD 100940--%><%--ZD 102784--%>
                                                                <table width="100%" style="border-collapse:collapse"><tr><td> <%-- ZD 100627 ZD 100813 --%>
                                                                    <asp:PlaceHolder ID="plhStreamImages" runat="server"></asp:PlaceHolder>
                                                                </td></tr><tr><td>
                                                                <asp:GridView ID="grdChild2" CellPadding="1" runat="server" AutoGenerateColumns="false" Width="97%"
                                                                    HeaderStyle-BackColor="#003333" HeaderStyle-ForeColor="white" HorizontalAlign="Right"
                                                                    GridLines="None" ShowHeader="false">
                                                                    <Columns>
                                                                        <asp:TemplateField ItemStyle-BackColor="#AED2F2" ItemStyle-ForeColor="#000000" ItemStyle-Width="5%"><%--ZD 101655--%>
                                                                            <ItemTemplate>
                                                                                <asp:ImageButton ID="participant" ToolTip="<%$ Resources:WebResources, ManageCustomAttribute_chkParty%>" runat="server" ImageUrl="~/image/MonitorMCU/participant.gif" Width="25px" AlternateText="<asp:Literal Text='<%$ Resources:WebResources, MonitorMCUparticipant%>' runat='server' />" /> <%--ZD 100419--%>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>                                                                        
                                                                        <asp:BoundField DataField="participantName" HeaderText="Name" ItemStyle-BackColor="#AED2F2" ItemStyle-ForeColor="#000000" ItemStyle-Width="20%"></asp:BoundField>
                                                                        <asp:BoundField DataField="partAddr" HeaderText="partAddr" ItemStyle-BackColor="#AED2F2" ItemStyle-ForeColor="#000000" ItemStyle-Width="17%"></asp:BoundField><%--ZD 101655--%>                                                                                                                                           
                                                                        <asp:TemplateField ItemStyle-BackColor="#AED2F2" ItemStyle-ForeColor="#000000" ItemStyle-Width="60%" ItemStyle-HorizontalAlign="Right" >
                                                                            <ItemTemplate>                                                                                                                                                           
																				<input type="hidden" id="participantName<%# Eval("xchildId") %>" value="<%# Eval("participantName")%>" /> <%--FB 2652--%>
                                                                                <input type="hidden" id="partUserId<%# Eval("xchildId") %>" value="<%=Session["UserID"]%>" />
                                                                                <input type="hidden" id="partEndpointID<%# Eval("xchildId") %>" value="<%# Eval("conPartyId") %>" />
                                                                                <input type="hidden" id="partTerminalType<%# Eval("xchildId") %>" value="<%# Eval("conTerminalType") %>" />
                                                                                <input type="hidden" id="confId<%# Eval("xchildId") %>" value="<%# Eval("mergStr") %>" />  
                                                                                <input type="hidden" id="mID<%# Eval("xchildId") %>" value="<%# Eval("bridID") %>" />  
                                                                                <input type="hidden" id="hdnSetlayoutVal<%# Eval("xchildId") %>" value="<%# Eval("LayoutValue") %>" />  
                                                                                <input type="hidden" id="hdnCodianMCUType<%# Eval("xchildId") %>" value="<%# Eval("partyCodianMcuType") %>" /> <%--ZD 102057--%>
                                                                                
                                                                                <img src="../image/MonitorMCU/TerminalStaus_<%# Eval("partyCallStatusinfo") %>.gif" style="padding-right:30px;" title="" class="GridEvents" alt="<asp:Literal Text='<%$ Resources:WebResources, TerminalStaus%>' runat='server' />" width="25px" />
																				<img src="../image/MonitorMCU/activeSpeaker_<%# Eval("partyactiveSpeaker") %>.gif" style="padding-right:30px;visibility:<%# (Eval("partyCallStatus").ToString() != "1")?"visible":"hidden" %>" title="<asp:Literal Text='<%$ Resources:WebResources, ActiveSpeaker%>' runat='server' />" class="GridEvents" alt="" width="25px" />  <%--ZD 100174--%>                                                                              
                                                                                <img src="../image/MonitorMCU/call_<%# Eval("partyCallStatus") %>.gif" style="cursor:pointer; visibility:<%# (Eval("partyconnectionType").ToString() == "1")?"visible":"hidden" %>" title="" class="GridEvents" id="call<%# Eval("xchildId") %>" alt="<asp:Literal Text='<%$ Resources:WebResources, call%>' runat='server' />" width="25px" /><%-- ZD 101146 ZD 101192 --%>
                                                                                <img src="../image/MonitorMCU/message_1.gif" title="<asp:Literal Text='<%$ Resources:WebResources, MonitorMCU_SendMessage%>' runat='server' />" style="cursor:pointer; visibility:<%# (Eval("partymessage3").ToString() == "1" && Eval("partyCallStatus").ToString() != "1")?"visible":"hidden" %>" class="GridEvents" id="message<%# Eval("xchildId") %>" alt="<asp:Literal Text='<%$ Resources:WebResources, approvalstatus_Message%>' runat='server' />" width="25px" />
                                                                                <img src="../image/MonitorMCU/audioTx_<%# Eval("partyAudioTxStatus") %>.gif" style="cursor:pointer; visibility:<%# (Eval("partyAudioTx3").ToString() == "1" && Eval("partyCallStatus").ToString() != "1")?"visible":"hidden" %>" class="GridEvents" title="<asp:Literal Text='<%$ Resources:WebResources, MutereceiveaudioTx%>' runat='server' />" id="audioTx<%# Eval("xchildId") %>" alt="audioTx" width="25px" />
                                                                                <img src="../image/MonitorMCU/audioRx_<%# Eval("partyAudioRxStatus") %>.gif" class="GridEvents" style="cursor:pointer; visibility:<%# (Eval("partyAudioRx3").ToString() == "1" && Eval("partyCallStatus").ToString() != "1")?"visible":"hidden" %>" title="<asp:Literal Text='<%$ Resources:WebResources, MutereceiveaudioRx%>' runat='server' />" id="audioRx<%# Eval("xchildId") %>" alt="audioRx" width="25px" />
                                                                                <img src="../image/MonitorMCU/videoTx_<%# Eval("partyVideoTxStatus") %>.gif" title="<asp:Literal Text='<%$ Resources:WebResources, MuteVideoTx%>' runat='server' />" class="GridEvents" style="cursor:pointer; visibility:<%# (Eval("partyVideoTx3").ToString() == "1" && Eval("partyCallStatus").ToString() != "1")?"visible":"hidden" %>" id="videoTx<%# Eval("xchildId") %>" alt="videoTx" width="25px" />
                                                                                <img src="../image/MonitorMCU/videoRx_<%# Eval("partyVideoRxStatus") %>.gif" title="<asp:Literal Text='<%$ Resources:WebResources, MuteVideoRx%>' runat='server' />" class="GridEvents" style="cursor:pointer; visibility:<%# (Eval("partyVideoRx3").ToString() == "1" && Eval("partyCallStatus").ToString() != "1")?"visible":"hidden" %>" id="videoRx<%# Eval("xchildId") %>" alt="videoRx" width="25px" />
                                                                                <img src="../image/MonitorMCU/meter.gif" title="<asp:Literal Text='<%$ Resources:WebResources, PacketInfo%>' runat='server' />" class="bandwidthmeter" style="cursor:pointer; visibility:<%# (Eval("partypacketloss3").ToString() == "1" && Eval("partyCallStatus").ToString() != "1")?"visible":"hidden" %>" id="meter<%# Eval("xchildId") %>" alt="<asp:Literal Text='<%$ Resources:WebResources, PacketInfo%>' runat='server' />" width="25px" />
                                                                                <a href='' onclick='this.childNodes[0].click();return false;'><img src="../image/MonitorMCU/delete_1.gif" title="<asp:Literal Text='<%$ Resources:WebResources, ResponseConference_btnRemoveImg1%>' runat='server' />" class="GridEvents" style="cursor:pointer;" id="delete<%# Eval("xchildId") %>" alt="<asp:Literal Text='<%$ Resources:WebResources, RoomSearch_DelGuestRoom%>' runat='server' />" width="25px" /></a> <%--ZD 100419--%>
                                                                                <img src="../image/MonitorMCU/layout_0.gif" style="visibility:<%# (Eval("partyLayout3").ToString() == "1" && Eval("partyCallStatus").ToString() != "1")?"visible":"hidden" %>" title="<asp:Literal Text='<%$ Resources:WebResources, Layout%>' runat='server' />" class="layout" id="<%# Eval("xchildId") %>" alt="<asp:Literal Text='<%$ Resources:WebResources, Layout%>' runat='server' />" width="25px" /><%--ZD 100419 --%>
                                                                                <img src="../image/MonitorMCU/camera_0.gif" style="visibility:<%# (Eval("partycamera3").ToString() == "1" && Eval("partyCallStatus").ToString() != "1")?"visible":"hidden" %>" title="<asp:Literal Text='<%$ Resources:WebResources, Camera%>' runat='server' />" class="GridEvents" id="camera<%# Eval("xchildId") %>" alt="<asp:Literal Text='<%$ Resources:WebResources, Camera%>' runat='server' />" width="25px" /> <%--ZD 100419 --%>
                                                                                <img src="../image/MonitorMCU/record_<%# Eval("partyrecordStatus") %>.gif" style="visibility:<%# (Eval("partyrecord3").ToString() == "1" && Eval("partyCallStatus").ToString() != "1")?"visible":"hidden" %>" class="GridEvents" id="record<%# Eval("xchildId") %>" alt="<asp:Literal Text='<%$ Resources:WebResources, Recording%>' runat='server' />" width="25px" title="<asp:Literal Text='<%$ Resources:WebResources, Recording%>' runat='server' />" />  <%--ZD 100419 --%>                                                                             
                                                                                <img src="../image/MonitorMCU/LectureMode_<%# Eval("partylecturemode") %>.gif" style="visibility:<%# (Eval("partylecturemode3").ToString() == "1" && Eval("partyCallStatus").ToString() != "1")?"visible":"hidden" %>" title="<asp:Literal Text='<%$ Resources:WebResources, Transcoding1_LectureMode%>' runat='server' />" class="GridEvents" alt="<asp:Literal Text='<%$ Resources:WebResources, LectureMode%>' runat='server' />" width="25px" id="LectureMode<%# Eval("xchildId") %>" />  <%--FB 2553-RMX--%>
                                                                                <img src="../image/MonitorMCU/LeaderParty_<%# Eval("partychairperson") %>.gif" style="visibility:<%# (Eval("partyLeader").ToString() == "1" && Eval("partyCallStatus").ToString() != "1")?"visible":"hidden" %>" title="<asp:Literal Text='<%$ Resources:WebResources, LeaderParty%>' runat='server' />" class="GridEvents" alt="<asp:Literal Text='<%$ Resources:WebResources, LeaderParty%>' runat='server' />" width="25px" id="LeaderParty<%# Eval("xchildId") %>" />  <%--FB 2553-RMX--%>                      
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                                </td></tr></table>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>



            </div>





    <div style="display: none">
        <input type="button" id="btnRefreshPage" onclick="refPage();" /></div>        
    <input type="hidden" id="msgPopupIdentificationID" />
    <input type="hidden" id="AddUserWindowRedirect" value="" />
    
    <%--Message and Duration Popup window--%>  
    <div id="popmsg" title="MyVRM" style="left: 425px; position: absolute; background-color: White;
        top: 420px; z-index: 9999; width: 350px; height:320px; display: none;"><%--ZD 100671--%>
        <table border="0" cellpadding="0" cellspacing="0" style="background-color:" width="100%" style="height:320px;" id="tblpopup">
            <tr style="height: 25px;" id="smsg">
                <td style="background-color: #3075AE;" colspan="2">
                    <b style="font-size: small; margin-left: 10px; color: White; font-family: Verdana;
                        font-style: normal;"><asp:Literal Text="<%$ Resources:WebResources, MonitorMCU_SendMessage%>" runat="server"></asp:Literal></b>
                </td>
            </tr>
            <tr style="height: 25px;" id="eTime">
                <td style="background-color: #3075AE;" colspan="2">
                    <b style="font-size: small; margin-left: 10px; color: White; font-family: Verdana;
                        font-style: normal;"><asp:Literal Text="<%$ Resources:WebResources, MonitorMCU_ExtendConferen%>" runat="server"></asp:Literal></b> <%--Tamil1--%>
                </td>
            </tr>
             <tr style="height: 25px;" id="eCamera">
                <td style="background-color: #3075AE;" colspan="2">
                    <b style="font-size: small; margin-left: 10px; color: White; font-family: Verdana;
                        font-style: normal;"><asp:Literal Text="<%$ Resources:WebResources, MonitorMCU_CameraDirectio%>" runat="server"></asp:Literal></b>
                </td>
            </tr>
            <tr class="pMsg">
                <td style="padding-left: 10px;">                    
                    <label><asp:Literal ID="Literal1" Text='<%$ Resources:WebResources, ResponseConference_Comments%>' runat='server' />:</label>
                </td>
                <td align="left">
                    <span id="username_warning" class="fontdisplay"></span><br />
                    <textarea id="popuptxt" class="altText" rows="5" cols="10"></textarea>
                </td>
            </tr>
            <tr class="pMsgDuration" >
                <td style="padding-left: 10px;">
                    <label><asp:Literal ID="Literal2" Text='<%$ Resources:WebResources, ResponseConference_Duration%>' runat='server' />:</label>
                </td>
                <td align="left">                
                    <input type="text" id="msgMinutes" class="altText" style="width:100px;"/>                    
                    <label><asp:Literal ID="Literal3" Text='<%$ Resources:WebResources, ExpressConference_mins%>' runat='server' /></label>                    
                </td>
            </tr> <%--FB 2981--%>
            <tr class="pSendMsgDuration" >
                <td style="padding-left: 10px;">
                    <label><asp:Literal ID="Literal4" Text='<%$ Resources:WebResources, ResponseConference_Duration%>' runat='server' />:</label>
                </td>
                <td align="left">                
                    <input type="text" id="msgSec" class="altText" style="width:100px;"/>                    
                    <label><asp:Literal ID="Literal5" Text='<%$ Resources:WebResources, Secs%>' runat='server' /></label>                    
                </td>
            </tr>
            <tr style="height: 40px;" class="pMsg">
                <td style="padding-left: 10px;">
                <label><asp:Literal ID="Literal6" Text='<%$ Resources:WebResources, Direction%>' runat='server' />:</label>                    
                </td>
                <td align="left">
                    <select id="direction" class="altSelectFormat" style="width:100px;">
                        <option value="1" selected="selected"><asp:Literal Text='<%$ Resources:WebResources, Top%>' runat='server' /></option>
                        <option value="2"><asp:Literal Text='<%$ Resources:WebResources, Middle%>' runat='server' /></option>
                        <option value="3"><asp:Literal Text='<%$ Resources:WebResources, Bottom%>' runat='server' /></option>
                    </select>
                </td>
            </tr>
            <%--ZD 100627 Start--%>
            <tr class="pCamera">
                <td align="left">
                   <div><br />
                        <label>&nbsp;&nbsp;&nbsp;<asp:Literal Text='<%$ Resources:WebResources, CameraMovement%>' runat='server' /></label>
                   </div>
                   <table align="center">
                        <tr>
                            <td colspan="2" style="text-align:center">
                                <img src="../image/MonitorMCU/camDirection_1.gif" title="<asp:Literal Text='<%$ Resources:WebResources, CameramoveUp%>' runat='server' />" class="CameraEvents" alt="<asp:Literal Text='<%$ Resources:WebResources, moveUp%>' runat='server' />" width="40px" id="cam1" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <img style="padding-right:25px" src="../image/MonitorMCU/camDirection_3.gif" title="<asp:Literal Text='<%$ Resources:WebResources, cameramoveLeft%>' runat='server' />" class="CameraEvents" alt="<asp:Literal Text='<%$ Resources:WebResources, moveLeft%>' runat='server' />" width="40px" id="cam3" />
                            </td>
                            <td>
                                <img style="padding-left:25px" src="../image/MonitorMCU/camDirection_4.gif" title="<asp:Literal Text='<%$ Resources:WebResources, cameramoveright%>' runat='server' />" class="CameraEvents" alt="<asp:Literal Text='<%$ Resources:WebResources, moveRight%>' runat='server' />" width="40px" id="cam4" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align:center">
                                <img src="../image/MonitorMCU/camDirection_2.gif" title="<asp:Literal Text='<%$ Resources:WebResources, cameramovedown%>' runat='server' />" class="CameraEvents" alt="<asp:Literal Text='<%$ Resources:WebResources, moveDown%>' runat='server' />" width="40px" id="cam2" />
                            </td>
                        </tr>
                    </table>
                    <div><br />
                         <label>&nbsp;&nbsp;&nbsp;<asp:Literal ID="Literal7" Text='<%$ Resources:WebResources, camerazoom%>' runat='server' /></label>
                    </div>                    
                    <table align="center">
                        <tr>
                            <td>
                                <img src="../image/MonitorMCU/camDirection_5.gif" title="<asp:Literal Text='<%$ Resources:WebResources, camerazoomin%>' runat='server' />" class="CameraEvents" alt="<asp:Literal Text='<%$ Resources:WebResources, zoomin%>' runat='server' />" width="40px" id="cam5" />
                            </td>
                            <td>
                                <img src="../image/MonitorMCU/camDirection_6.gif" title="<asp:Literal Text='<%$ Resources:WebResources, camerazoomout%>' runat='server' />" class="CameraEvents" alt="<asp:Literal Text='<%$ Resources:WebResources, ZoomOut%>' runat='server' />" width="40px" id="cam6" />
                            </td>
                        </tr>
                    </table>
                    <div><br />
                         <label>&nbsp;&nbsp;&nbsp;<asp:Literal ID="Literal8" Text='<%$ Resources:WebResources, camerafocus%>' runat='server' /></label>
                    </div>                       
                    <table align="center">
                        <tr>
                            <td>
                                <img src="../image/MonitorMCU/camDirection_7.gif" title="<asp:Literal Text='<%$ Resources:WebResources, camerafocusin%>' runat='server' />" class="CameraEvents" alt="<asp:Literal Text='<%$ Resources:WebResources, focusin%>' runat='server' />" width="40px" id="cam7" />
                            </td>
                            <td>
                                <img src="../image/MonitorMCU/camDirection_8.gif" title="<asp:Literal Text='<%$ Resources:WebResources, camerafocusout%>' runat='server' />" class="CameraEvents" alt="<asp:Literal Text='<%$ Resources:WebResources, focusout%>' runat='server' />" width="40px" id="cam8" />
                            </td>
                        </tr>
                   </table>
                   <br /><br />
                </td>
            </tr>
            <%--ZD 100627 End--%>
            <tr style="height: 40px;">
                <td align="center" colspan="2">
                    <div id="popupstatus" style="display: none;">
                    </div>                    
                    <button id="btnpopupcancel" class="altMedium0BlueButtonFormat"><asp:Literal Text='<%$ Resources:WebResources, ManageCustomAttribute_btnCancel%>' runat='server' /></button> &nbsp;&nbsp;
                    <button id="btnpopupSubmit" class="altMedium0BlueButtonFormat"><asp:Literal Text='<%$ Resources:WebResources, DashBoard_SendMsg%>' runat='server' /></button>
                </td>
            </tr>
            <tr>
                <td height="20px;">
                    
                </td>
            </tr>
        </table>
    </div>  
    <%--Bandwidth Information Popup Window--%>
    <div id="BandWidth" title="MyVRM" style="left: 425px; position: absolute; background-color: White;
        top: 420px; z-index: 9999; width: 350px; height: 400px; display: none;"><%--ZD 100671--%>        
                <table style='height: 370px;' border='0' cellpadding='0' cellspacing='0' width='100%' id='Table1'>
                <tr style='height: 25px;'><td style='background-color: #3075AE;' colspan='2' align='center'><b style='font-size: small; margin-left: 10px; color: White; font-family: Verdana; font-style: normal;'><asp:Literal Text="<%$ Resources:WebResources, MonitorMCU_PacketDetails%>" runat="server"></asp:Literal></b></td></tr>                
                <tr style='height: 40px;'><td style='padding-left: 10px; width:50%;'><b class='fontdisplay'><asp:Literal Text="<%$ Resources:WebResources, MonitorMCU_AudioPacketSent%>" runat="server"></asp:Literal></b></td><td align='left'><b> : </b><span class='fontdisplay' id="AudioPacketSent"></span></td></tr> <%--ZD 100632--%> 
                <tr style='height: 40px;'><td style='padding-left: 10px; width:50%;'><b class='fontdisplay'><asp:Literal Text="<%$ Resources:WebResources, MonitorMCU_AudioPacketRece%>" runat="server"></asp:Literal></b></td><td align='left'><b> : </b><span class='fontdisplay' id="AudioPacketReceived"></span></td></tr>
                <tr style='height: 40px;'><td style='padding-left: 10px; width:50%;'><b class='fontdisplay'><asp:Literal Text="<%$ Resources:WebResources, MonitorMCU_AudioPacketErro%>" runat="server"></asp:Literal></b></td><td align='left'><b> : </b><span class='fontdisplay' id="AudioPacketError"></span></td></tr>
                <tr style='height: 40px;'><td style='padding-left: 10px; width:50%;'><b class='fontdisplay'><asp:Literal Text="<%$ Resources:WebResources, MonitorMCU_AudioPacketMiss%>" runat="server"></asp:Literal></b></td><td align='left'><b> : </b><span class='fontdisplay' id="AudioPacketMissing"></span></td></tr>
                <tr style='height: 40px;'><td style='padding-left: 10px; width:50%;'><b class='fontdisplay'><asp:Literal Text="<%$ Resources:WebResources, MonitorMCU_VideoPacketSent%>" runat="server"></asp:Literal></b></td><td align='left'><b> : </b><span class='fontdisplay' id="VideoPacketSent"></span></td></tr> <%--ZD 100632--%> 
                <tr style='height: 40px;'><td style='padding-left: 10px; width:50%;'><b class='fontdisplay'><asp:Literal Text="<%$ Resources:WebResources, MonitorMCU_VideoPacketErro%>" runat="server"></asp:Literal></b></td><td align='left'><b> : </b><span class='fontdisplay' id="VideoPacketError"></span></td></tr>
                <tr style='height: 40px;'><td style='padding-left: 10px; width:50%;'><b class='fontdisplay'><asp:Literal Text="<%$ Resources:WebResources, MonitorMCU_VideoPacketRece%>" runat="server"></asp:Literal></b></td><td align='left'><b> : </b><span class='fontdisplay' id="VideoPacketReceived"></span></td></tr>
                <tr style='height: 40px;'><td style='padding-left: 10px; width:50%;'><b class='fontdisplay'><asp:Literal Text="<%$ Resources:WebResources, MonitorMCU_VideoPacketMiss%>" runat="server"></asp:Literal></b></td><td align='left'><b> : </b><span class='fontdisplay' id="VideoPacketMissing"></span></td></tr>                
                <tr style="height: 40px;"><td align="center" colspan="2"><button id="Cancelbandwidth" class="altMedium0BlueButtonFormat"><asp:Literal ID="Literal9" Text='<%$ Resources:WebResources, BridgeDetails_BtnClose%>' runat='server' /></button></td></tr>
                </table>
        </div>
    <%--Add User Popup with Jquery Wizard--%>
    <div id="PopupAddUser" class="rounded-corners" style="left: 425px; position: absolute;
    background-color: White; top: 420px; z-index: 9999; height: 500px; overflow:hidden; border: 0px;
    width: 700px; display: none;"><%--ZD 100591--%>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr style="height: 25px;">
            <td style="background-color: #3075AE;">
                <b style="font-size: small; padding-left: 15px; color: White; font-family: Verdana;
                    font-style: normal;"><asp:Literal Text="<%$ Resources:WebResources, AddParty%>" runat="server"></asp:Literal></b><%--ZD 102430--%>
            </td>
        </tr>    
    </table>        
    <input type="hidden" id="Hidden1" value="" />
    <iframe src="" id="addListUser" name="addListUser" style="height: 530px; border: 0px; overflow:hidden; width: 700px; overflow: hidden;"></iframe>
</div>
    <%--Set Layout Popup window--%>
    <div id="PopupSetLayout" class="rounded-corners" style="width:80%; background-color: White;
    top: 320px; z-index: 9999; min-height: 200px; border: 0px; display: none;">
    <table border="0" cellpadding="0" cellspacing="0" width="100%"  class="setLayoutContentTable">
        <tr style="height: 25px;">
            <td style="background-color: #3075AE;">
                <b style="font-size: small; padding-left: 15px; color: White; font-family: Verdana;
                    font-style: normal;"><asp:Literal Text="<%$ Resources:WebResources, Layout%>" runat="server"></asp:Literal></b><%--ZD 102430--%>
            </td>
        </tr>
        <tr style="height:30px;">
            <td>
            </td>
        </tr>       
        <tr id="polycomID">
            <td>
                <table border="0" width="100%" cellpadding="0"> 
                    <tr>
                        <td colspan="4">
                        </td>
                        <td style="padding-right:20px;" colspan="2">
                            <b class="fontdisplay"><asp:Literal Text="<%$ Resources:WebResources, MonitorMCU_CurrentLayout%>" runat="server"></asp:Literal></b>
                        </td>
                        <td>
                            <img src="" style="width:70px; height:70px;"  alt="Current Layout" id="CurrentLayout" /><%--ZD 100419--%>
                            <label id="lblCurrentFamilyLO"  ></label> <%--ZD 101869--%>
                        </td>
                        <td colspan="2">
                            <b class="fontdisplay"><asp:Literal Text="<%$ Resources:WebResources, MonitorMCU_SelectedLayout%>" runat="server"></asp:Literal></b>
                        </td>                        
                        <td colspan="6">
                           <%-- <asp:Label ID="lblFamilyLO" runat="server"></asp:Label>--%>
                            <img src="" style="width:70px; height:70px;"  alt="" id="CurSelectedLayout" /><%--ZD 100419--%>
                            <label id="lblFamilyLO" class="FamilyLayoutcolor" ></label> <%--ZD 101869--%>
                        </td>
                    </tr>
                    <%--ZD 101869 start--%>
					<%--ZD 102057--%>  
                    <tr id="trCodianFamily" runat="server">
                      <td colspan ="15">
                         <table border ="0">
                            <tr>
                              <td valign="middle" align="left" class="blackblodtext"> <asp:Literal Text="<%$ Resources:WebResources, MonitorMCU_Defaultfamily%>" runat="server"></asp:Literal> </td>
                               <td valign="middle" align="left">
                                <%--<checkbox id="Chkdefault" ></checkbox>--%>
                                <input id="Chkdefault" type="checkbox" onclick="fnChkdefault();" style="margin-left:5px" />
                               </td>
                            </tr>
                            <tr>
                                <td valign="middle" align="left" class="blackblodtext"><asp:Literal  Text="<%$ Resources:WebResources, MonitorMCU_family1%>" runat="server"></asp:Literal></td>
                                <td>
                                    <div align="left" id="101" class="FamilyLayoutcolor"  style="width:225px; height:60px;">
                                        <img src="image/displaylayout/05.gif" style="width:50px; cursor:pointer; height:50px; margin-left:5px; margin-top:5px"  alt="Layout"> 
                                        <img src="image/displaylayout/06.gif" style="width:50px; cursor:pointer; height:50px"  alt="Layout">
                                        <img src="image/displaylayout/07.gif" style="width:50px; cursor:pointer; height:50px"  alt="Layout">
                                        <img src="image/displaylayout/08.gif" style="width:50px; cursor:pointer; height:50px"  alt="Layout">
                                    </div>
                                </td>
                            </tr>
                             <tr>
                                <td valign="middle" align="left" class="blackblodtext"><asp:Literal  Text="<%$ Resources:WebResources, MonitorMCU_family2%>" runat="server"></asp:Literal></td>
                                <td>
                                    <div class="FamilyLayoutcolor" id="102" align="left" style="width:60px; height:60px"  >
                                        <img src="image/displaylayout/01.gif" style="width:50px; cursor:pointer; height:50px; margin-left:5px; margin-top:5px"  alt="Layout" >
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td valign="middle" align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, MonitorMCU_family3%>" runat="server"></asp:Literal></td>
                                <td>
                                    <div class="FamilyLayoutcolor" id="103" align="left" style="width:60px; height:60px;" >
                                        <img src="image/displaylayout/02.gif" style="width:50px; cursor:pointer; height:50px; margin-left:5px; margin-top:5px"  alt="Layout" >
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td valign="middle" align="left" class="blackblodtext"> <asp:Literal Text="<%$ Resources:WebResources, MonitorMCU_family4%>" runat="server"></asp:Literal></td>
                                <td>
                                    <div align="left" id="104" class="FamilyLayoutcolor"  style="width:225px; height:60px;">
                                        <img src="image/displaylayout/02.gif" style="width:50px; cursor:pointer; height:50px; margin-left:5px; margin-top:5px"  alt="Layout">
                                        <img src="image/displaylayout/03.gif" style="width:50px; cursor:pointer; height:50px"  alt="Layout">
                                        <img src="image/displaylayout/04.gif" style="width:50px; cursor:pointer; height:50px"  alt="Layout">
                                        <img src="image/displaylayout/43.gif" style="width:50px; cursor:pointer; height:50px"  alt="Layout">
                                    </div>
                                </td>
                            </tr>
                             <tr>
                                <td valign="middle" align="left" class="blackblodtext"> <asp:Literal Text="<%$ Resources:WebResources, MonitorMCU_family5%>" runat="server"></asp:Literal> </td>
                                <td>
                                    <div class="FamilyLayoutcolor" id="105" align="left" style="width:60px; height:60px;" >
                                        <img src="image/displaylayout/25.gif" style="width:50px; cursor:pointer; height:50px; margin-left:5px; margin-top:5px"  alt="Layout" >
                                    </div>
                                </td>
                            </tr>

                            </table>
                          </td></tr>
                          
                          <%--ZD 101869 End--%>
                    <tr>            
                                <% string j; for (int i = 1; i <= 63; i++)
                           {
                               j = (i > 9) ? i.ToString() : "0" + i.ToString(); %>                                                       
                               
                        <td style="width:72px; height:72px;" class="LayoutColour" align="center">
                            <img src="image/displaylayout/<%= j %>.gif" style="width:50px; cursor:pointer; height:50px;" id="<%= j %>" alt="<asp:Literal Text='<%$ Resources:WebResources, ManageConference_DisplayLayout%>' runat='server' />" class="SelectLayout" /><%--ZD 100419--%>
                        </td>                       
                                           
                        <% if ((i % 15) == 0) { %> 
                           </tr><tr> 
                           <% } } %>                           
                           </tr>                           
                </table>
            </td>
        </tr>        
        <tr style="height:50px;">
            <td align="center">
                <button class="altMedium0BlueButtonFormat" id="SetLayoutCancel"><asp:Literal Text='<%$ Resources:WebResources, ManageCustomAttribute_btnCancel%>' runat='server' /></button>    
                <button class="altMedium0BlueButtonFormat" id="SetLayoutSubmit"><asp:Literal Text='<%$ Resources:WebResources, ManageDepartment_btnCreateNewDepartment%>' runat='server' /></button> 
            </td>
        </tr>
    </table>
</div>
    <%--loader gif animation--%>
    <div id="progressdiv">
        <div id="progressdivwindow" >        
                <table border='0' id="proceeimgid" cellpadding='0' cellspacing='0' style="padding-left:600px; display:none;">
                <tr style='height: 25px;'>
                    <td align="center">
                        <img src="" alt="" />
                    </td>                    
                    </tr>
                </table>
        </div> 
    </div>  
    
    <%--Event log FB 2501 Dec7 Start--%>
    
    <div id="diveventlog" class="rounded-corners" style="left: 425px; position: absolute;
        background-color: White; top: 420px; min-height:200px; z-index: 9999; overflow: hidden;
        border: 0px; width: 700px; display: none;">
        <table align="center" border="0" cellpadding="0"  cellspacing="0" width="100%" id="Table2">
            <tr style='height: 25px;'>
                <td colspan="3" style='background-color: #3075AE;' align='center'>
                    <b style='font-size: small; margin-left: 10px; color: White; font-family: Verdana;
                        font-style: normal;'><asp:Literal Text="<%$ Resources:WebResources, MonitorMCU_EventLogs%>" runat="server"></asp:Literal></b>
                </td>
             </tr>                        
             <tr>
                <td colspan="3" height="20px;"></td>
             </tr>
            <tr>
                <td colspan="3" align="center">
                    <div id="EventLogHtmlContent"></div>
                </td>
            </tr>           
            <tr style="height: 40px;">
                <td align="center" colspan="3">
                    <button id="btnCancelEventLog" class="altMedium0BlueButtonFormat">
                        <asp:Literal Text='<%$ Resources:WebResources, MasterChildReport_btnClose%>' runat='server' /></button>
                </td>
            </tr>
        </table>        
    </div>
    
    <%--Event log FB 2501 Dec7 End--%>
    
    
    <%--FB 2652 Starts--%>
    <div id="PopupMuteAll" class="rounded-corners" style="left: 425px; position: absolute;
        background-color: White; top: 420px; min-height:200px; z-index: 9999; overflow: hidden;
        border: 0px; width: 400px; display: none;">
        <table align="center" border="0" cellpadding="0"  cellspacing="0" width="100%" id="Table3">
            <tr style='height: 25px;'>
                <td colspan="3" style='background-color: #3075AE;' align='center'>
                    <b style='font-size: small; margin-left: 10px; color: White; font-family: Verdana;
                        font-style: normal;'><asp:Literal Text="<%$ Resources:WebResources, MonitorMCU_MuteAllExcept%>" runat="server"></asp:Literal></b>
                </td>
             </tr>                        
             <tr>
                <td colspan="3" height="20px;"  ></td>
             </tr>
            <tr>
                <td colspan="3" align="center">
                    <div id="divMuteAll"></div>
                </td>
            </tr>           
            <tr style="height: 40px;">
            <td align="right" style="padding-right:10px">
                    <button id="btnMuteAllSubmit" class="altMedium0BlueButtonFormat">
                        <asp:Literal ID="Literal10" Text='<%$ Resources:WebResources, OrganisationSettings_btnSubmit%>' runat='server' /></button>
                </td>
                <td align="left" colspan="2">
                    <button id="btnMuteAllClose" class="altMedium0BlueButtonFormat">
                        <asp:Literal ID="Literal11" Text='<%$ Resources:WebResources, BridgeDetails_BtnClose%>' runat='server' /></button>
                </td>
            </tr>
        </table>        
    </div>
    <%--FB 2652 Ends--%>
    </ContentTemplate>
    </asp:UpdatePanel><%--ZD 101417 Ends--%>


            <%--ZD 101078 Start--%>

        <asp:GridView ID="grdGrand1" runat="server" AutoGenerateColumns="false"
            HorizontalAlign="Center" HeaderStyle-BackColor="#000033" HeaderStyle-ForeColor="White"
            GridLines="None" CellPadding="1" ShowHeader="false" BackColor="#F2F4F8">
            <Columns>
                <asp:TemplateField ItemStyle-BackColor="#3075AE" ItemStyle-ForeColor="#FFFFFF" ItemStyle-Width="8%">
                    <ItemTemplate>
                        <a href="javascript:showNestedGridView('divid-<%# Eval("nameid") %>2');">
                            <img id="imgdivid-<%# Eval("nameid") %>2" alt="<asp:Literal Text='<%$ Resources:WebResources, ShowHide%>' runat='server' />" border="0" src="image/loc/nolines_plus.gif" />
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/image/MonitorMCU/mcu.gif" Width="25px" AlternateText="<%$ Resources:WebResources, MonitorMCU_MonitorMCU%>" />
                            <asp:Label ID="lblMcuIP" Visible="false" Text='<%# Eval("ip") %>' runat="server"></asp:Label>    
                        </a>                                                
                    </ItemTemplate>
                </asp:TemplateField>
                <%--FB 2501 Dec10 Start--%>
                <asp:TemplateField ItemStyle-BackColor="#3075AE" ItemStyle-ForeColor="#FFFFFF" ItemStyle-Width="3%">
                    <ItemTemplate>
                        <span><asp:Literal ID="Literal12" Text='<%$ Resources:WebResources, MonitorMCU_Name%>' runat='server' /> </span>
                    </ItemTemplate>
                </asp:TemplateField>                 
                <asp:BoundField DataField="name" HeaderText="Name" ItemStyle-BackColor="#3075AE" ItemStyle-ForeColor="#FFFFFF" ItemStyle-Width="15%" ></asp:BoundField>
                <asp:TemplateField ItemStyle-BackColor="#3075AE" ItemStyle-ForeColor="#FFFFFF" ItemStyle-Width="3%">
                    <ItemTemplate>
                        <span><asp:Literal ID="Literal13" Text='<%$ Resources:WebResources, MonitorMCU_Silo%>' runat='server' /> </span>
                    </ItemTemplate>
                </asp:TemplateField>                 
                <asp:BoundField DataField="siloName" HeaderText="Silo" ItemStyle-BackColor="#3075AE" ItemStyle-ForeColor="#FFFFFF" ItemStyle-Width="15%" ></asp:BoundField>
                 <asp:TemplateField ItemStyle-BackColor="#3075AE" ItemStyle-ForeColor="#FFFFFF" ItemStyle-Width="5%">
                    <ItemTemplate>
                        <span><asp:Literal ID="Literal14" Text='<%$ Resources:WebResources, MonitorMCU_Address%>' runat='server' /> </span>
                    </ItemTemplate>
                </asp:TemplateField> 
                <asp:BoundField DataField="ip" HeaderText="IP"  ItemStyle-BackColor="#3075AE" ItemStyle-ForeColor="#FFFFFF" ItemStyle-Width="12%"></asp:BoundField>                
                 <asp:TemplateField ItemStyle-BackColor="#3075AE" ItemStyle-ForeColor="#FFFFFF" ItemStyle-Width="3%">
                    <ItemTemplate>
                        <span><asp:Literal ID="Literal15" Text='<%$ Resources:WebResources, MonitorMCU_Type%>' runat='server' /> </span>
                    </ItemTemplate>
                </asp:TemplateField> 
                <asp:BoundField DataField="type" HeaderText="Type" ItemStyle-BackColor="#3075AE" ItemStyle-ForeColor="#FFFFFF" ItemStyle-Width="17%"></asp:BoundField>                
                <asp:TemplateField ItemStyle-BackColor="#3075AE" ItemStyle-ForeColor="#FFFFFF" ItemStyle-Width="3%">
                    <ItemTemplate>
                        <span><asp:Literal ID="Literal16" Text='<%$ Resources:WebResources, MonitorMCU_Ports%>' runat='server' /> </span>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ItemStyle-BackColor="#3075AE" ItemStyle-ForeColor="#FFFFFF" ItemStyle-Width="5%">
                    <ItemTemplate>
                        <asp:Label ID="MCUTotalPort"  Text='<%# Eval("MCUTotalPort") %>' ToolTip="<%$ Resources:WebResources, AvailableTotal%>" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="MCUStatus" HeaderText="MCUStatus" ItemStyle-BackColor="#3075AE" ItemStyle-ForeColor="#FFFFFF" ItemStyle-Width="17%"></asp:BoundField>                
                <asp:TemplateField ItemStyle-BackColor="#3075AE" ItemStyle-ForeColor="#FFFFFF" ItemStyle-Width="3%">
                    <ItemTemplate>
                        <a href='' onclick='this.childNodes[0].click();return false;'><img src="../image/MonitorMCU/favourite_<%# Eval("favorite") %>.gif" title="<asp:Literal Text='<%$ Resources:WebResources, Favorite%>' runat='server' />" class="setfavorite" id="<%# Eval("mcuID") %>" width="25px"  alt="<asp:Literal Text='<%$ Resources:WebResources, Favorite%>' runat='server' />" /></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ItemStyle-BackColor="#D4D7DA" ItemStyle-ForeColor="#000000">
                    <ItemTemplate>
                        <tr>
                            <td colspan="100%" id="Grand_<%# Container.DataItemIndex +1 %>_child_<%# Container.DataItemIndex +1 %>">
                                <div id="divid-<%# Eval("nameid") %>2" style="display: none; position: relative;">
                                <table width="100%" style="border-collapse:collapse">
                                    <tr>
                                        <td>
                                            <iframe name='testiFrame' id='runtimeIFrame'  frameborder='0' scrolling='auto' height='400px' width='100%'  src='<%# Eval("MCUAccessURL") %>' style='left:0; background-color: beige;'></iframe> 
                                        </td>
                                    </tr>
                                    <tr><td></td></tr>
                                </table>
                                </div>  
                            </td>
                        </tr>
                    </ItemTemplate>
                  </asp:TemplateField>
                </Columns>
        </asp:GridView>

        <%--ZD 101078 End--%>
            <br />
            <br />
            <br />
            <br />
            <br /> 

                <div style="display: none">
                <asp:Button ID="btnPostBack" runat="server" /></div>        

	</form>
							
</body>
</html>
<%--Java SCript for refresh the page every 15 sec--%>
<script type="text/javascript">
    var timerEvent;
    function refPage() { //ZD 100980
        if (document.getElementById("communStatus") != null && document.getElementById("communStatus").value == "0") {
            //document.getElementById("btnRefreshPage").click(); // ZD 101417
            $('#PopupMuteAll').bPopup().detach();
            $('#popupdiv').remove();
            $('#PopupSetLayout').bPopup().detach();
            $('#popupdiv').remove();
            $('#PopupAddUser').bPopup().detach();
            $('#popupdiv').remove();
            $('#progressdivwindow').bPopup().detach();
            $('#popupdiv').remove();
            $('#BandWidth').bPopup().detach();
            $('#popupdiv').remove();
            $('#diveventlog').bPopup().detach();
            $('#popupdiv').remove();
            $('#popmsg').bPopup().detach();
            $('#popupdiv').remove();
            document.getElementById("refDiv").style.display = "block";
            __doPostBack('updtNonIframe', '');
        }
        else {
            clearTimeout(timerEvent);
            timerEvent = setTimeout("refPage()", 15000);
        }
//alert(document.getElementById("communStatus").value);
//document.getElementById("communStatus").value = "0"
}

function fnUpdateGridState()
{
var gridState = document.getElementById("hdnGridState");
var ids = gridState.value.split(",,"); // ZD 100988
var i = 0;
for(i = 0; i < ids.length-1; i++)
{
    if (document.getElementById(ids[i]) != null)
       document.getElementById(ids[i]).style.display = 'inline';
    if (document.getElementById("img" + ids[i]) != null)
       document.getElementById("img" + ids[i]).src = 'image/loc/nolines_minus.gif';
}
//setTimeout("refPage()", 15000); // ZD 101417
}
//fnUpdateGridState(); // ZD 101417

if(document.getElementById("grdGrand") != null) //ZD 101522
    document.getElementById("grdGrand").width = (screen.width - 25) + "px";
if(document.getElementById("grdGrand1") != null)
    document.getElementById("grdGrand1").width = (screen.width - 25) + "px";

function pageLoad(sender, args) { // ZD 101417
    fnUpdateGridState();
    
    clearTimeout(timerEvent);
    timerEvent = setTimeout("refPage()", 15000);


    if (args.get_isPartialLoad()) {
        document.getElementById("refDiv").style.display = "none";
        for (var y = 0; y < currentId.length; y++)
            clearInterval(currentId[y]);
        // Partial Postback
        while (document.getElementById("popupdiv") != null)
            $("popupdiv").remove();
    }

    $(document).ready(function () {

        $('.MuteAll').click(function () {
            fnMuteAll(this);
            return false;
        });

        $('.chkMuteAll').click(function () {
            fnchkMuteAll(this);
            return false;
        });

        $('#btnMuteAllSubmit').click(function () {
            fnbtnMuteAllSubmit();
            return false;
        });


        $('#btnMuteAllClose').click(function () {
            fnbtnMuteAllClose();
            return false;
        });

        $('.UnMuteAll').click(function () {
            fnUnMuteAll(this);
            return false;
        });

        fnTimerControl();

        $(".GridEvents").mouseover(function () {
            fnGridEvents(this);
            return false;
        });

        $('.layout').click(function () {
            fnlayout(this);
            return false;
        });

        $('.SelectLayout').click(function () {
            fnSelectLayout(this);
            return false;
        });

        $('#SetLayoutSubmit').click(function () {
            fnSetLayoutSubmit(this);
            return false;
        });

        $('#SetLayoutCancel').click(function () {
            fnSetLayoutCancel();
            return false;
        });

        $('.addUser').click(function () {
            fnaddUser(this);
            return false;
        });

        $('.altShortBlueButtonFormat').click(function () {
            fnaltShortBlueButtonFormat();
            return false;
        });

        /*
        $('.altLongBlueButtonFormat').click(function () {
        fnaltLongBlueButtonFormat();
        return false;
        });
        */
        //ZD 101869 start

        $('.FamilyLayoutcolor').mouseover(function (e) {
            fnFamilyLayoutcolor(this, e);
            return false;
        });

        
        $('.FamilyLayoutcolor').mouseout(function (e) {
            fnFamilyLayoutcolor(this, e);
            return false;
        });


        $('.FamilyLayoutcolor').click(function () {
            fnSelectFamilyLayout(this);
            return false;
        });

        //ZD 101869 End
        $('.LayoutColour').mouseover(function (e) {
            fnLayoutColour(this, e);
            return false;
        });

        $('.LayoutColour').mouseout(function (e) {
            fnLayoutColour(this, e);
            return false;
        });

        $(".setfavorite").click(function () {
            fnsetfavorite(this);
            return false;
        });

        $(".GridEvents").click(function () {
            fnGridEvents2(this);
            return false;
        });

        $('#btnpopupcancel').click(function () {
            fnbtnpopupcancel();
            return false;
        });

        $("#popuptxt").keypress(function () {
            return fnpopuptxt();
        });

        $("#btnpopupSubmit").click(function () {
            fnbtnpopupSubmit();
            return false;
        });

        $('.bandwidthmeter').click(function () {
            fnbandwidthmeter(this);
            return false;
        });

        $('#Cancelbandwidth').click(function () {
            fnCancelbandwidth();
            return false;
        });

        $('.EventLog').click(function () {
            fnEventLog(this);
            return false;
        });

        $('#btnCancelEventLog').click(function () {
            fnbtnCancelEventLog();
            return false;
        });

        $(".CameraEvents").click(function () {
            fnCameraEvents(this);
            return false;
        });

    });
}

document.onkeydown = function (evt) {
    evt = evt || window.event;
    var keyCode = evt.keyCode;
    if (keyCode == 27) {
        document.getElementById("communStatus").value = "0";
    }
};

function closeAddEndpoint() {
    $("#PopupAddUser").bPopup().close();
}
//ZD 101869 start
function fnChkdefault() {
    if ($("#Chkdefault").attr("checked")) {
        $('#lblFamilyLO').show();
        $('#lblFamilyLO').html("Default Family");
        $('#CurSelectedLayout').hide();
    }
    else {
        $('#lblFamilyLO').html("");
        $('#CurSelectedLayout').show();
    }
}
//ZD 101869 End
</script>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->





