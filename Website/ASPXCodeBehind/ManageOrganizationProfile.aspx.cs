/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using System.IO;
using System.Text;

namespace ns_OrgProfile
{
    public partial class ManageOrganizationProfile : System.Web.UI.Page
    {
        #region Private Data Members
        ns_Logger.Logger log;
        myVRMNet.NETFunctions obj;
        CustomizationUtil.CSSReplacementUtility cssUtil; //organization\CSS Module
       
        #endregion

        #region Protected Data Members
        protected System.Web.UI.WebControls.TextBox txtOrgID;
        protected System.Web.UI.WebControls.Label lblTitle;
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.TextBox txtOrgName;
        protected System.Web.UI.WebControls.TextBox txtOrgWebsite;
        protected System.Web.UI.WebControls.TextBox txtAddress1;
        protected System.Web.UI.WebControls.TextBox txtFaxNumber;
        protected System.Web.UI.WebControls.TextBox txtAddress2;
        protected System.Web.UI.WebControls.TextBox txtCity;
        protected System.Web.UI.WebControls.TextBox txtPhoneNumber;
        protected System.Web.UI.WebControls.TextBox txtEmailID;
        protected System.Web.UI.WebControls.TextBox TxtVideoRoomsTDB;//ZD 100535 12/12/2013 Inncrewin
        protected System.Web.UI.WebControls.TextBox TxtEndPointsTDB;//ZD 100535 12/12/2013 Inncrewin
        protected System.Web.UI.WebControls.TextBox TxtNonVideoRoomsTDB;//ZD 100535 12/12/2013 Inncrewin
        
        //FB 2678 Start
        protected System.Web.UI.WebControls.TextBox txtOrgexpirykey;
        protected System.Web.UI.WebControls.Label ActStatus;
        protected System.Web.UI.HtmlControls.HtmlTableRow ActivationTR;
        protected System.Web.UI.HtmlControls.HtmlTableCell ExportTD;
        protected System.Web.UI.HtmlControls.HtmlTable tbllicense;
        //FB 2678 End
        protected System.Web.UI.WebControls.TextBox txtExpiryDate;// FB 2678
        protected System.Web.UI.HtmlControls.HtmlTableRow trexpirydate;// FB 2678
        protected System.Web.UI.WebControls.DropDownList lstCountries;
        protected System.Web.UI.WebControls.DropDownList lstStates;
        protected System.Web.UI.WebControls.TextBox txtZipCode;
        protected System.Web.UI.HtmlControls.HtmlButton btnSubmitAddNew;
        //protected System.Web.UI.WebControls.Button btnSubmitAddNew;
        //protected System.Web.UI.WebControls.Button btnSubmit;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnOrganizationID;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnRooms;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnUsers;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnMCU;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnMCUEncha;//FB 2486
        protected System.Web.UI.WebControls.TextBox TxtMCU;
        protected System.Web.UI.WebControls.TextBox TxtUsers;
        protected System.Web.UI.WebControls.TextBox TxtExUsers;
        protected System.Web.UI.WebControls.TextBox TxtDomUsers;
        protected System.Web.UI.WebControls.TextBox TxtMobUsers; //FB 1979
        protected System.Web.UI.WebControls.TextBox TxtWebexUsers; //ZD 100221
        protected System.Web.UI.WebControls.TextBox TxtBJNUsers; //ZD 103550
        protected System.Web.UI.WebControls.TextBox TxtRooms;
        protected System.Web.UI.WebControls.DropDownList DrpFood;
        protected System.Web.UI.WebControls.DropDownList DrpResource;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanActiveRooms;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanActiveUsers;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanActiveExUsers;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanActiveDomUsers;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanActiveMobUsers; //FB 1979
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanActiveWebexUsers; //ZD 100221
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanBJNUsers; //ZD 103550
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanActiveMCU;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanActiveMCUEnchanced;//FB 2486
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanActiveVRooms;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanActiveNVRooms;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanActiveEpts;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanActiveFacility;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanActiveCat;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanActiveHK;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanActiveAPI;
        protected System.Web.UI.WebControls.TextBox TxtVRooms;
        protected System.Web.UI.WebControls.TextBox TxtNVRooms;
        protected System.Web.UI.WebControls.TextBox TxtEndPoint;
        protected System.Web.UI.WebControls.CheckBox ChkFacility;
        protected System.Web.UI.WebControls.CheckBox ChkCatering;
        protected System.Web.UI.WebControls.CheckBox ChkHK;
        protected System.Web.UI.WebControls.CheckBox ChkAPI;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnVRooms;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnNVRooms;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnEUsers;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnDUsers;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnMUsers; //FB 1979
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnWebexUsers; //ZD 100221
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnBJNUsers; //ZD 103550
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnEndPoint;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnFacility;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnCatering;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnHK;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnAPI;
        //FB 2347 Starts Commented for FB 2693
        //protected System.Web.UI.HtmlControls.HtmlInputHidden hdnPC; 
        //protected System.Web.UI.WebControls.CheckBox ChkPC; 
        //protected System.Web.UI.HtmlControls.HtmlGenericControl SpanActivePC; 
        //FB 2347 Ends
        //FB 2599 Start
        protected System.Web.UI.WebControls.CheckBox ChkCloud; //FB 2262 - j 
        protected System.Web.UI.HtmlControls.HtmlTableCell tdcloud;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdChkCloud;
        private int enableCloud = 0; //FB 2262 - J
        //FB 2599 End
        //FB 2426 start
        protected System.Web.UI.WebControls.TextBox TxtExtRooms;
        protected System.Web.UI.WebControls.TextBox TxtGstPerUser;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanExternalRooms;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnExtRooms;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnGstPerUser;
        //FB 2426 End
        //FB 2594 Starts
        protected System.Web.UI.WebControls.TextBox TxtMCUEncha;
        protected System.Web.UI.WebControls.CheckBox ChkPublicRoom;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdPublicRoom;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdChkPublicRoom;
        //FB 2594 Ends
        private String companyID = "";
        private int currRooms = 0;
        private int currVRooms = 0;
        private int currNVRooms = 0;
        private int currMCU = 0;
        private int currUsers = 0;
        private int currEUsers = 0;
        private int currDUsers = 0;
        private int currMUsers = 0; //FB 1979
        private int currWebexUsers = 0; //ZD 100221
        private int currBJNUsers = 0; //ZD 103550
        private int currEndpoint = 0;
        private int currMCUEnchanced = 0;//FB 2486
        private int currVMRRooms = 0;//FB 2586
        //FB 2426 Start
        private int currExtRooms = 0;
        private int currGstPerUser = 0;
        //FB 2426 End
        private int enableFacility = 0;
        private int enableCatering = 0;
        private int enableHK = 0;
        private int enableAPI = 0;
        //private int enablePC = 0; //FB 2347 //FB 2693
        private int enablePublicRoom = 0; //FB 2594
        //FB 2586 Start
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnVMR;
        protected System.Web.UI.WebControls.TextBox TxtVMR;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanVMRRooms;
        //FB 2586 End
        // FB 2693 Start
        protected System.Web.UI.WebControls.ImageButton img_Rooms ;
        protected System.Web.UI.WebControls.ImageButton img_Modules;
        protected System.Web.UI.WebControls.ImageButton img_MCUs;
        protected System.Web.UI.WebControls.ImageButton img_Users;

        protected System.Web.UI.HtmlControls.HtmlTable tblRooms;
        protected System.Web.UI.HtmlControls.HtmlTable tblMCUs;
        protected System.Web.UI.HtmlControls.HtmlTable tblusers;
        protected System.Web.UI.HtmlControls.HtmlTable tblModules;

        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnPCUser;        
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnJabber;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnLync;
        //protected System.Web.UI.HtmlControls.HtmlInputHidden hdnVidtel; //ZD 102004        
        protected System.Web.UI.WebControls.CheckBox chkJabber;
        protected System.Web.UI.WebControls.CheckBox chkLync;
        //protected System.Web.UI.WebControls.CheckBox chkVidtel; //ZD 102004
        protected System.Web.UI.WebControls.TextBox txtPCUser;        
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanJabber;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanLync;
        //protected System.Web.UI.HtmlControls.HtmlGenericControl SpanVidtel; //ZD 102004
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanActivePCUsers;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanGuRoomUser;
        private int currPCUsers = 0;
        private int enableJabber = 0, enableLync = 0, enableVidtel = 0;
        //FB 2693 End
        //FB 2694 Starts
        protected System.Web.UI.WebControls.TextBox txtVCHotRooms;
        protected System.Web.UI.WebControls.TextBox txtROHotRooms;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanActiveVC;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanActiveRO;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnVCHotdesking;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnROHotdesking;
        private int currVCHotRooms = 0, currROHotRooms;
        //FB 2694 End
        //FB 2659 Start
        protected System.Web.UI.WebControls.TextBox TxtSeats;
        protected System.Web.UI.HtmlControls.HtmlTable tblSeats;
        protected System.Web.UI.HtmlControls.HtmlTable tblallLicense;
        protected System.Web.UI.WebControls.TextBox TxtActiveUsers;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanUsers;//FB 2659
        protected System.Web.UI.HtmlControls.HtmlTableCell tdOrgExpiry;
        protected int enableCloudInstallation = 0;
        //FB 2659 End
		//FB 2593 Start
        protected System.Web.UI.WebControls.CheckBox ChkAdvReport;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanAdvReport;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnAdvReport;
        private int enableAdvReport = 0;
        //FB 2593 End
        //ZD 100518 - Start
        protected System.Web.UI.WebControls.TextBox txtMaxParticipant;
        protected System.Web.UI.WebControls.TextBox txtMaxConcntCall; 
        //ZD 100518 - End

        //ZD 101098 Start
        private int curriControlRooms = 0;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdniControl;
        protected System.Web.UI.WebControls.TextBox txtiControl;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanActiveiControl;
        //ZD 101098 End

        //ZD 101443
        protected int isLDAP = 0, isULBJN = 0; //ZD 104116


        #endregion

        #region Constructor
        public ManageOrganizationProfile()
        {
            log = new ns_Logger.Logger();
            obj = new myVRMNet.NETFunctions();
        }
        #endregion

        //ZD 101022
        #region InitializeCulture
        protected override void InitializeCulture()
        {
            if (Session["UserCulture"] != null)
            {
                UICulture = Session["UserCulture"].ToString();
                Culture = Session["UserCulture"].ToString();
                //ZD 101714
                if (Session["UserCulture"].ToString() == "fr-CA")
                    Culture = "en-US";
                base.InitializeCulture();
            }
        }
        #endregion

        #region PageLoad
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("ManageOrganizationProfile.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
                //FB 2659 Starts
                if (Session["EnableCloudInstallation"] != null)
                    int.TryParse(Session["EnableCloudInstallation"].ToString().Trim(), out enableCloudInstallation);
                if (enableCloudInstallation == 1)
                {
                    tblSeats.Visible = true;
                    tblallLicense.Visible = true;//ZD 100535 12/17/2013 inncrewin
                    tdOrgExpiry.Visible = false;
                    txtOrgexpirykey.Visible = false;
                    trexpirydate.Visible = false;
                }
                else
                {
                    tblSeats.Visible = false;
                }
                //FB 2659 End		
                
                if(Session["OrganizationToEdit"]!=null)
                    companyID = Session["OrganizationToEdit"].ToString();

                ChkPublicRoom.Enabled = false;//FB 2594
                ChkCloud.Enabled = false;//FB 2599

                errLabel.Text = "";

                btnSubmitAddNew.Disabled = false; //ZD 100420
                //btnSubmitAddNew.Enabled = true; //FB 1639
                if (Session["OrganizationsLimit"] != null) 
                {
                    if (Session["OrganizationsLimit"].ToString().Trim() == "1")
                        btnSubmitAddNew.Disabled = true; //ZD 100420
                        //btnSubmitAddNew.Enabled = false;
                }
                //ZD 100535 12/17/2013 inncrewin
                //FB 2693 start

                //img_Rooms.Attributes.Add("onclick", "javascript:ExpandCollapse(this,'" + tblRooms.ClientID + "', false);return false;");
                //img_Modules.Attributes.Add("onclick", "javascript:ExpandCollapse(this,'" + tblModules.ClientID + "', false);return false;");
                //img_MCUs.Attributes.Add("onclick", "javascript:ExpandCollapse(this,'" + tblMCUs.ClientID + "', false);return false;");
                //img_Users.Attributes.Add("onclick", "javascript:ExpandCollapse(this,'" + tblusers.ClientID + "', false);return false;");

                //FB 2693 start
                //ZD 100535 12/17/2013 inncrewin end

                //ZD 101443 Starts
                if (Session["IsLDAP"] != null && Session["IsLDAP"].ToString() != "")
                    int.TryParse(Session["IsLDAP"].ToString(), out isLDAP);

                if (isLDAP >= 1)
                {
                    TxtUsers.Text = "";
                    TxtActiveUsers.Text = "";
                    TxtUsers.Enabled = false;
                    TxtActiveUsers.Enabled = false;
                    if (enableCloudInstallation == 1)
                    {
                        TxtExUsers.Text = "";
                        TxtExUsers.Enabled = false;

                        TxtMobUsers.Text = "";
                        TxtMobUsers.Enabled = false;
                    }
                }
                //ZD 101443 End
                //ZD 104116 Starts
                if (Session["SiteBJNuserLimit"] != null && Session["SiteBJNuserLimit"].ToString() != "")
                    int.TryParse(Session["SiteBJNuserLimit"].ToString(), out isULBJN);

                if (isULBJN == -2)
                {
                    TxtBJNUsers.Text = "";
                    TxtBJNUsers.Enabled = false;
                }
                //ZD 104116 Ends
                if (!IsPostBack)
                {
                    if (companyID.Equals("new"))
                        lblTitle.Text = obj.GetTranslatedText("Create New Organization");//FB 1830 - Translation
                    else
                        lblTitle.Text = obj.GetTranslatedText("Edit Organization Profile");//FB 1830 - Translation

                    BindData();

                    if(Request.QueryString["m"] != null)
                        if (Request.QueryString["m"].ToString().Equals("1"))
                        {
                            errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                            errLabel.Visible = true;
                        }
                }
                // FB 2693 start
                //FB 2659 Starts
                if (enableCloudInstallation == 1)
                    SpanUsers.InnerText = hdnUsers.Value;
                //else
                //{//ZD 100535 12/17/2013 inncrewin
                    SpanActiveRooms.InnerText = hdnRooms.Value;
                    SpanActiveRooms.Style.Add("color", "#1F8CC0");
                    SpanActiveVRooms.InnerText = hdnVRooms.Value;
                    SpanActiveNVRooms.InnerText = hdnNVRooms.Value;
                    SpanActiveMCU.InnerText = hdnMCU.Value;
                    //ZD 101443 Starts
                    if (isLDAP == 1)
                        SpanActiveUsers.InnerText = obj.GetTranslatedText("Active directory enabled");
                    else
                        SpanActiveUsers.InnerText = hdnUsers.Value;
                    //ZD 101443 End

                    if (isLDAP == 1 && enableCloudInstallation == 1)
                    {
                        SpanUsers.InnerText = obj.GetTranslatedText("Active directory enabled");
                        SpanActiveExUsers.InnerText = obj.GetTranslatedText("Active directory enabled");                        
                        SpanActiveMobUsers.InnerText = obj.GetTranslatedText("Active directory enabled");
                    }
                    else
                    {                     
                        SpanActiveExUsers.InnerText = hdnEUsers.Value; //FB 2098                        
                        SpanActiveMobUsers.InnerText = hdnMUsers.Value; //FB 1979
                    }
                    SpanActiveDomUsers.InnerText = hdnDUsers.Value;   //FB 2098
                    SpanActiveWebexUsers.InnerText = hdnWebexUsers.Value; //ZD 100221
                    if (hdnBJNUsers.Value != "-2")
                        SpanBJNUsers.InnerText = hdnBJNUsers.Value; //104021
                    else
                        SpanBJNUsers.InnerText = obj.GetTranslatedText("Limited to Active Users");
                    SpanActiveEpts.InnerText = hdnEndPoint.Value;
                    SpanExternalRooms.InnerText = hdnExtRooms.Value; //FB 2426
                    SpanActiveFacility.InnerText = hdnFacility.Value; // FB 2570
                    SpanActiveCat.InnerText = hdnCatering.Value;
                    SpanActiveHK.InnerText = hdnHK.Value; //Edited for FB 1706  FB 2570
                    SpanActiveAPI.InnerText = hdnAPI.Value;
                    //SpanActivePC.InnerText =  hdnPC.Value ; //FB 2347 //FB 2693
                    SpanActiveMCUEnchanced.InnerText = hdnMCUEncha.Value; //FB 2486
                    SpanVMRRooms.InnerText = hdnVMR.Value; //FB 2586
                    SpanActiveiControl.InnerText = hdniControl.Value;//ZD 101098
                                    
                    SpanJabber.InnerText = hdnJabber.Value;
                    SpanLync.InnerText = hdnLync.Value;
                    //SpanVidtel.InnerText = hdnVidtel.Value; //ZD 102004
                    SpanActivePCUsers.InnerText = hdnPCUser.Value;
                    SpanGuRoomUser.InnerText = obj.GetTranslatedText("N/A");
                    // FB 2693 End
                    //FB 2694 Starts
                    SpanActiveVC.InnerText = hdnVCHotdesking.Value;
                    SpanActiveRO.InnerText = hdnROHotdesking.Value;
                    //FB 2694 Ends
					SpanAdvReport.InnerText = hdnAdvReport.Value;//FB 2593
                //}//FB 2659//ZD 100535 12/17/2013 inncrewin
            }
            catch (Exception ex)
            {
                log.Trace("PageLoad: " + ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region BindData
        /// <summary>
        /// BindData
        /// </summary>
        protected void BindData()
        {
            string status = "";//FB 2678
            try
            {
                if (Session["OrganizationToEdit"].ToString().Equals("new"))
                {
                    txtOrgName.Text = "";
                    txtAddress1.Text = "";
                    txtAddress2.Text = "";
                    txtCity.Text = "";
                    obj.GetCountryCodes(lstCountries);
                    try
                    {
                        if (companyID.Equals("new"))
                        {
                            lstCountries.Items.FindByValue("225").Selected = true;
                            UpdateStates(null, null);
                        }
                        else
                            lstCountries.Items.FindByValue("1").Selected = true;
                    }
                    catch (Exception ex1) { }

                    lstStates.Items.Clear();
                    obj.GetCountryStates(lstStates, lstCountries.SelectedValue);

                    if (companyID.Equals("new"))
                        lstStates.Items.FindByValue("-1").Selected = true; // ZD 100486 Default to Please select... Correspondong to the United States as Country

                    txtZipCode.Text = "";
                    txtPhoneNumber.Text = "";
                    txtFaxNumber.Text = "";
                    txtEmailID.Text = "";
                    txtOrgWebsite.Text = "";
                    //FB 2659 Starts
                    if (enableCloudInstallation == 0)
                    {
                        TxtRooms.Text = "0";
                        TxtVRooms.Text = "0";
                        TxtNVRooms.Text = "0";
                        if(isLDAP == 1)
                            TxtUsers.Text = "";
                        else
                            TxtUsers.Text = "0";
                        if (isLDAP == 1 && enableCloudInstallation == 1)
                        {
                            TxtExUsers.Text = "";
                            TxtMobUsers.Text = "";
                        }
                        else
                        {
                            TxtExUsers.Text = "0";
                            TxtMobUsers.Text = "0"; //FB 1979
                        }
                        TxtDomUsers.Text = "0";
                        TxtWebexUsers.Text = "0"; //ZD 100221
                        if (isULBJN == -2) //ZD 104116
                        {
                            TxtBJNUsers.Text = "";
                            TxtBJNUsers.Enabled = false;
                        }
                        else
                            TxtBJNUsers.Text = "0";//ZD 103550
                        TxtMCU.Text = "0";
                        TxtEndPoint.Text = "0";
                        TxtMCUEncha.Text = "0";//FB 2486
                        //FB 2426 Start
                        TxtExtRooms.Text = "0";
                        TxtGstPerUser.Text = "0";
                        //FB 2426 End
                        TxtVMR.Text = "0";//FB 2586
                        txtiControl.Text = "0";//ZD 101098
                        ChkFacility.Checked = false;
                        ChkHK.Checked = false;
                        ChkCatering.Checked = false;
                        ChkAPI.Checked = false;
                        //ChkPC.Checked = false; //FB 2347 FB 2693
                        ChkCloud.Enabled = false; //FB 2599
                        ChkPublicRoom.Checked = false; //FB 2594
                        ChkPublicRoom.Enabled = false; //FB 2594
                        txtOrgexpirykey.Text = "";//FB 2678
                        tbllicense.Attributes.Add("style", "display:none");//FB 2678
                        trexpirydate.Attributes.Add("style", "display:none");//FB 2678
                        txtExpiryDate.Text = "";// FB 2678
                        //FB 2693 Starts
                        txtPCUser.Text = "0";                        
                        chkJabber.Checked = false;
                        chkLync.Checked = false;
                        //chkVidtel.Checked = false; //ZD 102004
                        //FB 2693 Ends
                        //FB 2694 Starts
                        txtVCHotRooms.Text = "0";
                        txtROHotRooms.Text = "0";
                        //FB 2694 Ends
						ChkAdvReport.Checked = false;//FB 2593
                    }//FB 2689 Ends
                    //ZD 100535 12/17/2013 inncrewin
                    else if (enableCloudInstallation==1)
                    {
                        GetDefaultLicence();                       
                    }
                    //ZD 100535 12/17/2013 inncrewin ends
                }
                else
                {
                    String inXML = "<GetOrganizationProfile>";
                    inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                    inXML += "  <OrgId>" + Session["OrganizationToEdit"].ToString() + "</OrgId>";
                    inXML += "  </GetOrganizationProfile>";
                    log.Trace("GetOrganizationProfile inxml: " + inXML);
                    String outXML = obj.CallMyVRMServer("GetOrganizationProfile", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                    log.Trace("GetOrganizationProfile outxml: " + outXML);

                    if (outXML.IndexOf("<error>") < 0)
                    {
                        XmlDocument xmldoc = new XmlDocument();
                        xmldoc.LoadXml(outXML);

                        txtOrgName.Text = xmldoc.SelectSingleNode("//OrganizationProfile/OrganizationName").InnerText;
                        //txtOrgName.Enabled = false;
                        txtAddress1.Text = xmldoc.SelectSingleNode("//OrganizationProfile/Address1").InnerText;
                        txtAddress2.Text = xmldoc.SelectSingleNode("//OrganizationProfile/Address2").InnerText;
                        txtCity.Text = xmldoc.SelectSingleNode("//OrganizationProfile/City").InnerText;
                        txtZipCode.Text = xmldoc.SelectSingleNode("//OrganizationProfile/ZipCode").InnerText;
                        txtPhoneNumber.Text = xmldoc.SelectSingleNode("//OrganizationProfile/Phone").InnerText;
                        txtFaxNumber.Text = xmldoc.SelectSingleNode("//OrganizationProfile/Fax").InnerText;
                        txtEmailID.Text = xmldoc.SelectSingleNode("//OrganizationProfile/EmailID").InnerText;
                        txtOrgWebsite.Text = xmldoc.SelectSingleNode("//OrganizationProfile/Website").InnerText;
                        //FB 2678 Start
                        txtOrgexpirykey.Text = xmldoc.SelectSingleNode("//OrganizationProfile/Orgexpirykey").InnerText;
                        status = xmldoc.SelectSingleNode("OrganizationProfile/OrgStatus").InnerText;
                        if (enableCloudInstallation <= 0)//FB 2659
                        {

                            if (status == "1")
                            {
                                tbllicense.Attributes.Add("style", "display:block");//FB 2678
                                trexpirydate.Attributes.Add("style", "display:");//FB 2678
                                ActStatus.Text = obj.GetTranslatedText("Activated");
                                ActStatus.ForeColor = System.Drawing.Color.DarkGreen;
                                ExportTD.Attributes.Add("style", "display:none");
                            }
                            else if (status == obj.GetTranslatedText("Demo"))
                            {
                                tbllicense.Attributes.Add("style", "display:block");//FB 2678
                                trexpirydate.Attributes.Add("style", "display:");//FB 2678
                                ActStatus.Text = obj.GetTranslatedText("Demo");
                                ActStatus.ForeColor = System.Drawing.Color.Red;
                                ExportTD.Attributes.Add("style", "display:none");
                            }
                            else
                            {
                                tbllicense.Attributes.Add("style", "display:block");//FB 2678
                                trexpirydate.Attributes.Add("style", "display:");//FB 2678
                                ActStatus.Text = obj.GetTranslatedText("Deactivated");
                                ActStatus.ForeColor = System.Drawing.Color.Red;
                                ExportTD.Attributes.Add("style", "display:block");
                            }
                        }//FB 2659 Ends

                        if (enableCloudInstallation == 1)//FB 2659
                        {
                            //ZD 101525
                            if (isLDAP == 1)
                                TxtActiveUsers.Text = "";
                            else
                                TxtActiveUsers.Text = xmldoc.SelectSingleNode("//OrganizationProfile/Users").InnerText; //FB 2659

                            TxtSeats.Text = xmldoc.SelectSingleNode("//OrganizationProfile/Seats").InnerText;
                            Int32.TryParse(TxtActiveUsers.Text.Trim(), out currUsers);
                            //ZD 100518 Starts
                            txtMaxParticipant.Text = xmldoc.SelectSingleNode("//OrganizationProfile/MaxParticipants").InnerText;
                            txtMaxConcntCall.Text = xmldoc.SelectSingleNode("//OrganizationProfile/MaxConcurrentCall").InnerText;
                            //ZD 100518 Ends
                        }
                        else
                        {

                            String license = xmldoc.SelectSingleNode("//OrganizationProfile/OrgexpiryDate").InnerText;
                            //ZD 100995 start
                            if (license != "")
                            {
                                String formatDate = "";
                                formatDate = myVRMNet.NETFunctions.GetFormattedDate(license);
                                license = license.Replace(license, formatDate);
                                txtExpiryDate.Text = license;
                            }
                            //ZD 100995 start
                        }//ZD 100535 12/17/2013 inncrewin

                            // FB 2678 End
                            TxtRooms.Text = xmldoc.SelectSingleNode("//OrganizationProfile/Rooms").InnerText;
                            TxtVRooms.Text = xmldoc.SelectSingleNode("//OrganizationProfile/VideoRooms").InnerText;
                            TxtNVRooms.Text = xmldoc.SelectSingleNode("//OrganizationProfile/NonVideoRooms").InnerText;
                            if (isLDAP == 1)
                                TxtUsers.Text = "";
                            else
                                TxtUsers.Text = xmldoc.SelectSingleNode("//OrganizationProfile/Users").InnerText;
                            if (isLDAP == 1 && enableCloudInstallation == 1)
                            {
                                TxtExUsers.Text = "";
                                TxtMobUsers.Text = "";
                            }
                            else
                            {
                                TxtExUsers.Text = xmldoc.SelectSingleNode("//OrganizationProfile/ExchangeUsers").InnerText;
                                TxtMobUsers.Text = xmldoc.SelectSingleNode("//OrganizationProfile/MobileUsers").InnerText; //FB 1979
                            }
                            TxtDomUsers.Text = xmldoc.SelectSingleNode("//OrganizationProfile/DominoUsers").InnerText;
                            TxtWebexUsers.Text = xmldoc.SelectSingleNode("//OrganizationProfile/WebexUsers").InnerText; //ZD 100221
                            
                            if (isULBJN == -2)  //ZD 104116
                                TxtBJNUsers.Text = ""; 
                            else
                                TxtBJNUsers.Text = xmldoc.SelectSingleNode("//OrganizationProfile/BlueJeansUsers").InnerText; //ZD 103550

                            TxtMCU.Text = xmldoc.SelectSingleNode("//OrganizationProfile/MCU").InnerText;
                            TxtEndPoint.Text = xmldoc.SelectSingleNode("//OrganizationProfile/EndPoints").InnerText;
                            TxtMCUEncha.Text = xmldoc.SelectSingleNode("//OrganizationProfile/MCUEnchanced").InnerText;//FB 2486
                            //FB 2426 Start
                            TxtExtRooms.Text = xmldoc.SelectSingleNode("//OrganizationProfile/GuestRooms").InnerText;
                            TxtGstPerUser.Text = xmldoc.SelectSingleNode("//OrganizationProfile/GuestRoomPerUser").InnerText;
                            //FB 2426 End
                            TxtVMR.Text = xmldoc.SelectSingleNode("//OrganizationProfile/VMRRooms").InnerText;//FB 2586
                            txtiControl.Text = xmldoc.SelectSingleNode("//OrganizationProfile/iControlRooms").InnerText;//ZD 101098
                            string enabAV = xmldoc.SelectSingleNode("//OrganizationProfile/EnableFacilites").InnerText;
                            string enabCat = xmldoc.SelectSingleNode("//OrganizationProfile/EnableCatering").InnerText;
                            string enabHK = xmldoc.SelectSingleNode("//OrganizationProfile/EnableHouseKeeping").InnerText;
                            string enabAPI = xmldoc.SelectSingleNode("//OrganizationProfile/EnableAPIs").InnerText;
                            //string enabPC = xmldoc.SelectSingleNode("//OrganizationProfile/EnablePC").InnerText; //FB 2347 //FB 2693

                            if (xmldoc.SelectSingleNode("//OrganizationProfile/EnableCloud") != null) //FB 2599 feb 22
                                Int32.TryParse(xmldoc.SelectSingleNode("//OrganizationProfile/EnableCloud").InnerText, out enableCloud); //FB 2262  
                            //FB 2594 Starts
                            if (xmldoc.SelectSingleNode("//OrganizationProfile/EnablePublicRoom") != null)
                                Int32.TryParse(xmldoc.SelectSingleNode("//OrganizationProfile/EnablePublicRoom").InnerText, out enablePublicRoom);
                            //FB 2594 Ends

                            //FB 2693 Starts
                            if (xmldoc.SelectSingleNode("//OrganizationProfile/PCUsers") != null)
                                txtPCUser.Text = xmldoc.SelectSingleNode("//OrganizationProfile/PCUsers").InnerText;
                                                   
                            if (xmldoc.SelectSingleNode("//OrganizationProfile/EnableJabber") != null)
                                int.TryParse(xmldoc.SelectSingleNode("//OrganizationProfile/EnableJabber").InnerText.Trim(), out enableJabber);

                            if (xmldoc.SelectSingleNode("//OrganizationProfile/EnableLync") != null)
                                int.TryParse(xmldoc.SelectSingleNode("//OrganizationProfile/EnableLync").InnerText.Trim(), out enableLync);

                            if (xmldoc.SelectSingleNode("//OrganizationProfile/EnableVidtel") != null)
                                int.TryParse(xmldoc.SelectSingleNode("//OrganizationProfile/EnableVidtel").InnerText.Trim(), out enableVidtel);
                            //FB 2693 Ends
                            //FB 2694 Starts
                            if (xmldoc.SelectSingleNode("//OrganizationProfile/VCHotRooms") != null)
                                txtVCHotRooms.Text = xmldoc.SelectSingleNode("//OrganizationProfile/VCHotRooms").InnerText.Trim();

                            if (xmldoc.SelectSingleNode("//OrganizationProfile/ROHotRooms") != null)
                                txtROHotRooms.Text = xmldoc.SelectSingleNode("//OrganizationProfile/ROHotRooms").InnerText.Trim();
	
							//FB 2593 - Start
                        if (xmldoc.SelectSingleNode("//OrganizationProfile/EnableAdvancedReport") != null)
                            int.TryParse(xmldoc.SelectSingleNode("//OrganizationProfile/EnableAdvancedReport").InnerText.Trim(), out enableAdvReport);
                        //FB 2593 - End

                            int.TryParse(txtVCHotRooms.Text, out currVCHotRooms);
                            int.TryParse(txtROHotRooms.Text, out currROHotRooms);
                            //FB 2694 End
                            Int32.TryParse(TxtRooms.Text.Trim(), out currRooms);
                            Int32.TryParse(TxtVRooms.Text.Trim(), out currVRooms);
                            Int32.TryParse(TxtNVRooms.Text.Trim(), out currNVRooms);
                            Int32.TryParse(TxtMCU.Text.Trim(), out currMCU);
                            Int32.TryParse(TxtUsers.Text.Trim(), out currUsers);
                            Int32.TryParse(TxtExUsers.Text.Trim(), out currEUsers);
                            Int32.TryParse(TxtDomUsers.Text.Trim(), out currDUsers);
                            Int32.TryParse(TxtMobUsers.Text.Trim(), out currMUsers); //FB 1979
                            Int32.TryParse(TxtWebexUsers.Text.Trim(), out currWebexUsers); //ZD 100221
                            Int32.TryParse(TxtBJNUsers.Text.Trim(), out currBJNUsers); //ZD 103550
                            Int32.TryParse(TxtEndPoint.Text.Trim(), out currEndpoint);
                            Int32.TryParse(TxtMCUEncha.Text.Trim(), out currMCUEnchanced);//FB 2486
                            //FB 2426 Start
                            Int32.TryParse(TxtExtRooms.Text.Trim(), out currExtRooms);
                            Int32.TryParse(TxtGstPerUser.Text.Trim(), out currGstPerUser);
                            //FB 2426 End
                            Int32.TryParse(TxtVMR.Text.Trim(), out currVMRRooms);//FB 2586
                            Int32.TryParse(txtiControl.Text.Trim(), out curriControlRooms);//ZD 101098
                            Int32.TryParse(enabAV, out enableFacility);
                            Int32.TryParse(enabCat, out enableCatering);
                            Int32.TryParse(enabHK, out enableHK);
                            Int32.TryParse(enabAPI, out enableAPI);
                            //Int32.TryParse(enabPC, out enablePC); //FB 2347 //FB 2693
                            int.TryParse(txtPCUser.Text.Trim(), out currPCUsers); //FB 2693

                            if (enableFacility == 1)
                                ChkFacility.Checked = true;
                            if (enableCatering == 1)
                                ChkCatering.Checked = true;
                            if (enableHK == 1)
                                ChkHK.Checked = true;
                            if (enableAPI == 1)
                                ChkAPI.Checked = true;
                            //if (enablePC == 1)//FB 2347 //FB 2693
                            //    ChkPC.Checked = true;

                            //FB 2599 //FB 2645
                            if (enableCloud == 1)
                                ChkCloud.Checked = true;
                            else
                                ChkCloud.Checked = false;

                            //FB 2594 //FB 2645
                            if (enablePublicRoom == 1)
                                ChkPublicRoom.Checked = true;
                            else
                                ChkPublicRoom.Checked = false;

                            //FB 2693 Starts                            
                            if (enableJabber == 1)
                                chkJabber.Checked = true;
                            else
                                chkJabber.Checked = false;

                            if (enableLync == 1)
                                chkLync.Checked = true;
                            else
                                chkLync.Checked = false;

                            //ZD 102004
                            //if (enableVidtel == 1)
                            //    chkVidtel.Checked = true;
                            //else
                            //    chkVidtel.Checked = false;

                            //FB 2693 Ends
							//FB 2593 Start
                         if (enableAdvReport == 1)
                             ChkAdvReport.Checked = true;
                         else
                             ChkAdvReport.Checked = false;
                         //FB 2593 End
                        //FB 2659 Ends //ZD 100535 12/17/2013 inncrewin

                        obj.GetCountryCodes(lstCountries);
                        //Get Countries and States
                        try
                        {
                            lstCountries.ClearSelection();
                            lstCountries.Items.FindByValue(xmldoc.SelectSingleNode("//OrganizationProfile/Country").InnerText).Selected = true;
                            if ((lstCountries.Items.Count > 1) && (lstCountries.SelectedIndex > 0))
                            {
                                lstStates.Items.Clear();
                                obj.GetCountryStates(lstStates, lstCountries.SelectedValue);
                                if (lstStates.Items.Count > 0)
                                {
                                    lstStates.ClearSelection();
                                    lstStates.Items.FindByValue(xmldoc.SelectSingleNode("//OrganizationProfile/State").InnerText).Selected = true;
                                }
                            }
                        }
                        catch (Exception ex1) { }
                    }
                }

                obj.GetSysLicenseInfo();
                hdnUsers.Value = (obj.remainingUsers + currUsers).ToString();//FB 2659

                //if (enableCloudInstallation == 0)//FB 2659 //ZD 100535 12/17/2013 inncrewin
                //{
                    hdnRooms.Value = (obj.remainingRooms + currRooms).ToString();
                    hdnVRooms.Value = (obj.remainingVRooms + currVRooms).ToString();
                    hdnNVRooms.Value = (obj.remainingNVRooms + currNVRooms).ToString();
                    hdnMCU.Value = (obj.remainingMCUs + currMCU).ToString();
                    //hdnUsers.Value = (obj.remainingUsers + currUsers).ToString();
                    hdnEUsers.Value = (obj.remExchangeUsers + currEUsers).ToString();
                    hdnDUsers.Value = (obj.remDominoUsers + currDUsers).ToString();
                    hdnMUsers.Value = (obj.remMobileUsers + currMUsers).ToString(); //FB 1979
                    hdnWebexUsers.Value = (obj.remWebexUsers + currWebexUsers).ToString(); //ZD 100221
                    if (obj.MaxBJNUsers != -2)
                        hdnBJNUsers.Value = (obj.remBJNUsers + currBJNUsers).ToString();//ZD 103550
                    else
                        hdnBJNUsers.Value = "-2";
                    hdnEndPoint.Value = (obj.remainingEndPoints + currEndpoint).ToString();
                    hdnMCUEncha.Value = (obj.remainingEnchancedMCUs + currMCUEnchanced).ToString();//FB 2486
                    //FB 2426 Start
                    hdnExtRooms.Value = (obj.remainingExtRooms + currExtRooms).ToString();
                    hdnGstPerUser.Value = (obj.remainingGstRoomPerUser + currGstPerUser).ToString();
                    //FB 2426 End
                    hdnVMR.Value = (obj.remainingVMRRooms + currVMRRooms).ToString();//FB 2586
                    hdniControl.Value = (obj.remainingiControlRooms + curriControlRooms).ToString();//ZD 101098
                    hdnFacility.Value = (obj.remainingFacilities + enableFacility).ToString();
                    hdnCatering.Value = (obj.remainingCatering + enableCatering).ToString();
                    hdnHK.Value = (obj.remainingHouseKeeping + enableHK).ToString();
                    hdnAPI.Value = (obj.remainingAPI + enableAPI).ToString();
                    //hdnPC.Value = (obj.remainingPC + enablePC).ToString(); //FB 2347 //FB 2693
                    //FB 2694 Starts
                    hdnVCHotdesking.Value = (obj.remainingVCHotRooms + currVCHotRooms).ToString();
                    hdnROHotdesking.Value = (obj.remainingROHotRooms + currROHotRooms).ToString();
                    //FB 2694 End
                    //FB 2693 Starts
                    hdnPCUser.Value = (obj.remainingPCUsers + currPCUsers).ToString();                    
                    hdnJabber.Value = (obj.remainingJabber + enableJabber).ToString();
                    hdnLync.Value = (obj.remainingLync + enableLync).ToString();
                    //hdnVidtel.Value = (obj.remainingVidtel + enableVidtel).ToString(); //ZD 102004
                    if (hdnFacility.Value == "0")
                        ChkFacility.Enabled = false;
                    if (hdnCatering.Value == "0")
                        ChkCatering.Enabled = false;
                    if (hdnHK.Value == "0")
                        ChkHK.Enabled = false;
                    if (hdnAPI.Value == "0")
                        ChkAPI.Enabled = false;                    
                    if (hdnJabber.Value == "0")
                        chkJabber.Enabled = false;
                    if (hdnLync.Value == "0")
                        chkLync.Enabled = false;
                    //ZD 102004
                    //if (hdnVidtel.Value == "0")
                    //    chkVidtel.Enabled = false;
                    //FB 2693 Ends

					//FB 2593 Start
                    hdnAdvReport.Value = (obj.remainingAdvReport + enableAdvReport).ToString();
                    if (hdnAdvReport.Value == "0")
                    ChkAdvReport.Enabled = false;
                    //FB 2593 End

                    //FB 2599 Start FB 2262
                    if (obj.enableCloud == 1)
                    {
                        tdcloud.Visible = true;
                        tdChkCloud.Visible = true;
                        ChkCloud.Enabled = false;
                    }
                    else
                    {
                        tdChkCloud.Visible = false;
                        tdcloud.Visible = false;
                        ChkCloud.Checked = false;
                    }
                    //FB 2599 End

                    //FB 2645 Start
                    if (obj.enablePublicRooms == 1)
                    {
                        tdPublicRoom.Visible = true;
                        tdChkPublicRoom.Visible = true;
                        ChkPublicRoom.Enabled = false;
                    }
                    else
                    {
                        tdPublicRoom.Visible = false;
                        tdChkPublicRoom.Visible = false;
                        ChkPublicRoom.Checked = false;
                    }
                    //FB 2645 End

                    //FB 2678 Start
                    if (Session["IndividualOrgExpiry"] != null && Session["IndividualOrgExpiry"].ToString() != "")
                        if (Session["IndividualOrgExpiry"].ToString() == "0")
                            txtOrgexpirykey.Attributes.Add("readonly", "true");
                    //FB 2678 End
                }
            //}//FB 2659 Ends //ZD 100535 12/17/2013 inncrewin
            catch (Exception ex)
            {
                log.Trace("BindData: " + ex.StackTrace + " : " + ex.Message);
            }
        }
        /// <summary>
        /// ZD 100535 17/12/2013 inncrewin
        /// </summary>
        protected void GetDefaultLicence()
        {
            try
            {
                string inXML = "<GetDefaultLicense><userid>" + Session["userID"].ToString() + "</userid></GetDefaultLicense>";
                string outXML = obj.CallMyVRMServer("GetDefaultLicense", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);

                    TxtRooms.Text = xmldoc.SelectSingleNode("//GetDefaultLicense/Rooms").InnerText;
                    TxtVRooms.Text = xmldoc.SelectSingleNode("//GetDefaultLicense/VideoRooms").InnerText;
                    TxtNVRooms.Text = xmldoc.SelectSingleNode("//GetDefaultLicense/NonVideoRooms").InnerText;
                    //ZD 101525
                    if (isLDAP == 1)
                    {
                        TxtUsers.Text = "";
                        if (enableCloudInstallation == 1)
                        {
                            TxtExUsers.Text = "";
                            TxtMobUsers.Text = "";
                            TxtActiveUsers.Text = "";
                        }
                    }
                    else
                    {
                        TxtUsers.Text = xmldoc.SelectSingleNode("//GetDefaultLicense/Users").InnerText;
                        TxtExUsers.Text = xmldoc.SelectSingleNode("//GetDefaultLicense/ExchangeUsers").InnerText;
                        TxtMobUsers.Text = xmldoc.SelectSingleNode("//GetDefaultLicense/MobileUsers").InnerText;
                    }
                    TxtDomUsers.Text = xmldoc.SelectSingleNode("//GetDefaultLicense/DominoUsers").InnerText;
                    TxtMCU.Text = xmldoc.SelectSingleNode("//GetDefaultLicense/MCU").InnerText;
                    TxtEndPoint.Text = xmldoc.SelectSingleNode("//GetDefaultLicense/EndPoints").InnerText;
                    TxtMCUEncha.Text = xmldoc.SelectSingleNode("//GetDefaultLicense/MCUEnchanced").InnerText;
                    TxtExtRooms.Text = xmldoc.SelectSingleNode("//GetDefaultLicense/GuestRooms").InnerText;
                    TxtGstPerUser.Text = xmldoc.SelectSingleNode("//GetDefaultLicense/GuestRoomPerUser").InnerText;
                    TxtVMR.Text = xmldoc.SelectSingleNode("//GetDefaultLicense/VMRRooms").InnerText;//FB 2586
                    string enabAV = xmldoc.SelectSingleNode("//GetDefaultLicense/EnableFacilites").InnerText;
                    string enabCat = xmldoc.SelectSingleNode("//GetDefaultLicense/EnableCatering").InnerText;
                    string enabHK = xmldoc.SelectSingleNode("//GetDefaultLicense/EnableHouseKeeping").InnerText;
                    string enabAPI = xmldoc.SelectSingleNode("//GetDefaultLicense/EnableAPIs").InnerText;
                    if (xmldoc.SelectSingleNode("//GetDefaultLicense/PCUsers") != null)
                        txtPCUser.Text = xmldoc.SelectSingleNode("//GetDefaultLicense/PCUsers").InnerText;                    
                    string enableJabber = xmldoc.SelectSingleNode("//GetDefaultLicense/EnableJabber").InnerText;
                    string enableLync = xmldoc.SelectSingleNode("//GetDefaultLicense/EnableLync").InnerText;
                    string enableVidtel = xmldoc.SelectSingleNode("//GetDefaultLicense/EnableVidtel").InnerText;
                    if (xmldoc.SelectSingleNode("//GetDefaultLicense/VCHotRooms") != null)
                        txtVCHotRooms.Text = xmldoc.SelectSingleNode("//GetDefaultLicense/VCHotRooms").InnerText.Trim();
                    if (xmldoc.SelectSingleNode("//GetDefaultLicense/ROHotRooms") != null)
                        txtROHotRooms.Text = xmldoc.SelectSingleNode("//GetDefaultLicense/ROHotRooms").InnerText.Trim();
                    string enableadvancedreport = xmldoc.SelectSingleNode("//GetDefaultLicense/AdvancedReports").InnerText;

                    if (enabAV == "1")  ChkFacility.Checked = true;
                    if (enabCat == "1") ChkCatering.Checked = true;
                    if (enabHK == "1")  ChkHK.Checked = true;
                    if (enabAPI == "1") ChkAPI.Checked = true;                    
                    if (enableJabber == "1") chkJabber.Checked = true;
                    if (enableLync == "1")chkLync.Checked = true;
                    //if (enableVidtel == "1")chkVidtel.Checked = true; //ZD 102004
                    if (enableadvancedreport == "1") ChkAdvReport.Checked = true;

                    obj.GetSysLicenseInfo();
                    hdnRooms.Value = obj.remainingRooms.ToString();
                    hdnVRooms.Value = obj.remainingVRooms.ToString();
                    hdnNVRooms.Value = obj.remainingNVRooms.ToString();
                    hdnMCU.Value = obj.remainingMCUs.ToString();
                    hdnUsers.Value = obj.remainingUsers.ToString();
                    hdnEUsers.Value = obj.remExchangeUsers.ToString();
                    hdnDUsers.Value = obj.remDominoUsers.ToString();
                    hdnMUsers.Value = obj.remMobileUsers.ToString();
                    hdnEndPoint.Value = obj.remainingEndPoints.ToString();
                    hdnMCUEncha.Value = obj.remainingEnchancedMCUs.ToString();
                    hdnExtRooms.Value = obj.remainingExtRooms.ToString();
                    hdnGstPerUser.Value = obj.remainingGstRoomPerUser.ToString();
                    hdnVMR.Value = obj.remainingVMRRooms.ToString();
                    hdnFacility.Value = obj.remainingFacilities.ToString();
                    hdnCatering.Value = obj.remainingCatering.ToString();
                    hdnHK.Value = obj.remainingHouseKeeping.ToString();
                    hdnAPI.Value = obj.remainingAPI.ToString();
                    hdnVCHotdesking.Value = obj.remainingVCHotRooms.ToString();
                    hdnROHotdesking.Value = obj.remainingROHotRooms.ToString();
                    hdnPCUser.Value = obj.remainingPCUsers.ToString();                    
                    hdnJabber.Value = obj.remainingJabber.ToString();
                    hdnLync.Value = obj.remainingLync.ToString();
                    //hdnVidtel.Value = obj.remainingVidtel.ToString(); //ZD 102004
                    hdnAdvReport.Value = obj.remainingAdvReport.ToString();

                }
            }
            catch (Exception ex)
            {
                log.Trace("GetDefaultLicense BindData: " + ex.Message);
            }
        }
        // ZD 100535 17/12/2013 inncrewin
        #endregion

        #region UpdateStates
        protected void UpdateStates(Object sender, EventArgs e)
        {
            try
            {
                lstStates.Items.Clear();
                if (!lstCountries.SelectedValue.Equals("-1"))
                    obj.GetCountryStates(lstStates, lstCountries.SelectedValue);
            }
            catch (Exception ex)
            {
                log.Trace("UpdateStates: " + ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region ResetOrganizationProfile
        protected void ResetOrganizationProfile(Object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("ManageOrganizationProfile.aspx");
            }
            catch (System.Threading.ThreadAbortException) { }
            catch (Exception ex)
            {
                log.Trace("ResetOrganizationProfile: " + ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region SubmitOrganizationProfile
        protected void SubmitOrganizationProfile(Object sender, EventArgs e)
        {
            try
            {
                if(SetOrganizationProfile())
                 Response.Redirect("ManageOrganization.aspx?m=1");
                
            }
            catch (System.Threading.ThreadAbortException) { }
            catch (Exception ex)
            {
                log.Trace("SubmitOrganizationProfile: " + ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region SubmitAddNewOrganizationProfile
        protected void SubmitAddNewOrganizationProfile(Object sender,EventArgs e)
        {
            try
            {
                
                if (SetOrganizationProfile())
                {
                    Session["OrganizationToEdit"] = "new";
                    Response.Redirect("ManageOrganizationProfile.aspx");
                }
            }
            catch (System.Threading.ThreadAbortException) { }
            catch (Exception ex)
            {
                log.Trace("SubmitAddNewOrganizationProfile: " + ex.StackTrace + " : " + ex.Message);
            }
        }

        #endregion

        #region SetOrganizationProfile
        /// <summary>
        /// SetOrganizationProfile
        /// </summary>
        /// <returns></returns>
        protected bool SetOrganizationProfile()
        {
            try
            {
                //if (enableCloudInstallation == 0)//FB 2659 //ZD 100535 12/17/2013 inncrewin
                //{

                if (!validatelicense() && enableCloudInstallation == 0)//ZD 100535 12/17/2013 inncrewin
                    {
                        return false;
                    }

                    enableFacility = 0;
                    enableCatering = 0;
                    enableHK = 0;
                    enableAPI = 0;
                    //enablePC = 0; //FB 2693
                    enableCloud = 0;//FB 2262//FB 2599
                    enablePublicRoom = 0; //FB 2594
                    enableJabber = 0; enableLync = 0; enableVidtel = 0; //FB 2693
					enableAdvReport = 0;//FB 2593

                    if (ChkFacility.Checked == true)
                        enableFacility = 1;
                    if (ChkCatering.Checked == true)
                        enableCatering = 1;
                    if (ChkHK.Checked == true)
                        enableHK = 1;
                    if (ChkAPI.Checked == true)
                        enableAPI = 1;
                    //if (ChkPC.Checked == true) //FB 2347 //FB 2693
                    //    enablePC = 1;
                    if (ChkCloud.Checked == true) //FB 2262 - J  //FB 2599
                        enableCloud = 1;
                    if (ChkPublicRoom.Checked == true) //FB 2594
                        enablePublicRoom = 1;
                    //FB 2693 Starts                    
                    if (chkJabber.Checked == true)
                        enableJabber = 1;
                    if (chkLync.Checked == true)
                        enableLync = 1;
                    //ZD 102004
                    //if (chkVidtel.Checked == true)
                    //    enableVidtel = 1;
                    //FB 2693 Ends
					//FB 2593 Start
                    if (ChkAdvReport.Checked == true)
                    enableAdvReport = 1;
                    //FB 2593 End
                //} //ZD 100535 12/17/2013 inncrewin
                String inXML = "";
                inXML += "<SetOrganizationProfile>";
                inXML += "<Organization>";
                inXML += "<UserID>" + Session["userID"].ToString() + "</UserID>";
                inXML += "<OrgId>" + Session["OrganizationToEdit"].ToString() + "</OrgId>";
                inXML += "<OrgName>" + txtOrgName.Text + "</OrgName>";
                inXML += "<Address1>" + txtAddress1.Text + "</Address1>";
                inXML += "<Address2>" + txtAddress2.Text + "</Address2>";
                inXML += "<City>" + txtCity.Text + "</City>";
                inXML += "<State>" + lstStates.SelectedValue + "</State>";
                inXML += "<Country>" + lstCountries.SelectedValue + "</Country>";
                inXML += "<ZipCode>" + txtZipCode.Text + "</ZipCode>";
                inXML += "<Phone>" + txtPhoneNumber.Text + "</Phone>";
                inXML += "<Fax>" + txtFaxNumber.Text + "</Fax>";
                inXML += "<EmailID>" + txtEmailID.Text + "</EmailID>";
                inXML += "<Website>" + txtOrgWebsite.Text + "</Website>";
                inXML += "<Orgexpirykey>" + txtOrgexpirykey.Text + "</Orgexpirykey>";//FB 2678
                //if (enableCloudInstallation == 1)//FB 2659 //ZD 100535 12/17/2013 inncrewin
                //{
                //    inXML += "<Seats>" + TxtSeats.Text + "</Seats>";
                //    inXML += "<userlimit>" + TxtActiveUsers.Text + "</userlimit>";
                //    //ZD 100535 12/12/2013 Inncrewin
                //    inXML += "<mobileusers>" + TxtActiveUsers.Text + "</mobileusers>"; //100535
                //    inXML += "<ExchangeUserLimit>" + TxtActiveUsers.Text + "</ExchangeUserLimit>";
                //    inXML += "<videoroomlimit>" + TxtVideoRoomsTDB.Text + "</videoroomlimit>";
                //    inXML += "<nonvideoroomlimit>" + TxtNonVideoRoomsTDB.Text + "</nonvideoroomlimit>";
                //   inXML += "<endpointlimit>" + TxtEndPointsTDB.Text + "</endpointlimit>";
                //    //ZD 100535 12/12/2013 Inncrewin Ends
                //}
                //else
                //{
                //inXML += "<Seats>0</Seats>"; //ZD 100535 12/17/2013 inncrewin
                inXML += "<roomlimit>" + TxtRooms.Text + "</roomlimit>";
                inXML += "<videoroomlimit>" + TxtVRooms.Text + "</videoroomlimit>";
                inXML += "<nonvideoroomlimit>" + TxtNVRooms.Text + "</nonvideoroomlimit>";
                //inXML += "<userlimit>" + TxtUsers.Text + "</userlimit>"; //ZD 100535 12/17/2013 inncrewin
                //inXML += "<exchangeusers>" + TxtExUsers.Text + "</exchangeusers>"; //ZD 100535 12/17/2013 inncrewin
                inXML += "<dominousers>" + TxtDomUsers.Text + "</dominousers>";
                //inXML += "<mobileusers>" + TxtMobUsers.Text + "</mobileusers>"; //FB 1979//ZD 100535 12/17/2013 inncrewin
                inXML += "<WebexUsers>" + TxtWebexUsers.Text + "</WebexUsers>"; //ZD 100221
                inXML += "<BlueJeansUsers>" + TxtBJNUsers.Text + "</BlueJeansUsers>"; //ZD 103550
                inXML += "<mculimit>" + TxtMCU.Text + "</mculimit>";
                inXML += "<endpointlimit>" + TxtEndPoint.Text + "</endpointlimit>";
                inXML += "<mcuenchancedlimit>" + TxtMCUEncha.Text + "</mcuenchancedlimit>";//FB 2486
                inXML += "<vmrroomlimit>" + TxtVMR.Text + "</vmrroomlimit>";//FB 2586
                inXML += "<iControlroomlimit>" + txtiControl.Text + "</iControlroomlimit>";//ZD 101098
                
                //FB 2426 Start
                inXML += "<GuestRooms>" + TxtExtRooms.Text + "</GuestRooms>"; 
                inXML += "<GuestRoomPerUser>" + TxtGstPerUser.Text + "</GuestRoomPerUser>";
                Session["GuestRooms"] = TxtExtRooms.Text;
                //FB 2426 End
                inXML += "<enablefacilities>" + enableFacility + "</enablefacilities>";
                inXML += "<enablecatering>" + enableCatering + "</enablecatering>";
                inXML += "<enablehousekeeping>" + enableHK + "</enablehousekeeping>";
                inXML += "<enableAPI>" + enableAPI + "</enableAPI>";
                //inXML += "<enablePC>" + enablePC + "</enablePC>"; //FB 2347 //FB 2693
                inXML += "<enableCloud>" + enableCloud + "</enableCloud>"; //FB 2262 - J  //FB 2599
                inXML += "<EnablePublicRoom>" + enablePublicRoom + "</EnablePublicRoom>"; //FB 2594
                //FB 2693 Starts
                inXML += "<pcusers>" + txtPCUser.Text + "</pcusers>";
                
                inXML += "<enableJabber>" + enableJabber + "</enableJabber>";
                inXML += "<enableLync>" + enableLync+ "</enableLync>";
                inXML += "<enableVidtel>" + enableVidtel + "</enableVidtel>";
                //FB 2693 Ends
                //FB 2694 Starts
                inXML += "<VCHotRooms>" + txtVCHotRooms.Text + "</VCHotRooms>";
                inXML += "<ROHotRooms>" + txtROHotRooms.Text + "</ROHotRooms>";
                //FB 2694 End
				inXML += "<enableAdvReport>" + enableAdvReport + "</enableAdvReport>";//FB 2593
                //ZD 100535 12/17/2013 inncrewin
                if (enableCloudInstallation == 1)//FB 2659
                {
                    //ZD 100535 12/12/2013 Inncrewin
                    inXML += "<Seats>" + TxtSeats.Text + "</Seats>";
                    inXML += "<userlimit>" + TxtActiveUsers.Text + "</userlimit>";
                    inXML += "<mobileusers>" + TxtActiveUsers.Text + "</mobileusers>"; //100535
                    inXML += "<ExchangeUserLimit>" + TxtActiveUsers.Text + "</ExchangeUserLimit>";
                    inXML += "<exchangeusers>" + TxtActiveUsers.Text + "</exchangeusers>";
                    //ZD 100535 12/12/2013 Inncrewin Ends
                    //ZD 100518 Starts
                    inXML += "<MaxParticipants>" + txtMaxParticipant.Text + "</MaxParticipants>";
                    inXML += "<MaxConcurrentCall>" + txtMaxConcntCall.Text + "</MaxConcurrentCall>";
                    //ZD 100518 Ends
                }
                else
                {
                    inXML += "<Seats>0</Seats>";
                    inXML += "<userlimit>" + TxtUsers.Text + "</userlimit>";
                    inXML += "<mobileusers>" + TxtMobUsers.Text + "</mobileusers>"; //FB 1979
                    inXML += "<exchangeusers>" + TxtExUsers.Text + "</exchangeusers>";
                    //ZD 100518 Starts
                    inXML += "<MaxParticipants>0</MaxParticipants>";
                    inXML += "<MaxConcurrentCall>0</MaxConcurrentCall>";
                    //ZD 100518 Ends
                }
                //ZD 100535 12/17/2013 inncrewin
                inXML += "</Organization>";
                inXML += "</SetOrganizationProfile>";

                String outXML = obj.CallMyVRMServer("SetOrganizationProfile", inXML, Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") < 0)
                {
                     //Organization\CSS Module - Create folder for UI Settings --- Strat
                    if(Session["OrganizationToEdit"] != null)
                        if (Session["OrganizationToEdit"].ToString() == "new")
                        {
                            XmlDocument xd = new XmlDocument();
                            xd.LoadXml(outXML);

                            String orgID = xd.SelectSingleNode("//success/organizationID").InnerText.Trim();

                            cssUtil = new CustomizationUtil.CSSReplacementUtility();
                            cssUtil.ApplicationPath = Server.MapPath(".."); //FB 1830
                            orgID = "Org_" + orgID;
                            cssUtil.FolderName = orgID;
                            cssUtil.CreateOrgStyles();
                        }
                    //Organization\CSS Module -- End
                    errLabel.Text = "";
                    return true;
                }
                else
                {
                    //FB 1881 start
                    /*XmlDocument xd = new XmlDocument();
                    xd.LoadXml(outXML);
                    XmlNode nd = null;
                    nd = xd.SelectSingleNode("error");
                    errLabel.Text = nd.InnerText;*/
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    //FB 1881 end
                    errLabel.Visible = true;
                    return false;
                }
            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                errLabel.Visible = true;
                log.Trace("SetOrganizationProfile: " + ex.StackTrace + " : " + ex.Message);
                return false;
            }
        }
        #endregion

        #region ValidateLicense

        private Boolean validatelicense()
        {
            Int32 actMcus = 0, actUsers = 0, actEUsers = 0, actDUsers = 0, actMUsers = 0, actWebexUSers = 0, actBJNUsers = 0 ; //FB 1979 //ZD 100221
            Int32 actVRms = 0, actNVRms = 0, actEndpts = 0, actExtRooms = 0, actGstPerUser = 0; //FB 2426
            Int32 extVRms = 0, extNVRms = 0, extMcus = 0, extUsers = 0, extEUsers = 0, extMUsers = 0; //FB 1979
            Int32 extDUsers = 0, extEndpts = 0, extGuestRooms = 0;//FB 2426
            Int32 actMCUEnchanced = 0;//FB 2486
            Int32 extMCUEnchanced = 0;//FB 2486
            Int32 actVMRRooms = 0;//FB 2586
            Int32 extVMR = 0;//FB 2586
            Int32 actiControlRooms = 0;//ZD 101098
            Int32 extiControl = 0;//ZD 101098
            int actPCUsers = 0, extPCUsers = 0, extWebexUsers = 0; //FB 2693 ZD 100221 WebEx
            int actVCHotRooms = 0, actROHotRooms = 0; //FB 2694
            int extBJNUsers = 0;//ZD 103550
            try
            {
                Int32.TryParse(TxtMCU.Text, out actMcus);
                Int32.TryParse(TxtUsers.Text, out actUsers);
                Int32.TryParse(TxtExUsers.Text, out actEUsers);
                Int32.TryParse(TxtDomUsers.Text, out actDUsers);
                Int32.TryParse(TxtMobUsers.Text, out actMUsers); //FB 1979
                Int32.TryParse(TxtWebexUsers.Text, out actWebexUSers); //ZD 100221
                Int32.TryParse(TxtBJNUsers.Text, out actBJNUsers); //ZD 103550
                Int32.TryParse(TxtVRooms.Text, out actVRms);
                Int32.TryParse(TxtNVRooms.Text, out actNVRms);
                Int32.TryParse(TxtEndPoint.Text, out actEndpts);
                Int32.TryParse(TxtMCUEncha.Text, out actMCUEnchanced);//FB 2486
                //FB 2426 Start
                Int32.TryParse(TxtExtRooms.Text, out actExtRooms); 
                Int32.TryParse(TxtGstPerUser.Text, out actGstPerUser);
                //FB 2426 End
                Int32.TryParse(TxtVMR.Text, out actVMRRooms);//FB 2586
                Int32.TryParse(txtiControl.Text, out actiControlRooms);//ZD 101098
                Int32.TryParse(txtVCHotRooms.Text, out actVCHotRooms);//FB 2694
                Int32.TryParse(txtROHotRooms.Text, out actROHotRooms);//FB 2694

                Int32.TryParse(hdnUsers.Value, out currUsers);
                Int32.TryParse(hdnEUsers.Value, out currEUsers);
                Int32.TryParse(hdnDUsers.Value, out currDUsers);
                Int32.TryParse(hdnMUsers.Value, out currMUsers); //FB 1979
                Int32.TryParse(hdnWebexUsers.Value, out currWebexUsers); //ZD 100221
                if(hdnBJNUsers.Value != "-2")
                    Int32.TryParse(hdnBJNUsers.Value, out currBJNUsers); //ZD 103550
                Int32.TryParse(hdnMCU.Value, out currMCU);
                Int32.TryParse(hdnVRooms.Value, out currVRooms);
                Int32.TryParse(hdnNVRooms.Value, out currNVRooms);
                Int32.TryParse(hdnEndPoint.Value, out currEndpoint);
                Int32.TryParse(hdnMCUEncha.Value, out currMCUEnchanced);//FB 2486
                //FB 2426 Start
                Int32.TryParse(hdnExtRooms.Value, out currExtRooms); 
                Int32.TryParse(hdnGstPerUser.Value, out currGstPerUser);
                //FB 2426 End
                Int32.TryParse(hdnVMR.Value, out currVMRRooms);//FB 2586
                Int32.TryParse(hdniControl.Value, out curriControlRooms);//ZD 101098
                //FB 2693 Starts
                int.TryParse(txtPCUser.Text, out actPCUsers);
                int.TryParse(hdnPCUser.Value, out currPCUsers);
                //FB 2693 Ends
                Int32.TryParse(hdnVCHotdesking.Value, out currVCHotRooms);//FB 2694
                Int32.TryParse(hdnROHotdesking.Value, out currROHotRooms);//FB 2694
                //FB 1881 start                
                if (actVRms > currVRooms)
                    throw new Exception(obj.GetErrorMessage(455)); //"Video rooms limit exceeds VRM license."

                if (actNVRms > currNVRooms)
                    throw new Exception(obj.GetErrorMessage(456)); //"Non-Video rooms limit exceeds VRM license.");
                //FB 2694 Starts
                if (actVCHotRooms > currVCHotRooms)
                    throw new Exception(obj.GetErrorMessage(693)); //"VC Hotdesking rooms limit exceeds VRM license.");

                if (actROHotRooms > currROHotRooms)
                    throw new Exception(obj.GetErrorMessage(694)); //"RO Hotdesking rooms limit exceeds VRM license.");
                //FB 2694 End
                if (actMcus > currMCU)
                    throw new Exception(obj.GetErrorMessage(457)); //"MCU limit exceeds VRM license.");

                if (actMCUEnchanced > currMCUEnchanced)
                    throw new Exception(obj.GetErrorMessage(624)); //"MCU Enchanced limit exceeds VRM license.");

                if (actMCUEnchanced > actMcus)//FB 2486 
                    throw new Exception(obj.GetErrorMessage(626)); //"MCU Enchanced limit exceeds active Standard MCU.");

                if (actEndpts > currEndpoint)
                    throw new Exception(obj.GetErrorMessage(458)); //"Endpoints limit exceeds VRM license.");
                
                //ZD 101443
                if (isLDAP == 0 && actUsers > currUsers)
                    throw new Exception(obj.GetErrorMessage(459)); //"User limit exceeds VRM license.");

                if(!(isLDAP == 1 && enableCloudInstallation == 1))
                {
                    if (actEUsers > currEUsers)
                        throw new Exception(obj.GetErrorMessage(460)); //"Exchange user limit exceeds VRM license.");
                }

                if (actDUsers > currDUsers)
                    throw new Exception(obj.GetErrorMessage(461)); //"Domino user limit exceeds VRM license.");

                if (!(isLDAP == 1 && enableCloudInstallation == 1))
                {
                    if (actMUsers > currMUsers) //FB 1979
                        throw new Exception(obj.GetErrorMessage(526)); //"Mobile user limit exceeds VRM license.");
                }

                if (actWebexUSers > currWebexUsers) //ZD 100221 WebEx
                    throw new Exception(obj.GetErrorMessage(718)); //"Webex user limit exceeds VRM license.");

                if (hdnBJNUsers.Value != "-2")
                {
                    if (actBJNUsers > currBJNUsers) //ZD 103550
                        throw new Exception(obj.GetErrorMessage(761)); //"Blue Jeans user limit exceeds VRM license.");
                }

                //FB 2693 Starts
                if (actPCUsers > currPCUsers)
                    throw new Exception(obj.GetErrorMessage(682));  //PC user limit exceeds VRM license.
                //FB 26963 Ends
                //ZD 100634 start
                //ZD 101443
                if (isLDAP == 0 && (actEUsers > actUsers || actDUsers > actUsers || actMUsers > actUsers || actPCUsers > actUsers || actWebexUSers > actUsers || actBJNUsers > actUsers))
                    throw new Exception(obj.GetErrorMessage(715));
                

                //if ((actEUsers + actDUsers + actMUsers + actPCUsers) > actUsers) //FB 1979 //FB 2693
                //    throw new Exception(obj.GetErrorMessage(462)); //"Active users limit should be inclusive of both domino and exchange users.");

                //if (actWebexUSers > actUsers) //ZD 100221 WebEx
                //    throw new Exception(obj.GetErrorMessage(720));  //"Webex user limit exceeds Users limit. ");

                //ZD 100634 End

                //FB 2426 Start
                if (actExtRooms > currExtRooms) 
                    throw new Exception(obj.GetErrorMessage(618)); //"Guest Room limit exceeds VRM license.");

                if (actGstPerUser > actExtRooms)
                    throw new Exception(obj.GetErrorMessage(619)); //"Guest Room limit per user exceeds Guest Room limit.");
                //FB 2426 End

               

                if (!Session["OrganizationToEdit"].ToString().ToLower().Equals("new"))
                {
                    String inXML = "<GetActiveOrgDetails>";
                    inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                    inXML += "  <organizationID>" + Session["OrganizationToEdit"].ToString() + "</organizationID>";
                    inXML += "  </GetActiveOrgDetails>";
                    String outXML = obj.CallMyVRMServer("GetActiveOrgDetails", inXML, Application["MyVRMServer_ConfigPath"].ToString());

                    if (outXML.IndexOf("<error>") >= 0)
                        throw new Exception(obj.ShowErrorMessage(outXML));

                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);

                    Int32.TryParse(xmldoc.SelectSingleNode("//GetActiveOrgDetails/ActiveVideoRoom").InnerText, out extVRms);
                    Int32.TryParse(xmldoc.SelectSingleNode("//GetActiveOrgDetails/ActiveNonVideoRoom").InnerText, out extNVRms);
                    Int32.TryParse(xmldoc.SelectSingleNode("//GetActiveOrgDetails/ActiveMCU").InnerText, out extMcus);
                    Int32.TryParse(xmldoc.SelectSingleNode("//GetActiveOrgDetails/ActiveEndPoints").InnerText, out extEndpts);
                    Int32.TryParse(xmldoc.SelectSingleNode("//GetActiveOrgDetails/ActiveUsers").InnerText, out extUsers);
                    Int32.TryParse(xmldoc.SelectSingleNode("//GetActiveOrgDetails/ActiveExchangeUsers").InnerText, out extEUsers);
                    Int32.TryParse(xmldoc.SelectSingleNode("//GetActiveOrgDetails/ActiveDominoUsers").InnerText, out extDUsers);
                    Int32.TryParse(xmldoc.SelectSingleNode("//GetActiveOrgDetails/ActiveMobileUsers").InnerText, out extMUsers); // FB 1979
                    Int32.TryParse(xmldoc.SelectSingleNode("//GetActiveOrgDetails/ActiveGuestRooms").InnerText, out extGuestRooms); // FB 2426
                    Int32.TryParse(xmldoc.SelectSingleNode("//GetActiveOrgDetails/ActiveMCUEnchanced").InnerText, out extMCUEnchanced);//FB 2486
                    Int32.TryParse(xmldoc.SelectSingleNode("//GetActiveOrgDetails/ActiveVMRRooms").InnerText, out extVMR);//FB 2586
                    Int32.TryParse(xmldoc.SelectSingleNode("//GetActiveOrgDetails/ActiveiControlRooms").InnerText, out extiControl);//ZD 101098
                    Int32.TryParse(xmldoc.SelectSingleNode("//GetActiveOrgDetails/ActivePCUsers").InnerText, out extPCUsers);//FB 2693
                    Int32.TryParse(xmldoc.SelectSingleNode("//GetActiveOrgDetails/ActiveWebexUsers").InnerText, out extWebexUsers);//ZD 100221 WebEx
                    Int32.TryParse(xmldoc.SelectSingleNode("//GetActiveOrgDetails/ActiveBlueJeanUsers").InnerText, out extBJNUsers);//ZD 100221 WebEx

                    if (actVRms < extVRms)
                        throw new Exception(obj.GetErrorMessage(463)); //"Please deactivate video rooms to reduce the count.As there are more active video rooms.");
                    if (actNVRms < extNVRms)
                        throw new Exception(obj.GetErrorMessage(464)); //"Please deactivate non-video rooms to reduce the count.As there are more active non-video rooms.");
                    if (actMcus < extMcus)
                        throw new Exception(obj.GetErrorMessage(465)); //"Please delete mcu to reduce the count.As there are more active mcu.");
                    if (actEndpts < extEndpts)
                        throw new Exception(obj.GetErrorMessage(466)); //"Please delete endpoints to reduce the count.As there are more active endpoints.");
                    if (isLDAP == 0 && actUsers < extUsers) //ZD 101443
                        throw new Exception(obj.GetErrorMessage(467)); //"Please delete users to reduce the count.As there are more active users.");
                    if (!(isLDAP == 1 && enableCloudInstallation == 1))
                    {
                        if (actEUsers < extEUsers)
                            throw new Exception(obj.GetErrorMessage(482)); //"Please delete exchange users to reduce the count.As there are more active exchange users.");
                    }
                    if (actDUsers < extDUsers)
                        throw new Exception(obj.GetErrorMessage(494));//"Please delete domino users to reduce the count.As there are more active domino users."
                    if (!(isLDAP == 1 && enableCloudInstallation == 1))
                    {
                        if (actMUsers < extMUsers) // FB 1979
                            throw new Exception(obj.GetErrorMessage(527));//"Please delete mobile users to reduce the count.As there are more active mobile users."
                    }
                    
                    if (actPCUsers < extPCUsers) // FB 2693
                        throw new Exception(obj.GetErrorMessage(672)); 
 
                    //FB 1881 end
                    if (actExtRooms < extGuestRooms) // FB 2426
                        throw new Exception(obj.GetErrorMessage(615));//"Please delete External Rooms to reduce the count.As there are more active External Rooms."

                    if (actMCUEnchanced < extMCUEnchanced) //FB 2537
                        throw new Exception(obj.GetErrorMessage(628));//"Please delete MCU Enhanced to reduce the count.As there are more active MCU Enhanced.");

                    if (actVMRRooms < extVMR) //FB 2586
                        throw new Exception(obj.GetErrorMessage(657));//"Please deactivate VMR rooms to reduce the count.As there are more active VMR rooms.");

                    if (actiControlRooms < extiControl) //ZD 101098
                        throw new Exception(obj.GetErrorMessage(733));//"Please deactivate iControl rooms to reduce the count.As there are more active iControl rooms.");


                    if (actWebexUSers < extWebexUsers) // ZD 100221 WebEx
                        throw new Exception(obj.GetErrorMessage(717));//"Please delete webex users to reduce the count.As there are more active webex users."
                    
                    if(actBJNUsers < extBJNUsers &&  hdnBJNUsers.Value != "-2") //ZD 104116
                        throw new Exception(obj.GetErrorMessage(759));//"Please delete Blue Jean users to reduce the count.As there are more active Blue Jean users."
                    
                }
            }
            catch (Exception ex)
            {
               // errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                errLabel.Text = ex.Message;
                errLabel.Visible = true;
                log.Trace("SetOrganizationProfile: " + ex.StackTrace + " : " + ex.Message);
                return false;
            }
            return true;
        }

        #endregion

        //FB 2678 Start

        #region btnExportTxt_Click

        protected void btnExportTxt_Click(object sender, EventArgs e)
        {
            string orgid = "", Encrypted = "";
            XmlDocument docs = null;
            try
            {
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=LicenseActivationText.txt");
                Response.Charset = "";
                orgid = "ID: " + Session["OrganizationToEdit"].ToString();

                String inxmls = "<System><Cipher>" + orgid + "</Cipher></System>";

                string outXML = obj.CallMyVRMServer("GetEncrpytedText", inxmls, Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") < 0)
                {
                    docs = new XmlDocument();
                    docs.LoadXml(outXML);
                    XmlNode nde = docs.SelectSingleNode("System/Cipher");
                    if (nde != null)
                        Encrypted = nde.InnerXml;
                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                }


                StringBuilder sb = new StringBuilder(Encrypted);
                StringWriter sw = new StringWriter(sb);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
                sw.Close();
                sw.Dispose();
                sb = null;
            }
            catch (Exception ex)
            { }

        }

        #endregion

        //FB 2678 End
    }
}
