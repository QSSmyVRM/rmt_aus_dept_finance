﻿/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 //ZD 100886
#region Namespaces
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Web.Services;
using System.Xml;
using System.Collections.Generic;
using System.Web.Script.Services;
using System.Text;
#endregion

#region ns_point2point
public partial class ns_point2point : System.Web.UI.Page
{
    # region prviate DataMember
    //FB 2646  Starts
    protected System.Web.UI.WebControls.CheckBox chkAllSilo;   
    protected System.Web.UI.WebControls.Label lblchksilo;
        
    //FB 2646  Ends
    #endregion

    #region Global Variable declaration
    protected int gblConfIndex;
    protected string[] MCUConfAddr;
    protected string[] ConfPartList;
    protected string[] ConfPartId;
    protected XDocument xd;
    protected XElement nodes;
    protected XElement tree;
    myVRMNet.NETFunctions obj;
    ns_Logger.Logger log;
    protected System.Web.UI.WebControls.GridView grdParent;
    protected System.Web.UI.WebControls.DropDownList lstLineRate;
    //protected System.Web.UI.HtmlControls.HtmlImage streamCallerID;
    //protected System.Web.UI.HtmlControls.HtmlImage streamCalleeID;
    protected System.Web.UI.HtmlControls.HtmlSelect lstCalendar; // FB 101216  
    //FB 2616 Start
    MyVRMNet.LoginManagement loginMgmt = null;
    String userdetails = "";
    //FB 2616 End
    #endregion

    #region Class_Conference
    /// <summary>
    /// Conference Class
    /// </summary>
    public class Conference
    {
        public string confId { get; set; }
        public string confStatus { get; set; }
        public string mcuAlias { get; set; }
        public string confName { get; set; }
        public string xconfName { get; set; }
        public string confType { get; set; }
        public string siloName { get; set; } //FB 2646
        public string confDate { get; set; }
        public string confserverTime { get; set; }
        public string duration { get; set; }
        public TimeSpan ConfFinishingTime { get; set; }
        public string confActualStatus { get; set; }
        public string connectingStatus { get; set; }
        public string confLastRun { get; set; }
        public string xparentId { get; set; }
        public string xmergeString { get; set; }
        public int confCount { get; set; } 
        public string confDate1 { get; set; }
        public string conforgID { get; set; } //FB 2646
        public string confmessage { get; set; } //ZD 101133
    }
    #endregion

    #region Class_Participant
    /// <summary>
    /// Participant Class
    /// </summary>
    public class Participant
    {

        public string confid { get; set; }
        public string xchildId { get; set; }
        public string mergStr { get; set; }
        public string participantName { get; set; }
        public string partAddr { get; set; }
        public string connectionStatus { get; set; }
        public string conPartyId { get; set; }
        public string conTerminalType { get; set; }


        public string CallType { get; set; }
        public string partyCallStatusinfo { get; set; }
        public string partyCallStatus { get; set; }

        public string partytotalRowCount { get; set; }
        public string partyCurrentRowIndex { get; set; }
        public int partyMcuIndex { get; set; }
        public int partyConfIndex { get; set; }
        public int partyCurIndex { get; set; }

        public string partyrxpreviewURL { get; set; }
    }
    #endregion

    #region ns_point2point
    public ns_point2point()
    {
        //
        // TODO: Add constructor logic here
        //
        obj = new myVRMNet.NETFunctions();
        log = new ns_Logger.Logger();
    }
    #endregion

    //ZD 101022
    #region InitializeCulture
    protected override void InitializeCulture()
    {
        if (Session["UserCulture"] != null)
        {
            UICulture = Session["UserCulture"].ToString();
            Culture = Session["UserCulture"].ToString();
            //ZD 101714
            if (Session["UserCulture"].ToString() == "fr-CA")
                Culture = "en-US";
            base.InitializeCulture();
        }
    }
    #endregion

    #region Page_Load
    /// <summary>
    /// Page_Load
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        //int index = e.Row.RowIndex;
        //gblMcuIndex = index;

        String inXml = "";
        try
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            obj.AccessandURLConformityCheck("point2point.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263

            // ZD 101216
            if (!IsPostBack)
            {
                lstCalendar.Items.Clear();
                lstCalendar.Items.Add(new ListItem(obj.GetTranslatedText("Call Monitor (P2P)"), "5")); 
                lstCalendar.Items.Add(new ListItem(obj.GetTranslatedText("Call Monitor"), "4"));
            }

            //FB 2646 Starts
            if (Session["UsrCrossAccess"] != null)
            {
                if (Session["UsrCrossAccess"].ToString().Equals("1") && Session["organizationID"].ToString() == "11" && Session["OrganizationsLimit"].ToString().Trim() != "1")
                {
                    chkAllSilo.Attributes.Add("style", "display:block");
                    lblchksilo.Attributes.Add("style", "display:block");
                }
                else
                {
                    chkAllSilo.Attributes.Add("style", "display:none");
                    lblchksilo.Attributes.Add("style", "display:none");
                }
            }
            //FB 2646 Ends

            //FB 2616 Start
            if (Request.QueryString["tp"] == "P2P")
            {
                loginMgmt = new MyVRMNet.LoginManagement();
                if (Request.QueryString["req"] != null)
                    userdetails = Request.QueryString["req"];

                loginMgmt.simpleDecrypt(ref userdetails);
                string[] id = userdetails.Split(',');
                loginMgmt.queyStraVal = id[0].ToString();
                loginMgmt.qStrPVal = id[1].ToString();
                loginMgmt.GetHomeCommand();
            }
            //FB 2616 End

            inXml = "<P2PCallMonitor>";
            if (!chkAllSilo.Checked) //FB 2646
                inXml += obj.OrgXMLElement();
            inXml += "<UserID>" + Session["userID"].ToString() + "</UserID>";
            inXml += "<ConferenceID></ConferenceID><ConferenceUniqueID></ConferenceUniqueID>";
            inXml += "<StatusFilter><ConferenceStatus>5</ConferenceStatus></StatusFilter>";
            inXml += "<TypeFilter><ConfType>4</ConfType></TypeFilter>";
            inXml += "</P2PCallMonitor>";




            String outxml = obj.CallMyVRMServer("GetP2PCallsforMonitor", inXml.ToString(), Application["MyVRMServer_ConfigPath"].ToString());


            XmlDocument xdoc = new XmlDocument();
            xdoc.LoadXml(outxml);
            xd = XDocument.Parse(xdoc.OuterXml);
            //FB 2588 Start
            string tformat = "";
            if (Session["timeFormat"].ToString().Equals("0"))
                tformat = "MM/dd/yyyy HH:mm";
            else if (Session["timeFormat"].ToString().Equals("1"))
                tformat = "MM/dd/yyyy hh:mm tt";
            else if (Session["timeFormat"].ToString().Equals("2"))
                tformat = "MM/dd/yyyy HHmmZ";
            //FB 2588 End

            //ZD 100995
            if (Session["EmailDateFormat"].ToString() == "1")
                tformat = "dd MMM yyyy " + tformat.Split(' ')[1];     

            //int k = 0;
            
            var conf = (from XMcuConf in xd.Descendants("Conference")
                        select new Conference
                        {
                            confId = Convert.ToString(XMcuConf.Element("ConferenceID").Value),
                            confActualStatus = Convert.ToString(XMcuConf.Element("ConferenceActualStatus").Value),
                            mcuAlias = Convert.ToString(XMcuConf.Element("ConferenceUniqueID").Value),
                            confName = Convert.ToString(XMcuConf.Element("ConferenceName").Value) + " (" + Convert.ToString(XMcuConf.Element("ConferenceUniqueID").Value) + ")",
                            xconfName = Convert.ToString(XMcuConf.Element("ConferenceName").Value), // + gblMcuIndex.ToString(),
                            confType = Convert.ToString(XMcuConf.Element("ConferenceType").Value),
                            siloName = obj.GetTranslatedText("Silo: ") + Convert.ToString(XMcuConf.Element("SiloName").Value), //FB 2646
                            confDate = DateTime.Parse(XMcuConf.Element("ConferenceDateTime").Value).ToString(tformat),//FB 2501 Dec7 //FB 2588
                            confDate1 = Convert.ToString(XMcuConf.Element("ConferenceDateTime").Value),//FB 2501 Dec10
                            //confserverTime = Convert.ToString(XMcuConf.Element("ServerDateTime").Value),
                            duration = ((Convert.ToInt32(XMcuConf.Element("ConferenceDuration").Value) / 60) + "hrs : " + ((Convert.ToInt32(XMcuConf.Element("ConferenceDuration").Value)) - (Convert.ToInt32(XMcuConf.Element("ConferenceDuration").Value) / 60) * 60) + "mins"), //ZD 100528
                            ConfFinishingTime = (Convert.ToDateTime(Convert.ToString(XMcuConf.Element("ConferenceDateTime").Value)).AddMinutes(Convert.ToInt64(XMcuConf.Element("ConferenceDuration").Value))).Subtract(Convert.ToDateTime(Convert.ToString(XMcuConf.Element("ServerDateTime").Value))), 
                            confStatus = Convert.ToString(XMcuConf.Element("ConferenceStatus").Value),
                            connectingStatus = Convert.ToString(XMcuConf.Element("CallStartMode").Value),
                            confLastRun = Convert.ToString(XMcuConf.Element("LastRunDate").Value),
                            xparentId = Convert.ToString(XMcuConf.Element("ConferenceUniqueID").Value),
                            confCount = Convert.ToInt32(xdoc.SelectSingleNode("//SearchConference/Conferences/ConferenceCount").InnerXml),
                            //xmergeString = Convert.ToString(XMcuConf.Element("ConferenceID").Value)
                            conforgID = Convert.ToString(XMcuConf.Element("ConferenceOrgID").Value), // FB 2646
                            confmessage = Convert.ToString(XMcuConf.Element("Confmessage").Value) //ZD 101133
                        }).ToList();


            ConfPartList = (from node in xd.Descendants("Conference")
                            select node.Element("PartyList").Value).ToArray();

            ConfPartId = (from node in xd.Descendants("Conference")
                          select node.Element("ConferenceUniqueID").Value).ToArray();

            lstLineRate.ClearSelection();
            if (lstLineRate.Items.Count <= 0)
                obj.BindLineRate(lstLineRate);

            grdParent.DataSource = conf;
            grdParent.DataBind();



        }
        catch (Exception ex)
        {
            log.Trace("Page_Load" + ex.Message);
        }
    }
    #endregion

    #region bindParticipant
    protected void bindParticipant(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                Label confUid = (Label)e.Row.FindControl("OngoingConfUniqueID");
                Label confid = (Label)e.Row.FindControl("OngoingconfID");

                int index = e.Row.RowIndex;
                gblConfIndex = index;

                nodes = (XElement)(from p in xd.Descendants("Conference")
                                   where p.Element("ConferenceUniqueID").Value == ConfPartId[index].ToString()
                                   select p).Single();

                if (ConfPartList[index].ToString() != "")
                {
                    int i = 0;
                    var party = (from XMcuPart in nodes.Descendants("PartyList").Descendants("Party")
                                 select new Participant
                                 {
                                     participantName = Convert.ToString(XMcuPart.Element("Name").Value),
                                     partAddr = (Convert.ToString(XMcuPart.Element("Address").Value)).Trim(),
                                     connectionStatus = Convert.ToString(XMcuPart.Element("Connectstatus").Value),
                                     conPartyId = Convert.ToString(XMcuPart.Element("PartyId").Value),
                                     conTerminalType = Convert.ToString(XMcuPart.Element("TerminalType").Value),
                                     partyCallStatus = ((Convert.ToString(XMcuPart.Element("Connectstatus").Value) == "2") ? "0" : ((Convert.ToString(XMcuPart.Element("Connectstatus").Value) == "0") ? "1" : "0")),
                                     partyCallStatusinfo = Convert.ToString(XMcuPart.Element("Connectstatus").Value),

                                     CallType = Convert.ToString(XMcuPart.Element("CallType").Value) == "1" ? obj.GetTranslatedText("Caller") : obj.GetTranslatedText("Callee"), //ZD 100288

                                     partyConfIndex = gblConfIndex,
                                     partyCurIndex = i++,

                                     partyrxpreviewURL = XMcuPart.Element("rxPreviewURL").Value,
                                     xchildId = confUid.Text + "__" + gblConfIndex,
                                     confid = confid.Text

                                 }).ToList();

                    
                    // ZD 101068 Starts
                    string imgurl;
                    PlaceHolder plhStreamImg = (PlaceHolder)e.Row.FindControl("plhp2pgrdChild1");
                    Table tbl = new Table();
                    tbl.Attributes.Add("align", "right");
                    tbl.Style.Add("width", "95%");
                    TableRow tr = new TableRow();
                    TableCell tc = null;
                    tbl.Rows.Add(tr);
                    HtmlImage htmImg = null;
                    for (int j = 0; j < party.Count; j++)
                    {

                        imgurl = party[j].partyrxpreviewURL;
                        if (imgurl == "")
                            imgurl = "../image/MonitorMCU/blank.jpg";

                        htmImg = new HtmlImage();
                        htmImg.Src = imgurl;
                        htmImg.Attributes.Add("width", "150px");

                        tc = new TableCell();
                        tc.Controls.Add(htmImg);
                        tr.Cells.Add(tc);
                    }
                    plhStreamImg.Controls.Add(tbl);
                    // ZD 101068 Ends

                    GridView gvChild = (GridView)e.Row.FindControl("p2pgrdChild2");

                    gvChild.DataSource = party;
                    gvChild.DataBind();
                }
            }
        }
        catch (Exception ex)
        {
            log.Trace("bindparticipant" + ex.Message);
        }
    }
    #endregion

    #region connect Or Disconnect
    /// <summary>
    /// fnconnectOrDisconnect
    /// </summary>
    /// <param name="userID"></param>
    /// <param name="confID"></param>
    /// <param name="EndpointID"></param>
    /// <param name="terminalType"></param>
    /// <param name="ImgStatus"></param>
    /// <param name="fnidentification"></param>
    /// <param name="fnmethod"></param>
    /// <returns></returns>
    [WebMethod]
    public static string fnconnectOrDisconnect(string userID, string confID, string EndpointID, string terminalType, string ImgStatus)
    {
        myVRMNet.NETFunctions obj;
        obj = new myVRMNet.NETFunctions();
        ns_Logger.Logger log;
        log = new ns_Logger.Logger();
        string res = "";
        StringBuilder inXML = new StringBuilder();
        String outXML = "";
        try
        {
            outXML = ConnectDisconnect(userID, confID, EndpointID, terminalType, ImgStatus);

            if (outXML.IndexOf("<error>") >= 0)
                res = obj.GetTranslatedText("Operation UnSuccessful");
            else
                if (ImgStatus == "1")
                    res = "0";
                else
                    res = "1";
        }
        catch (Exception ex)
        {
            log.Trace("fnAudioVideoStatus" + ex.Message);
        }
        return res;
    }
    #endregion

    #region Send Message
    /// <summary>
    /// fnSaveComments
    /// </summary>
    /// <param name="userID"></param>
    /// <param name="confID"></param>
    /// <param name="endpointID"></param>
    /// <param name="terminalType"></param>
    /// <param name="Message"></param>
    /// <param name="Direction"></param>
    /// <param name="Duration"></param>
    /// <param name="fnIdentification"></param>
    /// <returns></returns>
    [WebMethod]
    public static string fnSaveComments(string userID, string confID, string Message)
    {
        myVRMNet.NETFunctions obj;
        obj = new myVRMNet.NETFunctions();
        ns_Logger.Logger log;
        log = new ns_Logger.Logger();
        String outXML = "";
        string res = "";
        StringBuilder inXML = new StringBuilder();
        try
        {
            inXML.Append("<login>");
            inXML.Append("<userID>" + userID + "</userID>");
            inXML.Append("<confID>" + confID + "</confID>");
            inXML.Append("<messageText>" + Message + "</messageText>");
            inXML.Append("</login>");
            outXML = obj.CallCOM2("SendMessageToConference", inXML.ToString(), HttpContext.Current.Application["RTC_ConfigPath"].ToString());
            XmlDocument xdoc = new XmlDocument();
            xdoc.LoadXml(outXML);
            if (outXML.IndexOf("<error>") >= 0)
                res = obj.GetTranslatedText("Operation UnSuccessful");
            else
                res = obj.GetTranslatedText("Operation Successful");

        }
        catch (Exception ex)
        {
            log.Trace("SendMessage" + ex.Message);
        }
        return res;
    }
    #endregion

    #region Conference Extend Time
    /// <summary>
    /// fnTimeExpand
    /// </summary>
    /// <param name="userID"></param>
    /// <param name="confID"></param>
    /// <param name="Duration"></param>
    /// <returns></returns>
    [WebMethod]
    public static string fnTimeExpand(string userID, string confID, string Duration)
    {
        myVRMNet.NETFunctions obj;
        obj = new myVRMNet.NETFunctions();
        ns_Logger.Logger log;
        log = new ns_Logger.Logger();
        string res = "";
        StringBuilder inXML = new StringBuilder();
        string outXML = "";
        try
        {
            inXML.Append("<login>");
            inXML.Append(obj.OrgXMLElement());
            inXML.Append("<userID>" + userID + "</userID>");
            inXML.Append("<confInfo>");
            inXML.Append("<confID>" + confID + "</confID>");
            inXML.Append("<retry>0</retry>");
            inXML.Append("<extendEndTime>" + Duration + "</extendEndTime>");
            inXML.Append("</confInfo>");
            inXML.Append("</login>");

            //ZD 100819 Start
            outXML = obj.CallMyVRMServer("CheckForExtendTimeConflicts", inXML.ToString(), HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
            if (outXML.IndexOf("<error>") >= 0)
            {
                res = obj.GetTranslatedText("Operation UnSuccessful");
            }
            else
            {
                outXML = obj.CallCOM2("SetTerminalControl", inXML.ToString(), HttpContext.Current.Application["RTC_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") >= 0)
                {
                    res = obj.ShowErrorMessage(outXML);
                }
                else
                {
                    outXML = obj.CallMyVRMServer("SetTerminalControl", inXML.ToString(), HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                    if (outXML.IndexOf("<error>") >= 0)
                        res = obj.GetTranslatedText("Operation UnSuccessful");
                    else
                        res = obj.GetTranslatedText("Operation Successful");
                }
            }
            //ZD 100819 End
        }
        catch (Exception ex)
        {
            log.Trace("fnTimeExpand" + ex.Message);
        }
        return res;

    }
    #endregion

    //Method Changed for FB 2569 Start

    #region GetEventLogs
    /// <summary>
    /// fnGetEventLogs
    /// </summary>
    /// <param name="userID"></param>
    /// <param name="confID"></param>
    /// <returns></returns>
    [WebMethod]
    public static string fnGetEventLogs(string userID, string confID)
    {
        myVRMNet.NETFunctions obj;
        obj = new myVRMNet.NETFunctions();
        ns_Logger.Logger log;
        log = new ns_Logger.Logger();
        string res = "";
        StringBuilder inXML = new StringBuilder();
        String outXML = "";
        try
        {
            inXML.Append("<GetConferenceAlerts>");
            inXML.Append(obj.OrgXMLElement());
            inXML.Append("<userID>" + userID + "</userID>");
            inXML.Append("<ConferenceID>" + confID + "</ConferenceID>");
            inXML.Append("<P2PEvents>1</P2PEvents>");
            inXML.Append("</GetConferenceAlerts>");
            outXML = obj.CallCOM2("GetConferenceAlerts", inXML.ToString(), HttpContext.Current.Application["RTC_ConfigPath"].ToString());

            if (outXML.IndexOf("<error>") >= 0)
                res = obj.GetTranslatedText("Operation UnSuccessful");
            else
            {
                XDocument xd;
                XmlDocument xdoc = new XmlDocument();
                xdoc.LoadXml(outXML);
                xd = XDocument.Parse(xdoc.OuterXml);
                var recordCount = (from m in xd.Descendants("Alert") select m).Count();
                string[] ConfMsg = (from node in xd.Descendants("Alert") select node.Element("Message").Value).ToArray();
                string[] ConfDate = (from node in xd.Descendants("Alert") select node.Element("Timestamp").Value).ToArray();


                var htmlcontent = "";

                if (recordCount != 0)
                {
                    htmlcontent = "<table style='border-collapse:collapse; background-color:#F7EBF3;' border='1px solid red' width='98%'><tr><th width='10%'>S.NO</th><th width='30%'>'" + obj.GetTranslatedText("Date and Time") + " '</th><th width='60%'>'" + obj.GetTranslatedText("Message") + " '</th></tr></table>";//ZD 100288
                    htmlcontent += "<table style='border-collapse:collapse;' border='1px solid red' width='98%'>";
                    for (int i = 0; i < recordCount; i++)
                    {
                        htmlcontent += "<tr><td align ='center' width='10%' >" + i + "</td><td align='center' width='30%'>" + ConfDate[i].ToString() + "</td><td align='left' style='padding-left:20px;' width='60%'>" + ConfMsg[i].ToString() + "</td></tr>";
                    }
                }
                else
                {
                    htmlcontent = "<table border='0' cellpadding='0' cellspacing='0' width='100%'><tr><td colspan='3' height='20px'></td></tr>";
                    htmlcontent += "<tr><td colspan='3' align='center'><b style='font-family:Verdana;' >" + obj.GetTranslatedText("No Events") + "</b><br><br><br><br></td></tr>";
                }
                htmlcontent += "<tr></table>";
                res = htmlcontent.ToString();
            }
        }
        catch (Exception ex)
        {
            log.Trace("PacketDetails" + ex.Message);
        }
        return res;
    }
    #endregion

    //Method Changed for FB 2569 End

    #region Delete Command
    /// <summary>
    /// fndelete
    /// </summary>
    /// <param name="userID"></param>
    /// <param name="confID"></param>
    /// <param name="EndpointID"></param>
    /// <param name="terminalType"></param>
    /// <param name="fnmethod"></param>
    /// <returns></returns>
    [WebMethod]
    public static string fndelete(string userID, string confID, string EndpointID, string terminalType, string fnmethod)
    {
        myVRMNet.NETFunctions obj;
        obj = new myVRMNet.NETFunctions();
        ns_Logger.Logger log;
        log = new ns_Logger.Logger();
        string res = "";
        StringBuilder inXML = new StringBuilder();
        string outXML = "";
        try
        {
            inXML.Append("<login>");
            inXML.Append(obj.OrgXMLElement());
            inXML.Append("<userID>" + userID + "</userID>");
            inXML.Append("<conferenceID>" + confID + "</conferenceID>");
            inXML.Append("</login>");

            outXML = obj.CallCOM2("TerminateConference", inXML.ToString(), HttpContext.Current.Application["RTC_ConfigPath"].ToString());
            if (outXML.IndexOf("<error>") >= 0)
            {
                res = obj.GetTranslatedText("Operation UnSuccessful");
            }
            else
            {
                outXML = obj.CallMyVRMServer("TerminateConference", inXML.ToString(), HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                //FB 3013 Starts
                if (outXML.IndexOf("<error>") >= 0)
                    res = obj.GetTranslatedText("Operation UnSuccessful");
                else
                    res = obj.GetTranslatedText("Operation Successful");
                //FB 3013 Ends
            }
        }
        catch (Exception ex)
        {
            log.Trace("fndelete" + ex.Message);
        }
        return res;
    }
    #endregion

    #region Change Host Details
    /// <summary>
    /// ChangeHostDetails
    /// </summary>
    /// <param name="userID"></param>
    /// <param name="confID"></param>
    /// <param name="EndpointID"></param>
    /// <param name="terminalType"></param>
    /// <param name="ImgStatus"></param>
    /// <returns></returns>
    [WebMethod]
    public static string fnChangeHostDetails(string userID, string confID, string OldEndpointID, string NewEndpointID, string terminalType, string callStatus)
    {
        myVRMNet.NETFunctions obj;
        obj = new myVRMNet.NETFunctions();
        ns_Logger.Logger log;
        log = new ns_Logger.Logger();
        string inXML = "";
        StringBuilder AddXML = new StringBuilder();
        string outXML = "";
        string res = "";
        try
        {
            if(callStatus =="1")            
                outXML = ConnectDisconnect(userID, confID, OldEndpointID, terminalType, "0");

            if ((outXML != null || outXML != "" )&& outXML.IndexOf("<error>") >= 0)
               return res = obj.GetTranslatedText("Operation UnSuccessful");
           
                inXML = "<SetP2PCallerEndpoint>";
                inXML += obj.OrgXMLElement();
                inXML += "<UserID>" + userID + "</UserID>";
                inXML += "<ConfID>" + confID + "</ConfID>";
                inXML += "<OldEndPoints>";
                inXML += "<endpointID>" + OldEndpointID + "</endpointID>";
                inXML += "<terminalType>" + terminalType + "</terminalType>";
                inXML += "</OldEndPoints>";
                inXML += "<NewEndPoints>";
                inXML += "<RoomID>" + NewEndpointID + "</RoomID>";
                inXML += "<TerminalType>2</TerminalType>";
                inXML += "</NewEndPoints>";
                inXML += "</SetP2PCallerEndpoint>";
                outXML = obj.CallMyVRMServer("SetP2PCallerEndpoint", inXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") >= 0)
                    res = obj.GetTranslatedText("Operation UnSuccessful");
                else
                {
                    inXML = String.Empty;
                    outXML = String.Empty;
                    inXML = "<Conference><confID>" + confID + "</confID></Conference>";

                    outXML = obj.CallCOM2("SetConferenceOnMcu", inXML, HttpContext.Current.Application["RTC_ConfigPath"].ToString());

                    if (outXML.IndexOf("<error>") >= 0)
                        res = obj.GetTranslatedText("Operation UnSuccessful");
                    else
                        res = obj.GetTranslatedText("Operation Successful");
                }            
        }
        catch (Exception ex)
        {
            log.Trace("fnPopupAddUser" + ex.Message);
        }        
        return res.ToString();
    }
    #endregion

    #region ConnectORDisconnect Calls
    /// <summary>
    /// ConnectDisconnect
    /// </summary>
    /// <param name="userID"></param>
    /// <param name="confID"></param>
    /// <param name="EndpointID"></param>
    /// <param name="terminalType"></param>
    /// <param name="ImgStatus"></param>
    /// <returns></returns>
    public static string ConnectDisconnect(string userID, string confID, string EndpointID, string terminalType, string ImgStatus)
    {
        myVRMNet.NETFunctions obj;
        obj = new myVRMNet.NETFunctions();
        ns_Logger.Logger log;
        log = new ns_Logger.Logger();
        string outXML = "", inXML = "";
        try
        {
            inXML = "<login>";
            inXML += "<userID>" + userID + "</userID>";
            inXML += "<confID>" + confID + "</confID>";
            inXML += "<endpointID>" + EndpointID + "</endpointID>";
            inXML += "<terminalType>" + terminalType + "</terminalType>";
            if (ImgStatus == "0")
            {
                inXML += "<connectOrDisconnect>0</connectOrDisconnect>";
            }
            else
            {
                inXML += "<connectOrDisconnect>1</connectOrDisconnect>";
            }
            inXML += "<connectOrDisconnectAll>0</connectOrDisconnectAll>";
            inXML += "</login>";

            outXML = obj.CallCOM2("ConnectDisconnectTerminal", inXML, HttpContext.Current.Application["RTC_ConfigPath"].ToString());
        }
        catch (Exception ex)
        {
            log.Trace("ConnectDisConnect" + ex.Message);
        }
        return outXML;
    }
    #endregion

    #region Change Line Rate
    /// <summary>
    /// ChangeLineRate
    /// </summary>
    /// <param name="userID"></param>
    /// <param name="confID"></param>
    /// <param name="EndpointID"></param>
    /// <param name="terminalType"></param>
    /// <param name="ImgStatus"></param>
    /// <returns></returns>
    [WebMethod]
    public static string fnChangeLineRate(string userID, string confID, string EndpointID, string terminalType, string Linerate)
    {
        myVRMNet.NETFunctions obj;
        obj = new myVRMNet.NETFunctions();
        ns_Logger.Logger log;
        log = new ns_Logger.Logger();
        string inXML = "";
        StringBuilder AddXML = new StringBuilder();
        string outXML = "";
        string res = "";
        try
        {
            outXML = ConnectDisconnect(userID, confID, EndpointID, terminalType, "0");

            if (outXML.IndexOf("<error>") >= 0)
                res = obj.GetTranslatedText("Operation UnSuccessful");

            else
            {
                inXML = "<SetP2PCallerLinerate>";
                inXML += obj.OrgXMLElement();
                inXML += "<UserID>" + userID + "</UserID>";
                inXML += "<ConfID>" + confID + "</ConfID>";
                inXML += "<endpointID>" + EndpointID + "</endpointID>";
                inXML += "<terminalType>" + terminalType + "</terminalType>";
                inXML += "<Linerate>" + Linerate + "</Linerate>";
                inXML += "</SetP2PCallerLinerate>";
                outXML = obj.CallMyVRMServer("SetP2PCallerLinerate", inXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") >= 0)
                    res = obj.GetTranslatedText("Operation UnSuccessful");
                else
                {
                    inXML = String.Empty;
                    outXML = String.Empty;
                    inXML = "<Conference><confID>" + confID + "</confID></Conference>";

                    outXML = obj.CallCOM2("SetConferenceOnMcu", inXML, HttpContext.Current.Application["RTC_ConfigPath"].ToString());

                    if (outXML.IndexOf("<error>") >= 0)
                        res = obj.GetTranslatedText("Operation UnSuccessful");
                    else
                        res = obj.GetTranslatedText("Operation Successful");
                }
            }
        }
        catch (Exception ex)
        {
            log.Trace("fnChangeLineRate" + ex.Message);
        }
        return res.ToString();
    }
    #endregion

}
#endregion
