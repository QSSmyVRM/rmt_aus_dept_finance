/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886
using System;
using System.Web;
using System.Text;

/// <summary>
/// Summary description for Class1
/// </summary>
/// 
namespace ns_InXML
{
    public class InXML
    {
        ns_Logger.Logger log;
        myVRMNet.NETFunctions obj;//Organization Module Fixes
        private String errXML;//FB 1881 
        internal bool isAllSilo = false; //FB 2274
        public InXML()
        {
            //
            // TODO: Add constructor logic here
            //
            log = new ns_Logger.Logger();
            obj = new myVRMNet.NETFunctions();//Organization Module Fixes
            errXML = obj.GetErrorMessage(200); //FB 1881
        }
        public String SearchConferenceWorkOrders(String UserID, String ConfID, String WOName, String Rooms, String DateFrom, String DateTo, String TimeFrom, String TimeTo, String Status, String Tpe, String MaxRecords, String PageEnable, String PageNo, String filter) //FB 1114
        {
            try
            {
                String inXML = "";
                inXML += "<login>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <user>";
                inXML += "      <userID>" + HttpContext.Current.Session["userID"].ToString() + "</userID>";
                inXML += "  </user>";
                inXML += "  <userID>" + UserID + "</userID>";

                inXML += "  <Filter>" + filter + "</Filter>";  //code added for Fb 1114

                inXML += "  <ConfID>" + ConfID + "</ConfID>";
                inXML += "  <Name>" + WOName + "</Name>";
                inXML += "  <Rooms>";
                if (Rooms.IndexOf(",") >= 0)
                    for (int i = 0; i < Rooms.Split(',').Length; i++)
                        if (!Rooms.Split(',')[i].Trim().Equals(""))
                            inXML += "      <RoomID>" + Rooms.Split(',')[i] + "</RoomID>";
                inXML += "  </Rooms>";
                inXML += "  <DateFrom>" + DateFrom + "</DateFrom>";
                inXML += "  <DateTo>" + DateTo + "</DateTo>";
                inXML += "  <TimeFrom>" + TimeFrom + "</TimeFrom>";
                inXML += "  <TimeTo>" + TimeTo + "</TimeTo>";
                inXML += "  <Status>" + Status + "</Status>";
                inXML += "  <Type>" + Tpe + "</Type>";
                inXML += "  <MaxRecords>" + MaxRecords + "</MaxRecords>";
                inXML += "  <pageEnable>" + PageEnable + "</pageEnable>";
                inXML += "  <pageNo>" + PageNo + "</pageNo>";
                inXML += "  <SortBy>3</SortBy>";
                inXML += "</login>";
                return inXML;
            }
            catch (Exception ex)
            {
                log.Trace("error in SearchConferenceWorkOrder: " + ex.Message);
                return errXML;//FB 1881
                //return "<error>error in SearchConferenceWorkOrder</error>";
            }
        }
        public String SearchConferenceWorkOrders(String UserID, String ConfID, String WOName, String Rooms, String DateFrom, String DateTo, String TimeFrom, String TimeTo, String Status, String Tpe, String MaxRecords, String PageEnable, String PageNo, String SortBy, String filter) //FB 1114
        {
            try
            {
                String inXML = "";
                inXML += "<login>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <user>";
                inXML += "      <userID>" + HttpContext.Current.Session["userID"].ToString() + "</userID>";
                inXML += "  </user>";
                inXML += "  <userID>" + UserID + "</userID>";


                inXML += "  <Filter>" + filter + "</Filter>";  //code added for Fb 1114

                inXML += "  <ConfID>" + ConfID + "</ConfID>";
                inXML += "  <Name>" + WOName + "</Name>";
                inXML += "  <Rooms>";
                if (Rooms.IndexOf(",") >= 0)
                    for (int i = 0; i < Rooms.Split(',').Length; i++)
                        if (!Rooms.Split(',')[i].Trim().Equals(""))
                            inXML += "      <RoomID>" + Rooms.Split(',')[i] + "</RoomID>";
                inXML += "  </Rooms>";
                inXML += "  <DateFrom>" + DateFrom + "</DateFrom>";
                inXML += "  <DateTo>" + DateTo + "</DateTo>";
                inXML += "  <TimeFrom>" + TimeFrom + "</TimeFrom>";
                inXML += "  <TimeTo>" + TimeTo + "</TimeTo>";
                inXML += "  <Status>" + Status + "</Status>";
                inXML += "  <Type>" + Tpe + "</Type>";
                inXML += "  <MaxRecords>" + MaxRecords + "</MaxRecords>";
                inXML += "  <pageEnable>" + PageEnable + "</pageEnable>";
                inXML += "  <pageNo>" + PageNo + "</pageNo>";
                inXML += "  <SortBy>" + SortBy + "</SortBy>";
                inXML += "</login>";
                return inXML;
            }
            catch (Exception ex)
            {
                log.Trace("error in SearchConferenceWorkOrder:" + ex.Message);
                return errXML;//FB 1881
                //return "<error>error in SearchConferenceWorkOrder</error>";
            }
        }
        public String GetInventoryDetails(String InventoryID, String DateFrom, String DateTo, String TimeFrom, String TimeTo, String Timezone, String Tpe, String pre, String WorkorderID)
        {
            
            try
            {
                String InXML = "";                
                //Code added by Offshore for FB Issue 1073 -- Start
                DateFrom = myVRMNet.NETFunctions.GetDefaultDate(DateFrom);
                DateTo = myVRMNet.NETFunctions.GetDefaultDate(DateTo);
                //Code changed by Offshore for FB Issue 1073 -- End
                //FB 2588 Starts
                TimeFrom = myVRMNet.NETFunctions.ChangeTimeFormat(TimeFrom);
                TimeTo = myVRMNet.NETFunctions.ChangeTimeFormat(TimeTo);
                //FB 2588 Ends
                InXML += "<login>";
                InXML += obj.OrgXMLElement();//Organization Module Fixes
                InXML += "  <userID>" + HttpContext.Current.Session["userID"].ToString() + "</userID>";
                InXML += "  <Type>" + Tpe + "</Type>";
                InXML += "  <ID>" + InventoryID + "</ID>";
                InXML += "  <StartByDate>" + DateFrom + "</StartByDate>";
                InXML += "  <StartByTime>" + TimeFrom + "</StartByTime>";
                InXML += "  <CompleteByDate>" + DateFrom + "</CompleteByDate>";
                InXML += "  <CompleteByTime>" + TimeTo + "</CompleteByTime>";
                InXML += "  <Timezone>" + Timezone + "</Timezone>";
                InXML += "  <WorkorderID>" + WorkorderID + "</WorkorderID>";
                InXML += "  <PreselectedItems>" + pre + "</PreselectedItems>";
                InXML += "</login>";
                return InXML;
            }
            catch (Exception ex)
            {
                log.Trace("error in GetInventoryDetails: " + ex.Message);
                return errXML;//FB 1881
                //return "<error>error in GetInventoryDetails</error>";
            }
        }

        public String GetWorkOrderDetails(String WorkorderID, String ConfID)
        {
            try
            {
                String InXML = "";
                InXML += "<login>";
                InXML += obj.OrgXMLElement();//Organization Module Fixes
                InXML += "  <userID>" + HttpContext.Current.Session["userID"].ToString() + "</userID>";
                InXML += "  <ConfID>" + ConfID + "</ConfID>";
                InXML += "  <WorkorderID>" + WorkorderID + "</WorkorderID>";
                InXML += "</login>";
                return InXML;
            }
            catch (Exception ex)
            {
                log.Trace("error in GetInventoryDetails: " + ex.Message);
                return errXML;//FB 1881
                //return "<error>error in GetInventoryDetails</error>";
            }
        }
        public String SearchConference(string UserID, string ConferenceStatus, string ConferenceName, string ConferenceID, string ConferenceUniqueID, string CTSNumericID, string ConferenceSearchType, string DateFrom, string DateTo, string ConferenceHost, string ConferenceParticipant, string Public, string SelectionType, string SelectedRooms, string PageNo, string SortBy, string RecurrenceStyle, string customInxml, string conciergeInxml, String strHotdesking, String SortingOrder, string allsiloInxml, bool IsDeleted, string FilterType, string MCUXML) //, String ApprovalPending ,Custom Attribute Fix //FB 2694 //FB 2822 //FB 2870 //FB 3006 //FB 2639 - Search TIK# 100037 //ZD 100369_MCU
        {
            try
            {
                //HttpContext.Current.Response.Write(HttpContext.Current.Session["userID"].ToString());
                StringBuilder InXML = new StringBuilder();
                string Status = "2";

                InXML.Append("<SearchConference>");

                if (!isAllSilo)//FB 2274
                    InXML.Append(obj.OrgXMLElement());//Organization Module Fixes

                InXML.Append("<UserID>" + UserID + "</UserID>");
                InXML.Append("<ConferenceID>" + ConferenceID + "</ConferenceID>");
                InXML.Append("<ConferenceUniqueID>" + ConferenceUniqueID + "</ConferenceUniqueID>");
                InXML.Append("<CTSNumericID>" + CTSNumericID + "</CTSNumericID>");//FB 2870
                InXML.Append("<StatusFilter>");
                if (ConferenceStatus.IndexOf(",") >= 0)
                {
                    for (int i = 0; i < ConferenceStatus.Split(',').Length; i++)
                    {
                        InXML.Append("<ConferenceStatus>" + ConferenceStatus.Split(',')[i] + "</ConferenceStatus>");
                        if (ConferenceStatus.Split(',')[i] == "1")
                            Status = "0";
                    }
                    InXML.Append("<Status>" + Status + "</Status>");
                }
                else
                {
                    InXML.Append("<ConferenceStatus>" + ConferenceStatus + "</ConferenceStatus>");
                    InXML.Append("<Status>1</Status>");
                }
                InXML.Append("</StatusFilter>");
                //FB 1942 Starts TIK# 100037
                if (IsDeleted)
                    InXML.Append("<SearchDeletedConf>1</SearchDeletedConf>");
                else
                    InXML.Append("<SearchDeletedConf>0</SearchDeletedConf>");
                //FB 1942 Ends
                InXML.Append("<ConferenceName>" + ConferenceName + "</ConferenceName>");
                InXML.Append("<ConferenceSearchType>" + ConferenceSearchType + "</ConferenceSearchType>");
                InXML.Append("<DateFrom>" + DateFrom + "</DateFrom>");
                InXML.Append("<DateTo>" + DateTo + "</DateTo>");
                InXML.Append("<ConferenceHost>" + ConferenceHost + "</ConferenceHost>");
                InXML.Append("<ConferenceParticipant>" + ConferenceParticipant + "</ConferenceParticipant>");
                InXML.Append("<Public>" + Public + "</Public>");
                if (HttpContext.Current != null && HttpContext.Current.Request.QueryString["t"] != null) //ZD 100718
                    if (HttpContext.Current.Request.QueryString["t"].ToString().Equals("6"))
                        InXML.Append("<ApprovalPending>1</ApprovalPending>");
                    else
                        InXML.Append("<ApprovalPending>0</ApprovalPending>");
                InXML.Append("<RecurrenceStyle>" + RecurrenceStyle + "</RecurrenceStyle>");
                InXML.Append("<Location>");
                InXML.Append("<SelectionType>" + SelectionType + "</SelectionType>");
                InXML.Append("<SelectedRooms>" + SelectedRooms + "</SelectedRooms>");
                InXML.Append("</Location>");
                InXML.Append("<SelectedMCU>"); //ZD 100369
                InXML.Append(MCUXML);
                InXML.Append("</SelectedMCU>");
                InXML.Append("<PageNo>" + PageNo + "</PageNo>"); //<!-- Page number will be any number based on what you return in total pages -->
                InXML.Append("<SortBy>" + SortBy + "</SortBy>"); // <!-- 1:UniqueID, 2:Conference Name, 3:Conference Start Date/Time -->
                InXML.Append("<SortingOrder>" + SortingOrder + "</SortingOrder>");//FB 2822
                InXML.Append(allsiloInxml); //FB 3006
                //FB 2632 - Starts
                InXML.Append("<ConciergeSupport>");
                InXML.Append(conciergeInxml);
                InXML.Append("</ConciergeSupport>");
                //FB 2632 - End

                //Custom Attribute Fix  -- Start
                if (customInxml.Trim() != "")
                {
                    InXML.Append("<CustomAttributesList>");
                    InXML.Append(customInxml);
                    InXML.Append("</CustomAttributesList>");
                }
                //Custom Attribute Fix -- End
                //FB 2694
                if (strHotdesking != "")
                    InXML.Append(strHotdesking);

                InXML.Append("<FilterType>" + FilterType + "</FilterType>");//FB 2639 - Search

                InXML.Append("</SearchConference>");
                return InXML.ToString();
            }
            catch (Exception ex)
            {
                log.Trace("error in SearchConference: " + ex.Message);
                return errXML;//FB 1881
                //return "<error>error in SearchConference</error>";
            }

        }

        public String SearchProviderMenus(String UserID, String RoomID, String ServiceID)
        {
            try
            {
                String inXML = "";
                inXML += "<SearchProviderMenus>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <UserID>" + UserID + "</UserID>";
                inXML += "  <RoomID>" + RoomID  + "</RoomID>";
                inXML += "  <ServiceID>" + ServiceID + "</ServiceID>";
                inXML += "</SearchProviderMenus>";
                return inXML;
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message + " : " + ex.StackTrace);
                return "";
            }
        }
    }
}
